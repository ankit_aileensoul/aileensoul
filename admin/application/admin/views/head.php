<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="Dump Truck">

    <meta name="author" content="Dump Truck">

    <meta name="keyword" content="Dump Truck">

    <link rel="shortcut icon" href="<?php echo base_url('admin/assets/img/dump.png') ?>" type="image/x-icon" />

    <title><?php echo $title; ?></title>



    <!-- Bootstrap core CSS -->

    <link href="<?php  echo base_url('admin/assets/css/bootstrap.css') ?>" rel="stylesheet">

    <!--external css-->

    <link href="<?php echo  base_url('admin/assets/font-awesome/css/font-awesome.css') ?>" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('admin/assets/css/zabuto_calendar.css') ?>" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('admin/assets/js/gritter/css/jquery.gritter.css') ?>" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('admin/assets/lineicons/style.css') ?>" />    

    

    <!-- Custom styles for this template -->

    <link href="<?php  echo base_url('admin/assets/css/style.css') ?>?v=<?php echo time();?>" rel="stylesheet">

    <link href="<?php echo base_url('admin/assets/css/style-responsive.css'); ?>?v=<?php echo time();?>" rel="stylesheet">

    <link href="<?php echo base_url('admin/assets/css/table-responsive.css') ?>?v=<?php echo time();?>" rel="stylesheet">

    <script src="<?php echo base_url('admin/assets/js/chart-master/Chart.js') ?>?v=<?php echo time();?>" ></script>

   

  </head>


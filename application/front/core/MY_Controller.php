<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
     // echo $this->session->userdata('aileenuser');  echo "hello"; die();
        if (!$this->session->userdata('aileenuser')) {
            redirect('login', 'refresh');
        } else {
            $this->data['userid'] = $this->session->userdata('aileenuser');
        }

        
        $user_id=$this->data['userid'];
        $condition_array=array('status' => '1');
        $this->data['loged_in_user']=$this->common->select_data_by_id('user','user_id',$user_id,'user_name,user_image',$condition_array);
        
        
    }


}

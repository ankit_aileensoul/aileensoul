<?php if (!defined('BASEPATH'))    exit('No direct script access allowed');

class Business_profile extends MY_Controller {

   

    public function __construct() 
    {
        parent::__construct();

         $this->load->library('form_validation');
         $this->load->model('email_model');
 
        include ('include.php');
    }

    public function index()
    {  
     
      $userid  = $this->session->userdata('aileenuser'); 

          $contition_array = array('user_id' => $userid,  'status' =>'0');
         $businessdata = $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');
         if($businessdata){

          $this->load->view('business_profile/reactivate', $this->data); 
         }

         else{
         $userid  = $this->session->userdata('aileenuser'); 
         
         $contition_array = array( 'user_id' => $userid, 'is_deleted' => '0', 'status' =>'1' );
        $userdata = $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');
              
             $contition_array = array('status' => 1);
          $this->data['countries'] =  $this->common->select_data_by_condition('countries', $contition_array, $data = '*', $sortby = 'country_name', $orderby = 'ASC', $limit = '', $offset = '', $join_str = array(), $groupby = ''); 

           //for getting state data
            $contition_array = array('status' => 1);
            $this->data['states'] =  $this->common->select_data_by_condition('states', $contition_array, $data = '*', $sortby = 'state_name', $orderby = 'ASC', $limit = '', $offset = '', $join_str = array(), $groupby = '');
          

             //for getting city data
            $contition_array = array('status' => 1);
            $this->data['cities'] =  $this->common->select_data_by_condition('cities', $contition_array, $data = '*', $sortby = 'city_name', $orderby = 'ASC', $limit = '', $offset = '', $join_str = array(), $groupby = '');
           
           if(count($userdata) > 0){

            if($userdata[0]['business_step'] == 1){ 
             redirect('business_profile/contact_information', refresh);
            }
            else if($userdata[0]['business_step'] == 2){ 
             redirect('business_profile/description', refresh);
            }
            else if($userdata[0]['business_step'] == 3){
                redirect('business_profile/image', refresh);
            }
            else if($userdata[0]['business_step'] == 4){
                redirect('business_profile/addmore', refresh);
            }
            else if($userdata[0]['business_step'] == 5){
                redirect('business_profile/business_profile_post', refresh);
            }
            
         }
            else{ 
         $this->load->view('business_profile/business_info', $this->data);
       }

    }
             
    }

    public function ajax_data() { 

   //dependentacy industrial and sub industriyal start


    	if(isset($_POST["industry_id"]) && !empty($_POST["industry_id"])){ 
    //Get all state data
         $contition_array = array('industry_id' => $_POST["industry_id"] , 'status' => 1);
     $subindustriyaldata =  $this->data['subindustriyaldata'] =  $this->common->select_data_by_condition('sub_industry_type', $contition_array, $data = '*', $sortby = 'sub_industry_name', $orderby = 'ASC', $limit = '', $offset = '', $join_str = array(), $groupby = '');
   
   	
    //Count total number of rows
   
    
    //Display sub industiyal list
    if(count($subindustriyaldata) > 0){
        echo '<option value="">Select Sub Industrial</option>';
     foreach($subindustriyaldata as $st){
            echo '<option value="'.$st['sub_industry_id'].'">'.$st['sub_industry_name'].'</option>';
     
        }
    }else{
        echo '<option value="">Subindustriyal not available</option>';
    }
}

   // dependentacy industrial and sub industriyal end	

      
       if(isset($_POST["country_id"]) && !empty($_POST["country_id"])){ 
    //Get all state data
         $contition_array = array('country_id' => $_POST["country_id"] , 'status' => 1);
     $state =  $this->data['states'] =  $this->common->select_data_by_condition('states', $contition_array, $data = '*', $sortby = 'state_name', $orderby = 'ASC', $limit = '', $offset = '', $join_str = array(), $groupby = '');
   
    //Count total number of rows
   
    
    //Display states list
    if(count($state) > 0){
        echo '<option value="">Select state</option>';
     foreach($state as $st){
            echo '<option value="'.$st['state_id'].'">'.$st['state_name'].'</option>';
     
        }
    }else{
        echo '<option value="">State not available</option>';
    }
}

if(isset($_POST["state_id"]) && !empty($_POST["state_id"])){
    //Get all city data
     $contition_array = array('state_id' => $_POST["state_id"] , 'status' => 1);
     $city =  $this->data['city'] =  $this->common->select_data_by_condition('cities', $contition_array, $data = '*', $sortby = 'city_name', $orderby = 'ASC', $limit = '', $offset = '', $join_str = array(), $groupby = '');
    
    
    //Display cities list
      if(count($city) > 0){
          echo '<option value="">Select city</option>';
          foreach($city as $cit){
              echo '<option value="'.$cit['city_id'].'">'.$cit['city_name'].'</option>';
          }
      }else{
          echo '<option value="">City not available</option>';
      }
      }
  }

    public function business_information_update()
    {
            $userid = $this->session->userdata('aileenuser');

            $contition_array = array('status' => 1);
           $this->data['countries'] =  $this->common->select_data_by_condition('countries', $contition_array, $data = '*', $sortby = 'country_name', $orderby = 'ASC', $limit = '', $offset = '', $join_str = array(), $groupby = ''); 

           //for getting state data
            $contition_array = array('status' => 1);
            $this->data['states'] =  $this->common->select_data_by_condition('states', $contition_array, $data = '*', $sortby = 'state_name', $orderby = 'ASC', $limit = '', $offset = '', $join_str = array(), $groupby = '');
           // echo "<pre>";print_r($this->data['states']);echo "</pre>";die();

             //for getting city data
            $contition_array = array('status' => 1);
            $this->data['cities'] =  $this->common->select_data_by_condition('cities', $contition_array, $data = '*', $sortby = 'city_name', $orderby = 'ASC', $limit = '', $offset = '', $join_str = array(), $groupby = '');
            

            $contition_array = array( 'user_id' => $userid, 'is_deleted' => '0' , 'status' => '1');
           $userdata = $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');


           if($userdata){
            $step = $userdata[0]['business_step'];

            if($step == 1 || $step >1)
            {
              
              $this->data['country1'] = $userdata[0]['country'];
              $this->data['state1'] = $userdata[0]['state'];
              $this->data['city1'] = $userdata[0]['city'];
             $this->data['companyname1'] = $userdata[0]['company_name'];
             $this->data['pincode1'] =  $userdata[0]['pincode'];
             $this->data['address1'] =  $userdata[0]['address'];
             }
             
             } 

            $this->load->view('business_profile/business_info', $this->data);
    }



//business automatic retrieve controller start
public function business()
    {
         $json = [];
        

        if(!empty($this->input->get("q"))){
            $this->db->like('skill', $this->input->get("q"));
            $query = $this->db->select('skill_id as id,skill as text')
                        ->limit(10)
                        ->get("skill");
            $json = $query->result();
        }

        
        echo json_encode($json);
        
    }
//business automatic retrieve controller End


    // business prodile slug start

  public function setcategory_slug($slugname, $filedname, $tablename, $notin_id = array())
        {$slugname = $oldslugname = $this->create_slug($slugname);
        $i = 1;
        while($this->comparecategory_slug($slugname, $filedname, $tablename, $notin_id)>0)
        {$slugname = $oldslugname.'-'.$i;
        $i++;
        }return $slugname;
    }

    public function comparecategory_slug($slugname, $filedname, $tablename, $notin_id = array()) {
        $this->db->where($filedname, $slugname);
        if (isset($notin_id) && $notin_id != "" && count($notin_id) > 0 && !empty($notin_id)) {
            $this->db->where($notin_id);
        }
        $num_rows = $this->db->count_all_results($tablename);
        return $num_rows;
    }

    public function create_slug($string) {
        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', strtolower(stripslashes($string)));
        $slug = preg_replace('/[-]+/', '-', $slug);
        $slug = trim($slug, '-');
        return $slug;

        }
    
    // business slug end

  public function business_information_insert(){
       
       
        $userid = $this->session->userdata('aileenuser');
          

               if($this->input->post('next')){  

        $this->form_validation->set_rules('companyname', 'Company Name', 'required');
       
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('state', 'state', 'required');
        
        $this->form_validation->set_rules('business_address', 'Address', 'required');
        
        if ($this->form_validation->run() == FALSE) { 
         $this->load->view('business_profile/business_info'); 
         } 

          //get data by id only

         $contition_array = array( 'user_id' => $userid, 'is_deleted' => '0' , 'status' => '1');
           $userdata = $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');
           
        

           $companyname = $this->input->post('companyname');

         if($userdata)
         {
                       $data = array(
                          'company_name' => $this->input->post('companyname'),
                          'country' => $this->input->post('country'),
                          'state' => $this->input->post('state'),
                          'city' => $this->input->post('city'),
                          'pincode' => $this->input->post('pincode'),
                          'address' => $this->input->post('business_address'),
                          'user_id'=> $userid,
                    'business_slug'=> $this->setcategory_slug($companyname,'business_slug','business_profile'),
                          'modified_date' => date('Y-m-d',time()),
                          'status'=>1,
                          'is_deleted'=>0
                  ); 
                    
                  $updatdata =   $this->common->update_data($data,'business_profile','user_id',$userid);
                 if($updatdata ){ 
                     
                        $this->session->set_flashdata('success', 'Business information updated successfully');
                       redirect('business_profile/contact_information', refresh);
                 }else{
                          $this->session->flashdata('error','Sorry!! Your data not inserted');
                          redirect('business_profile', refresh);
                 }
         } 

         else
         {
                 
                    $data = array(
                        'company_name' => $this->input->post('companyname'),
                        'country' => $this->input->post('country'),
                        'state' => $this->input->post('state'),
                        'city' => $this->input->post('city'),
                        'pincode' => $this->input->post('pincode'),
                        'address' => $this->input->post('business_address'),
                        'user_id'=> $userid,
                        'business_slug'=> $this->setcategory_slug($companyname,'business_slug','business_profile'),
                        'created_date' => date('Y-m-d',time()),
                        'status'=>1,
                        'is_deleted'=>0,
                        'business_step'=>1
                ); 
                 
                    
                   
                $insert_id =   $this->common->insert_data_getid($data,'business_profile'); 
               if($insert_id){ 
                   
                       
                       $this->session->set_flashdata('success', 'Business information updated successfully');
                     redirect('business_profile/contact_information', refresh);
               }else{
                        $this->session->flashdata('error','Sorry!! Your data not inserted');
                       redirect('business_profile', refresh);
               }
              
         }
      }
    }

    public function contact_information(){

          $userid = $this->session->userdata('aileenuser');

            $contition_array = array( 'user_id' => $userid, 'is_deleted' => '0' , 'status' => '1');
           $userdata = $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');
           
           if($userdata){
            $step = $userdata[0]['business_step'];

            if($step == 2 || $step > 2 || ($step >= 1 && $step <= 2))
            {
             $this->data['contactname1'] = $userdata[0]['contact_person'];
             $this->data['contactmobile1'] =  $userdata[0]['contact_mobile'];
             $this->data['contactemail1'] =  $userdata[0]['contact_email'];
             $this->data['contactwebsite1'] =  $userdata[0]['contact_website'];
             }
             
             } 

        $this->load->view('business_profile/contact_info', $this->data);
        }

     public function contact_information_insert(){
             $userid = $this->session->userdata('aileenuser');


             if($this->input->post('previous')){ 
               redirect('business_profile', refresh);
              }


             $this->form_validation->set_rules('contactname', 'Contact person', 'required');
             $this->form_validation->set_rules('contactmobile', 'Phone no', 'required|numeric|min_length[10]|max_length[11]');
             $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            

             if ($this->form_validation->run() == FALSE) { 
                 $this->load->view('business_profile/contact_info'); 
                } 
            else{
                    $data = array(
                    'contact_person' => $this->input->post('contactname'),
                    'contact_mobile' => $this->input->post('contactmobile'),
                    'contact_email' => $this->input->post('email'),
                    'contact_website' => $this->input->post('contactwebsite'),
                    'modified_date' => date('Y-m-d',time()),
                     'business_step'=>2
            ); 
                   
       
      $updatdata =   $this->common->update_data($data,'business_profile','user_id',$userid);


          if($updatdata){ 
             $this->session->set_flashdata('success', 'Contact information updated successfully');
            redirect('business_profile/description', refresh);
          }else{
             $this->session->flashdata('error','Your data not inserted');
                   redirect('business_profile/contact_information', refresh);
          }
       
      }
    }


     public function check_email() { 
        

        $email = $this->input->post('email');

         $userid = $this->session->userdata('aileenuser');

            $contition_array = array( 'user_id' => $userid, 'is_deleted' => '0' , 'status' => '1');
           $userdata = $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

           $email1=$userdata[0]['contact_email'];
      
           if($email1)
           {
               $condition_array = array('is_deleted' => '0', 'user_id !=' => $userid, 'status' => '1');
        
              $check_result = $this->common->check_unique_avalibility('business_profile', 'contact_email', $email, '', '', $condition_array);
           }
          else
          {
       
          $condition_array = array('is_deleted' => '0', 'status' => '1');
        
        $check_result = $this->common->check_unique_avalibility('business_profile', 'contact_email', $email, '', '', $condition_array);
     
        }

        if ($check_result) {
        echo 'true';
        die();
        } else {
        echo 'false';
        die();
        }
        }
       
// khyati changes start 7-4

    public function description(){ 

        $userid = $this->session->userdata('aileenuser');

            $contition_array = array( 'user_id' => $userid, 'is_deleted' => '0' , 'status' => '1');
           $userdata = $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');


           if($userdata){
            $step = $userdata[0]['business_step'];

            if($step == 3 || ($step >= 1 && $step <= 3) || $step > 3)
            {
    $business_type1 =    $this->data['business_type1'] = $userdata[0]['business_type'];
   $industriyal1 = $this->data['industriyal1'] =  $userdata[0]['industriyal'];
             $this->data['subindustriyal1'] =  $userdata[0]['subindustriyal'];
             $this->data['business_details1'] =  $userdata[0]['details'];
             $this->data['other_business'] =  $userdata[0]['other_business_type'];
             $this->data['other_industry'] =  $userdata[0]['other_industrial'];


              $contition_array = array('status' => 1,"industry_id !=" => $industriyal1);
          $this->data['industriyaldata'] =  $this->common->select_data_by_condition('industry_type', $contition_array, $data = '*', $sortby = 'industry_name', $orderby = 'ASC', $limit = '', $offset = '', $join_str = array(), $groupby = '');

            $contition_array = array('status' => 1,"type_id !=" => $business_type1);
          $this->data['businesstypedata'] =  $this->common->select_data_by_condition('business_type', $contition_array, $data = '*', $sortby = 'business_name', $orderby = 'ASC', $limit = '', $offset = '', $join_str = array(), $groupby = ''); 


             }
             
             } 
             //cho "<pre>"; print_r($this->data['industriyal1']); die();

        $this->load->view('business_profile/description', $this->data);
        }

// khyati changes end 7-4


     public function description_insert(){
      
             $userid = $this->session->userdata('aileenuser');

             

               if($this->input->post('next')){  

             $this->form_validation->set_rules('business_type', 'Business Type', 'required');
             $this->form_validation->set_rules('industriyal', 'Industriyal', 'required');
         
             $this->form_validation->set_rules('business_details', 'Details', 'required');

             if ($this->form_validation->run() == FALSE) {
                 $this->load->view('business_profile/description'); 
                } 
            else{
                    $data = array(
          'business_type' => $this->input->post('business_type'),
          'industriyal' => $this->input->post('industriyal'),
          'subindustriyal' => $this->input->post('subindustriyal'),
          'other_business_type' => $this->input->post('bustype'),
          'other_industrial' => $this->input->post('indtype'),
          'details' => $this->input->post('business_details'),
          'modified_date' => date('Y-m-d',time()),
          'business_step'=>3
            ); 
       
      $updatdata =   $this->common->update_data($data,'business_profile','user_id',$userid);

          if($updatdata){
               $this->session->set_flashdata('success', 'Description updated successfully'); 
            redirect('business_profile/image', refresh);
          }else{
             $this->session->flashdata('error','Your data not inserted');
                   redirect('business_profile/description', refresh);
          }
        }
     }
    }

    public function image(){

        $userid = $this->session->userdata('aileenuser');

            $contition_array = array( 'user_id' => $userid, 'is_deleted' => '0' , 'status' => '1');
           $userdata = $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

           if($userdata){
            $step = $userdata[0]['business_step'];

            if($step == 4 || ($step >= 1 && $step <= 4) || $step > 4 )
            {
             $this->data['image1'] = $userdata[0]['business_profile_image'];
             }
             
             } 
        $this->load->view('business_profile/image', $this->data);
        }

    public function image_insert(){
      
             $userid = $this->session->userdata('aileenuser');

               if($this->input->post('next')){  


              if (empty($_FILES['image1']['name']))
              {
                 
              }
            else
              {
                $config['upload_path'] = 'uploads/business_profile_images/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
              
                $config['file_name'] = $_FILES['image1']['name'];
               
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('image1'))
                {
                    $uploadData = $this->upload->data();
                   
                    $picture = $uploadData['file_name'];

                }
                else
                {
                    $picture = '';
                }
          }

              if($picture){ 

              $data = array(  
                    'business_profile_image' =>$picture,
                    'modified_date' => date('Y-m-d',time()),
                    'business_step'=>4
                    );

                    }
            else{

              $data = array(
                    'business_profile_image' =>$this->input->post('filename'),
                    'modified_date' => date('Y-m-d',time()),
                    'business_step'=>4

            );

            } 
      $updatdata =   $this->common->update_data($data,'business_profile','user_id',$userid);

          if($updatdata){ 
              $this->session->set_flashdata('success', 'Image updated successfully'); 
            redirect('business_profile/addmore', refresh);
          }else{
             $this->session->flashdata('error','Your data not inserted');
                   redirect('business_profile/image', refresh);
          }
       }
    }
//end edit skills


    public function addmore(){
        $userid = $this->session->userdata('aileenuser');

            $contition_array = array( 'user_id' => $userid, 'is_deleted' => '0' , 'status' => '1');
           $userdata = $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

           if($userdata){
            $step = $userdata[0]['business_step'];

           if($step == 5 || ($step >= 1 && $step <= 5) || $step > 5)
            {
             $this->data['addmore1'] = $userdata[0]['addmore'];
             }
             
             } 
       
        $this->load->view('business_profile/addmore', $this->data);
        }

     public function addmore_insert(){
             $userid = $this->session->userdata('aileenuser');

                    $data = array(
                    'addmore' => $this->input->post('addmore'),
                    'modified_date' => date('Y-m-d',time()),
                    'business_step'=>5
            ); 
       
      $updatdata =   $this->common->update_data($data,'business_profile','user_id',$userid);

          if($updatdata){ 
               
            redirect('business_profile/business_profile_post', refresh);
          }else{
             $this->session->flashdata('error','Your data not inserted');
                   redirect('business_profile/addmore', refresh);
          }
        
    }


//business_profile_post start

    public function business_profile_post(){
        
           $userid = $this->session->userdata('aileenuser');
            
            $user_name = $this->session->userdata('user_name'); 
            
            
            $contition_array = array('user_id' => $userid, 'status' =>'1' );
             $this->data['businessdata'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

            //echo "<pre>"; print_r($this->data['businessdata']); die(); 

     $business_profile_id =  $this->data['businessdata'][0]['business_profile_id'];

      $contition_array = array('is_deleted' => 0, 'status' => 1 , 'user_id !=' => $userid );
       $userlist = $this->data['userlist'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = 'business_profile_id', $orderby = 'DESC', $limit = '', $offset = '', $join_str = array(), $groupby = '');

              //echo $business_profile_id; die();

//userlist for followdata strat

 // using category             
    $industriyal = $this->data['businessdata'][0]['industriyal'];
    foreach ($userlist as $rowcategory) {
    
          if($industriyal == $rowcategory['industriyal'])
          {  
            $userlistcategory[] = $rowcategory;
            
          }
        
        }

        $this->data['userlistview1'] = $userlistcategory;

//using city 


        $businessregcity = $this->data['businessdata'][0]['city'];

        $contition_array = array('is_deleted' => 0, 'status' => 1 , 'user_id !=' => $userid, 'industriyal !=' => $industriyal);
       $userlist2 = $this->data['userlist2'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = 'business_profile_id', $orderby = 'DESC', $limit = '', $offset = '', $join_str = array(), $groupby = '');


        foreach ($userlist2 as $rowcity) {
  
          

          if($businessregcity == $rowcity['city'])
          {  
            $userlistcity[] = $rowcity;
            
          }
         
        }

        $this->data['userlistview2'] = $userlistcity;

// using state

        $businessregstate = $this->data['businessdata'][0]['state'];

        $contition_array = array('is_deleted' => 0, 'status' => 1 , 'user_id !=' => $userid, 'industriyal !=' => $industriyal, 'city' => $businessregcity);
       $userlist3 = $this->data['userlist3'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = 'business_profile_id', $orderby = 'DESC', $limit = '', $offset = '', $join_str = array(), $groupby = '');



          foreach ($userlist3 as $rowstate) {
    
          

            if($businessregstate == $rowstate['state'])
            {  
              $userliststate[] = $rowstate;
              
            }
          
          }

   
          $this->data['userlistview3'] = $userliststate;

// using 3 user 

      $contition_array = array('is_deleted' => 0, 'status' => 1 , 'user_id !=' => $userid, 'industriyal !=' => $industriyal, 'city !=' => $businessregcity, 'state !=' => $businessregstate);
       $userlastview = $this->data['userlastview'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = 'business_profile_id', $orderby = 'DESC', $limit = '', $offset = '', $join_str = array(), $groupby = '');  

         
         $this->data['userlistview4'] = $userlastview;
       

//userlist for followdata end


//data fatch using follower start

              $contition_array = array('follow_from' => $business_profile_id, 'follow_status' =>'1',  'follow_type' =>'2');

            $followerdata = $this->data['followerdata'] =  $this->common->select_data_by_condition('follow', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

            //echo "<pre>" ; print_r($this->data['followerdata']); die();

             foreach ($followerdata  as $fdata) { 
           
              $contition_array = array('business_profile_id' => $fdata['follow_to']);

           $this->data['business_data'] = $this->common->select_data_by_condition('business_profile', $contition_array, $data='*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');

           // echo "<pre>" ; print_r($this->data['business_data']); die();

           $business_userid = $this->data['business_data'][0]['user_id'];
           //echo $business_userid; die();
          $contition_array = array('user_id' => $business_userid, 'status' =>'1', 'is_delete' => '0');

           $this->data['business_profile_data'] = $this->common->select_data_by_condition('business_profile_post', $contition_array, $data='*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');

            //echo "<pre>"; print_r($this->data['business_profile_data']) ; die();

           $followerabc[] = $this->data['business_profile_data']; 

         }
         //echo "<pre>" ; print_r($followerabc); die();

//data fatch using follower end
//data fatch using industriyal start

            $userselectindustriyal = $this->data['businessdata'][0]['industriyal'];
          
           $contition_array = array('industriyal' => $userselectindustriyal, 'status' =>'1');
            $businessprofiledata = $this->data['businessprofiledata'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

           

            foreach ($businessprofiledata  as $fdata) {
           
             
              $contition_array = array('business_profile_post.user_id' => $fdata['user_id'], 'business_profile_post.status' =>'1', 'business_profile_post.user_id !=' => $userid, 'is_delete' => '0');

           $this->data['business_data'] = $this->common->select_data_by_condition('business_profile_post', $contition_array, $data='*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');
           $industriyalabc[] = $this->data['business_data']; 

         } 
//data fatch using industriyal end
//data fatch using login user last post start

         $condition_array = array('user_id' => $userid, 'status' =>'1', 'is_delete' => '0' );

        $business_datauser  = $this->data['business_datauser'] = $this->common->select_data_by_condition('business_profile_post', $condition_array, $data='*', $sortby = 'business_profile_post_id', $orderby = 'DESC', $limit = '', $offset = '', $join_str= array(), $groupby = '');
           $userabc[][] = $this->data['business_datauser'][0]; 

 
           
//data fatch using login user last post end
//array merge and get unique value  

                  
          if(count($industriyalabc) == 0 && count($business_datauser) != 0){ 
           
           $unique = $userabc;
             }elseif(count($business_datauser) == 0 && count($industriyalabc) != 0){ 
           $unique = $industriyalabc;
             }elseif(count($business_datauser) != 0 && count($industriyalabc) != 0){ 
               $unique = array_merge($industriyalabc, $userabc);
             }

           //echo "<pre>"; print_r($unique); die();

           if(count($followerabc) == 0 && count($unique) != 0){
           $unique_user = $unique;
             }elseif(count($unique) == 0 && count($followerabc) != 0){
           $unique_user = $followerabc;
             } else{
           $unique_user = array_merge($unique, $followerabc);
             }
       

       
          foreach($unique_user as $ke => $arr) 
          {  
                foreach($arr as $k => $v) 
                 { 
                       
                  $postdata[] = $v;

                 }
          }

               $new = array();
              foreach ($postdata as $value)
              {
                  $new[$value['business_profile_post_id']] = $value;
              }

            $post = array();
               
                            foreach ($new as $key => $row){

                          $post[$key] = $row['business_profile_post_id'];
                    

                         }
             array_multisort($post, SORT_DESC, $new);

           $this->data['businessprofiledata'] = $new;

          //echo "<pre>"; print_r($this->data['businessprofiledata']) ; die();





           $contition_array = array('status' => '1', 'is_deleted' => '0');


         $businessdata= $this->data['results'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = 'company_name,other_industrial,other_business_type', $sortby = '', $orderby = '', $limit = '', $offset = '', $$join_str = array(), $groupby);
         // echo "<pre>";print_r($businessdata);die();


           $contition_array = array('status' => '1', 'is_delete' => '0');


         $businesstype= $this->data['results'] =  $this->common->select_data_by_condition('business_type', $contition_array, $data = 'business_name', $sortby = '', $orderby = '', $limit = '', $offset = '', $$join_str = array(), $groupby);
         // echo "<pre>";print_r($businesstype);

           $contition_array = array('status' => '1', 'is_delete' => '0');


         $industrytype= $this->data['results'] =  $this->common->select_data_by_condition('industry_type', $contition_array, $data = 'industry_name', $sortby = '', $orderby = '', $limit = '', $offset = '', $$join_str = array(), $groupby);
          // echo "<pre>";print_r($industrytype);die();
 $unique=array_merge($businessdata,$businesstype,$industrytype);
  foreach($unique as $key=>$value){
          foreach($value as $ke=>$val){
            if($val != ""){


            $result[] = $val;
          }
          }
          }

          $this->data['demo']=$result; 
             // echo '<pre>'; print_r($result); die();






           $this->load->view('business_profile/business_profile_post',$this->data);
        }

      public function business_profile_manage_post($id){
        
        // manage post start
            $userid = $this->session->userdata('aileenuser');
            $user_name = $this->session->userdata('user_name');


            $contition_array = array('user_id' => $userid, 'status' => '1');
             $this->data['slug_data'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');
             //echo "<pre>"; print_r($this->data['slug_data']); die();
            $slug_id = $this->data['slug_data'][0]['business_slug'];
            //echo  $slug_id ; die();
            if($id == $slug_id || $id == ''){
         
            $contition_array = array('business_slug' => $slug_id, 'status' =>'1' );
             $this->data['businessdata'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');
            
               $contition_array = array('user_id' => $userid, 'status' => 1, 'is_delete' => '0');

           $this->data['business_profile_data'] = $this->common->select_data_by_condition('business_profile_post', $contition_array, $data, $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');

           //echo "<pre>"; print_r($this->data['business_profile_data']); die();
         }
         else
         {

            $contition_array = array('business_slug' => $id, 'status' =>'1' );
             $this->data['businessdata'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');
            
              $contition_array = array('user_id' => $id, 'status' => 1, 'is_delete' => '0');

           
           $this->data['business_profile_data'] = $this->common->select_data_by_condition('business_profile_post', $contition_array, $data, $sortby = 'business_profile_post_id', $orderby = 'DESC', $limit = '', $offset = '', $join_str = array(), $groupby = '');

         }

    //manage post end

    //save post start

              $userid = $this->session->userdata('aileenuser');
            $user_name = $this->session->userdata('user_name'); 
          
            $contition_array = array('user_id' => $userid);
             $this->data['businessdata'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

             $contition_array = array('user_id' => $userid);
            $savedata = $this->data['savedata'] =  $this->common->select_data_by_condition('business_profile_save', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

             //echo "<pre>"; print_r($this->data['savedata']); die();

             foreach ($savedata as $savepost) {

              $savepostid = $savepost['post_id'];

            $contition_array = array('business_profile_post_id' => $savepostid, 'status'=> '1');
           $this->data['business_post_data'] = $this->common->select_data_by_condition('business_profile_post', $contition_array, $data, $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');
               
               $businesssavepost[] = $this->data['business_post_data'];
             }

             $this->data['business_save_data'] = $businesssavepost;



           $contition_array = array('status' => '1', 'is_deleted' => '0');


         $businessdata= $this->data['results'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = 'company_name,other_industrial,other_business_type', $sortby = '', $orderby = '', $limit = '', $offset = '', $$join_str = array(), $groupby);
         // echo "<pre>";print_r($businessdata);die();


           $contition_array = array('status' => '1', 'is_delete' => '0');


         $businesstype= $this->data['results'] =  $this->common->select_data_by_condition('business_type', $contition_array, $data = 'business_name', $sortby = '', $orderby = '', $limit = '', $offset = '', $$join_str = array(), $groupby);
         // echo "<pre>";print_r($businesstype);

           $contition_array = array('status' => '1', 'is_delete' => '0');


         $industrytype= $this->data['results'] =  $this->common->select_data_by_condition('industry_type', $contition_array, $data = 'industry_name', $sortby = '', $orderby = '', $limit = '', $offset = '', $$join_str = array(), $groupby);
          // echo "<pre>";print_r($industrytype);die();
 $unique=array_merge($businessdata,$businesstype,$industrytype);
  foreach($unique as $key=>$value){
          foreach($value as $ke=>$val){
            if($val != ""){


            $result[] = $val;
          }
          }
          }

          $this->data['demo']=$result; 

             //echo "<pre>"; print_r($this->data['business_profile_data']); die();
            $this->load->view('business_profile/business_profile_manage_post',$this->data);

     // save post end       
      }


  public function business_profile_deletepost()
      { 

        $id =  $_POST["business_profile_post_id"]; 
        //echo $id; die();
       $data = array(  
                     'is_delete' => 1,
                     'modify_date' => date('Y-m-d',time())
                     ); 
                   
          
      $updatdata =   $this->common->update_data($data,'business_profile_post','business_profile_post_id',$id);                
      }

        public function business_profile_addpost()
        {
           $userid = $this->session->userdata('aileenuser');
            $contition_array = array('user_id' => $userid, 'status' =>'1');
             $this->data['businessdata'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

            $this->load->view('business_profile/business_profile_addpost',$this->data); 
        }


        public function business_profile_addpost_insert(){ 
         
            
            $userid  = $this->session->userdata('aileenuser');

          
         $data = array(             
          'product_name' => $this->input->post('my_text'),
          'product_description' => $this->input->post('product_desc'),
          'created_date' => date('Y-m-d',time()),
          'status'=>1,
          'is_delete'=>0,
          'user_id' => $userid
               ); 

      $insert_id =  $this->common->insert_data_getid($data,'business_profile_post');
     //echo $insert_id; die(); 
       $config = array(
                'upload_path' => 'uploads/bus_post_image/',
                'max_size' => 2500000000000,
                'allowed_types' => 'gif|jpeg|jpg|png|pdf|mp4|mp3',
                'overwrite' => true,
                'remove_spaces' => true);
            $images = array();
            $this->load->library('upload');

            $files = $_FILES;
        $count = count($_FILES['postattach']['name']); 
      
         for ($i = 0; $i < $count; $i++) { 

        $_FILES['postattach']['name'] = $files['postattach']['name'][$i];
        $_FILES['postattach']['type'] = $files['postattach']['type'][$i];
        $_FILES['postattach']['tmp_name'] = $files['postattach']['tmp_name'][$i];
        $_FILES['postattach']['error'] = $files['postattach']['error'][$i];
        $_FILES['postattach']['size'] = $files['postattach']['size'][$i];

                $fileName = $title . '_' . $_FILES['postattach']['name'];
                $images[] = $fileName;
               $config['file_name'] = $fileName; 
                
                $this->upload->initialize($config);
                $this->upload->do_upload();
                if ($this->upload->do_upload('postattach')) {//echo "hello"; die();
                    $return['data'][] = $this->upload->data();
                    $return['status'] = "success";
                    $return['msg'] = sprintf($this->lang->line('success_item_added'), "Image", "uploaded");
                    
                     $data = array(
                                    'image_name' => $fileName,
                                    'image_type' => 2,
                                    'post_id' => $insert_id
                                    ); 

      $insert_id =  $this->common->insert_data_getid($data,'post_image');
               
                } else { 
                
                  redirect('business_profile/business_profile_post', refresh);
                    
                }

            }

            redirect('business_profile/business_profile_post', refresh);
    // new code end
        }


        public function business_profile_editpost($id)
        { 
            $contition_array = array( 'business_profile_post_id' => $id);
             $this->data['business_profile_data'] =  $this->common->select_data_by_condition('business_profile_post', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

            $this->load->view('business_profile/business_profile_editpost',$this->data); 
        }

        public function business_profile_editpost_insert($id){

            
                 $editimage = $this->input->post('hiddenimg');

                 $this->form_validation->set_rules('productname', 'Product name', 'required');
                 $this->form_validation->set_rules('description', 'Description', 'required');
                 if ($this->form_validation->run() == FALSE) { 
                 $this->load->view('business_profile/business_profile_editpost'); 
                } 
              else{

            
                    $config['upload_path'] = 'uploads/business_profile_images/';
                    $config['allowed_types'] = 'jpg|jpeg|png|gif|mp4|3gp|pdf';
                   
                    $config['file_name'] = $_FILES['image']['name'];
                    $config['upload_max_filesize'] = '40M' ;
                    
                    //Load upload library and initialize configuration
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    
                    if($this->upload->do_upload('image'))
                    {
                        $uploadData = $this->upload->data();
                       
                        $picture = $uploadData['file_name'];
                    }
                    else
                    {
                        $picture = '';
                    }
              

            		if($picture){ 

                    $data = array(
                     'product_name' => $this->input->post('productname'),
                     'product_image' => $picture,
                     'product_description' => $this->input->post('description'),
                     'modify_date' => date('Y-m-d',time())
            ); 
                }else{ 
                	$data = array(
                     'product_name' => $this->input->post('productname'),
                     'product_image' => $this->input->post('hiddenimg'),
                     'product_description' => $this->input->post('description'),
                     'modify_date' => date('Y-m-d',time())
            ); 
                }
           
      $updatdata =   $this->common->update_data($data,'business_profile_post','business_profile_post_id',$id);

          if($updatdata){ 
            redirect('business_profile/business_profile_manage_post', refresh);
           }
            else{
             $this->session->flashdata('error','Your data not inserted');
                   redirect('business_profile/business_profile_editpost', refresh);
          }
        }
    }
  
//business_profile_contactperson start


    public function business_profile_contactperson($id)
        {
            
            $userid = $this->session->userdata('aileenuser');
            $contition_array = array('user_id' => $id, 'status' =>'1');
             $this->data['contactperson'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

            $this->load->view('business_profile/business_profile_contactperson',$this->data); 
        }


//business_profile_contactperson _query

    public function business_profile_contactperson_query($id)
        { 
            $userid  = $this->session->userdata('aileenuser');

             $contition_array = array('user_id' => $id);
             $this->data['contactperson'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');
                        
            $email= $this->input->post('email');
            
            $toemail= $this->input->post('toemail');
             
            $userdata = $this->common->select_data_by_id('user','user_id', $userid, $data = '*', $join_str = array());


            $msg = 'Hey !' . " " .$this->data['contactperson'][0]['contact_person'] ."<br/>". 
            $msg .=  $userdata[0]['first_name'] .$userdata[0]['last_name'].'('.$userdata[0]['user_email'] .')'. ',';
            $msg .= 'this person wants to contact with you!!';$msg .= "<br>";
            $msg .= $this->input->post('msg');
            $from='raval.khyati13@gmail.com';

               
           
            $subject = "contact message";
           
          
            $mail = $this->email_model->do_email($msg, $subject,$toemail,$from);


            //insert contact start


            $data = array(
                        'contact_from_id' => $userid,
                        'contact_to_id' =>$id,
                        'contact_type' => 2,
                        'created_date' => date('Y-m-d',time()),
                        'status'=>1,
                        'is_delete'=> $userid,
                        'contact_desc'=>$this->input->post('msg')      
                );

               
   $insertdata =   $this->common->insert_data_getid($data,'contact_person');
   

//insert contact person end 
//insert contact person notification start


                     $data = array(
                                    'not_type' => 7,
                                    'not_from_id' => $userid,
                                    'not_to_id' => $id,
                                    'not_read' => 2,
                                    'not_product_id' => $insertdata,
                                    'not_from' => 3
                                    ); 

    $insert_id =  $this->common->insert_data_getid($data,'notification');

            if($insertdata){ 
               
                $this->session->set_flashdata('success', 'contacted successfully');
                   redirect('business_profile/business_profile_post','refresh'); 
              }else{
                 $this->session->flashdata('error','Your data not inserted');
                       redirect('business_profile/business_profile_contactperson/'.$id, refresh);
              } 
//insert contact person notifiaction end   
          
            }


        public function business_profile_save_post()
        {
           $userid = $this->session->userdata('aileenuser');
            $user_name = $this->session->userdata('user_name'); 
          
            $contition_array = array('user_id' => $userid);
             $this->data['businessdata'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

             $contition_array = array('user_id' => $userid);
            $savedata = $this->data['savedata'] =  $this->common->select_data_by_condition('business_profile_save', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

             //echo "<pre>"; print_r($this->data['savedata']); die();

             foreach ($savedata as $savepost) {

              $savepostid = $savepost['post_id'];

            $contition_array = array('business_profile_post_id' => $savepostid, 'status'=> '1');
           $this->data['business_post_data'] = $this->common->select_data_by_condition('business_profile_post', $contition_array, $data, $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');
               
               $businesssavepost[] = $this->data['business_post_data'];
             }

             $this->data['business_profile_data'] = $businesssavepost;

             //echo "<pre>"; print_r($this->data['business_profile_data']); die();
            $this->load->view('business_profile/business_profile_manage_post',$this->data);
        }

        public function user_image_insert() { 


        $userid = $this->session->userdata('aileenuser');
        

            if($this->input->post('cancel2')){  
                redirect('business_profile/business_profile_post', refresh);
              }
            elseif ($this->input->post('cancel1')) {
              redirect('business_profile/business_profile_save_post', refresh);
            }
            elseif ($this->input->post('cancel3')) {
              redirect('business_profile/business_profile_addpost', refresh);
            }
            elseif ($this->input->post('cancel4')) {
              redirect('business_profile/business_resume', refresh);
            }
            elseif ($this->input->post('cancel5')) {
              redirect('business_profile/business_profile_manage_post', refresh);
            }
            elseif ($this->input->post('cancel6')) {
              redirect('business_profile/userlist', refresh);
            }
            elseif ($this->input->post('cancel7')) {
              redirect('business_profile/followers', refresh);
            }
            elseif ($this->input->post('cancel8')) {
              redirect('business_profile/following', refresh);
            }

             if (empty($_FILES['profilepic']['name']))
             { 
                 $this->form_validation->set_rules('profilepic', 'Upload profilepic', 'required');
            
            }
            else
            { 
                $config['upload_path'] = 'uploads/user_image/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif|mp4|3gp|mpeg|mpg|mpe|qt|mov|avi|pdf';
              
                $config['file_name'] = $_FILES['profilepic']['name'];
               
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('profilepic'))
                {
                    $uploadData = $this->upload->data();
                    $picture = $uploadData['file_name'];
                }
                else
                {
                    $picture = '';
                }

                $data = array(
                    
                    'business_user_image' =>$picture,
                    'modified_date' => date('Y-m-d',time())
            ); 
                
       
      $updatdata =   $this->common->update_data($data,'business_profile','user_id',$userid);

          if($updatdata){ 
                if($this->input->post('hitext') == 1){ 
                redirect('business_profile/business_profile_save_post', refresh);
                }
                elseif($this->input->post('hitext') == 2)
                {
                 redirect('business_profile/business_profile_post', refresh);
                }
                elseif($this->input->post('hitext') == 3)
                {
                 redirect('business_profile/business_profile_addpost', refresh);
                }
                elseif($this->input->post('hitext') == 4)
                {
                 redirect('business_profile/business_resume', refresh);
                }
                elseif($this->input->post('hitext') == 5)
                {
                 redirect('business_profile/business_profile_manage_post', refresh);
                }
                elseif($this->input->post('hitext') == 6)
                {
                 redirect('business_profile/userlist', refresh);
                }
                elseif($this->input->post('hitext') == 7)
                {
                 redirect('business_profile/followers', refresh);
                }
                elseif($this->input->post('hitext') == 8)
                {
                 redirect('business_profile/following', refresh);
                }

          }else{
             $this->session->flashdata('error','Your data not inserted');
                   redirect('business_profile/business_profile_post', refresh);
          }
        }
    }

      public function business_resume($id) { 

       $userid = $this->session->userdata('aileenuser');


         $contition_array = array('user_id' => $userid, 'status' => '1');
             $this->data['slug_data'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');
            
            $slug_id = $this->data['slug_data'][0]['business_slug'];

        if($id == $this->data['slug_data'][0]['business_slug'] || $id == ''){
         $contition_array = array('business_slug' => $slug_id, 'status' => '1');
             $this->data['businessdata'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');
           }
           else{

             $contition_array = array('business_slug' => $id, 'status' => '1');
             $this->data['businessdata'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

           }

             
         $contition_array = array('status' => '1', 'is_deleted' => '0');


         $businessdata= $this->data['results'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = 'company_name,other_industrial,other_business_type', $sortby = '', $orderby = '', $limit = '', $offset = '', $$join_str = array(), $groupby);
         // echo "<pre>";print_r($businessdata);die();


           $contition_array = array('status' => '1', 'is_delete' => '0');


         $businesstype= $this->data['results'] =  $this->common->select_data_by_condition('business_type', $contition_array, $data = 'business_name', $sortby = '', $orderby = '', $limit = '', $offset = '', $$join_str = array(), $groupby);
         // echo "<pre>";print_r($businesstype);

           $contition_array = array('status' => '1', 'is_delete' => '0');


         $industrytype= $this->data['results'] =  $this->common->select_data_by_condition('industry_type', $contition_array, $data = 'industry_name', $sortby = '', $orderby = '', $limit = '', $offset = '', $$join_str = array(), $groupby);
          // echo "<pre>";print_r($industrytype);die();
 $unique=array_merge($businessdata,$businesstype,$industrytype);
  foreach($unique as $key=>$value){
          foreach($value as $ke=>$val){
            if($val != ""){


            $result[] = $val;
          }
          }
          }

          $this->data['demo']=$result; 






             
        
        $this->load->view('business_profile/business_resume', $this->data);

      }

     public function business_user_post($id){
               
            $this->data['userid'] = $id;
             $userid = $this->session->userdata('aileenuser');
            $user_name = $this->session->userdata('user_name');

            $contition_array = array('user_id' => $userid, 'status' =>'1');
             $this->data['businessdata'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

            

            $this->data['usdata'] =  $this->common->select_data_by_id('user', 'user_id', $id, $data = '*', $join_str = array());


            $contition_array = array( 'user_id' => $id);
           $this->data['business_profile_data'] = $this->common->select_data_by_condition('business_profile_post', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');
            
            $this->load->view('business_profile/business_profile_manage_post', $this->data);
        }

  //Business Profile Save Post Start
  public function business_profile_save(){

            $id = $_POST['business_profile_post_id'];
    
         $userid = $this->session->userdata('aileenuser');

        $contition_array = array('post_id'=>$id,'user_id'=> $userid,'is_delete' => 0);
        $userdata =  $this->common->select_data_by_condition('business_profile_save', $contition_array, $data = '*', $sortby = '', $orderby = 'desc', $limit = '', $offset = '', $join_str = array(), $groupby = ''); 
       
        $save_id=$userdata[0]['save_id'];
        
         if($userdata)
         {

             $contition_array = array('business_delete' => 1);
             $jobdata =  $this->common->select_data_by_condition('business_profile_save', $contition_array, $data = 'save_id', $sortby = '', $orderby = 'desc', $limit = '', $offset = '', $join_str = array(), $groupby = ''); 
           
                $data = array(
                  
                 'business_delete'=> 0 ,
                  'business_save'=> 1
                    ); 
                 
                   
                  $updatedata =  $this->common->update_data($data,'business_profile_save','save_id',$save_id);
                 

                  if($updatedata){ 
                     
                    //$savepost = '<div> Saved Post </div>';
                     $savepost.= '<i class="fa fa-bookmark" aria-hidden="true"></i>';
                     $savepost.='Saved Post';
                     //$savepost .= '</a>';      
                      echo $savepost;
                        
                   
                  }
         }
         else
        {
       
            $data = array(
                'post_id'=> $id,
                'user_id' => $userid,
                 'created_date'=> date('Y-m-d h:i:s',time()),
                  'is_delete'=> 0,
                  'business_save'=> 1,
                  'business_delete'=> 0 
        ); 
      
           
      $insert_id =   $this->common->insert_data_getid($data,'business_profile_save'); 
                if($insert_id)
                { 
                     //$savepost = '<div> Saved Post </div>';
                     $savepost.= '<i class="fa fa-bookmark" aria-hidden="true"></i>';
                     $savepost.='Saved Post';      
                    echo $savepost;  
                  
                  
                }
        }

  }

  //Business Profile Save Post End

  //Business Profile Remove Save Post Start
public function business_profile_delete($id){

   $id = $_POST['save_id'];
  $userid = $this->session->userdata('aileenuser');

            $data = array(
                    'business_save'=> 0,
                   'business_delete'=> 1,
                  'modify_date' => date('Y-m-d h:i:s',time())
                
        ); 
      
      $updatedata =   $this->common->update_data($data,'business_profile_save','save_id',$id);
      

}

  //Business Profile Remove Save Post Start

//location automatic retrieve controller start
public function location()
    {
        $json = [];

       

        if(!empty($this->input->get("q"))){
            $this->db->like('city_name', $this->input->get("q"));
            $query = $this->db->select('city_id as id,city_name as text')
                        ->order_by("city_name", "asc")
                        ->limit(10)
                        ->get("cities");
            $json = $query->result();
        }

        
        echo json_encode($json);
        
    }
//location automatic retrieve controller End
 
 // user list of artistic users

public function userlist()
    {
         $this->data['userid'] =  $userid = $this->session->userdata('aileenuser');
            $artdata = $this->data['artdata'] =    $this->common->select_data_by_id('business_profile', 'user_id', $userid, $data = '*');

         $contition_array = array('user_id' => $userid);
             $this->data['artisticdata'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

        
         $contition_array = array('business_step' =>5, 'is_deleted' => 0, 'status' => 1 , 'user_id !=' => $userid);
             $this->data['userlist'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

             // followers count
             $join_str[0]['table'] = 'follow';
             $join_str[0]['join_table_id'] = 'follow.follow_to';
            $join_str[0]['from_table_id'] = 'business_profile.business_profile_id';
             $join_str[0]['join_type'] = '';
             $contition_array = array('follow_to' => $artdata[0]['business_profile_id'], 'follow_status' => 1,'follow_type' =>1);
            
             $this->data['followers'] =  count($this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str, $groupby = ''));

             // follow count end

             // fllowing count
             $join_str[0]['table'] = 'follow';
             $join_str[0]['join_table_id'] = 'follow.follow_from';
             $join_str[0]['from_table_id'] = 'business_profile.business_profile_id';
             $join_str[0]['join_type'] = '';

 $contition_array = array('follow_from' =>$artdata[0]['business_profile_id'], 'follow_status' => 1,'follow_type' => 1);
            
             $this->data['following'] =  count($this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str, $groupby = ''));

             //following end
      
 $contition_array = array('status' => '1', 'is_deleted' => '0');


         $businessdata= $this->data['results'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = 'company_name,other_industrial,other_business_type', $sortby = '', $orderby = '', $limit = '', $offset = '', $$join_str = array(), $groupby);
         // echo "<pre>";print_r($businessdata);die();


           $contition_array = array('status' => '1', 'is_delete' => '0');


         $businesstype= $this->data['results'] =  $this->common->select_data_by_condition('business_type', $contition_array, $data = 'business_name', $sortby = '', $orderby = '', $limit = '', $offset = '', $$join_str = array(), $groupby);
         // echo "<pre>";print_r($businesstype);

           $contition_array = array('status' => '1', 'is_delete' => '0');


         $industrytype= $this->data['results'] =  $this->common->select_data_by_condition('industry_type', $contition_array, $data = 'industry_name', $sortby = '', $orderby = '', $limit = '', $offset = '', $$join_str = array(), $groupby);
          // echo "<pre>";print_r($industrytype);die();
 $unique=array_merge($businessdata,$businesstype,$industrytype);
  foreach($unique as $key=>$value){
          foreach($value as $ke=>$val){
            if($val != ""){


            $result[] = $val;
          }
          }
          }

          $this->data['demo']=$result; 







      
      $this->load->view('business_profile/business_userlist', $this->data);
    
    }


    public function follow()
    { 
          $userid = $this->session->userdata('aileenuser');

           $business_id =  $_POST["follow_to"];

          $contition_array = array('user_id' => $userid, 'is_deleted' => 0, 'status' => 1);

         $artdata = $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

    $contition_array = array('follow_type' => 2, 'follow_from' => $artdata[0]['business_profile_id'], 'follow_to' => $business_id);
             $follow =  $this->common->select_data_by_condition('follow', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

        
     if($follow){
       $data = array(
            'follow_type' => 2,
            'follow_from' => $artdata[0]['business_profile_id'],
            'follow_to' => $business_id,
            'follow_status' => 1,
            );
       $update = $this->common->update_data($data,'follow','follow_id',$follow[0]['follow_id']);

       // insert notification

           $data = array(
                'not_type' => 8,
                'not_from_id' => $userid,
                'not_to_id' => $business_id,
                'not_read' => 2,
                'not_product_id' => $follow[0]['follow_id'],
                'not_from' => 6
                ); 
           
        $insert_id =  $this->common->insert_data_getid($data,'notification'); 
        // end notoification

       if($update){
         
         	$follow = '<div>';
             $follow = '<button id="unfollow' . $business_id.'" onClick="unfollowuser('.$business_id.')">
                              Following
                      </button>';
              $follow .= '</div>';
              echo $follow;
       }
       }else{
        $data = array(
            'follow_type' => 2,
            'follow_from' => $artdata[0]['business_profile_id'],
            'follow_to' => $business_id,
            'follow_status' => 1,
            );
       $insert =   $this->common->insert_data($data,'follow'); 

        // insert notification

           $data = array(
                'not_type' => 8,
                'not_from_id' => $userid,
                'not_to_id' => $business_id,
                'not_read' => 2,
                'not_product_id' => $insert,
                'not_from' => 6
                ); 
           
        $insert_id =  $this->common->insert_data_getid($data,'notification'); 
        // end notoification
       if($insert){
         $follow = '<div>';
             $follow = '<button id="unfollow' . $business_id.'" onClick="unfollowuser('.$business_id.')">
                               Following
                      </button>';
             $follow .= '</div>';
              echo $follow;
       }
     }
    }

     public function unfollow()
    {  
          $userid = $this->session->userdata('aileenuser');

         $business_id =  $_POST["follow_to"];

       $contition_array = array('user_id' => $userid, 'is_deleted' => 0, 'status' => 1);

         $artdata = $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

$contition_array = array('follow_type' => 2, 'follow_from' => $artdata[0]['business_profile_id'], 'follow_to' => $business_id);
            
             $follow =  $this->common->select_data_by_condition('follow', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

        
     if($follow){
       $data = array(
            'follow_type' => 2,
            'follow_from' => $artdata[0]['business_profile_id'],
            'follow_to' => $business_id,
            'follow_status' => 0,
            );
       $update = $this->common->update_data($data,'follow','follow_id',$follow[0]['follow_id']);
       if($update){

       	 $unfollow = '<div>';
         $unfollow = '<button id="follow' . $business_id.'" onClick="followuser('.$business_id.')">
                               Follow 
                      </button>';
          $unfollow .= '</div>';   
              echo $unfollow;
       }
       }
    }

    public function followers()
    {
         $this->data['userid'] =  $userid = $this->session->userdata('aileenuser');

$contition_array = array('user_id' => $userid, 'is_deleted' => 0, 'status' => 1);

         $artdata=  $this->data['artisticdata'] = $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');


             $join_str[0]['table'] = 'follow';
             $join_str[0]['join_table_id'] = 'follow.follow_to';
             $join_str[0]['from_table_id'] = 'business_profile.business_profile_id';
             $join_str[0]['join_type'] = '';

         $contition_array = array('follow_to' => $artdata[0]['business_profile_id'], 'follow_status' => 1,'follow_type' =>2);
            
             $this->data['userlist'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str, $groupby = '');


 $contition_array = array('status' => '1', 'is_deleted' => '0');


         $businessdata= $this->data['results'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = 'company_name,other_industrial,other_business_type', $sortby = '', $orderby = '', $limit = '', $offset = '', $$join_str = array(), $groupby);
         // echo "<pre>";print_r($businessdata);die();


           $contition_array = array('status' => '1', 'is_delete' => '0');


         $businesstype= $this->data['results'] =  $this->common->select_data_by_condition('business_type', $contition_array, $data = 'business_name', $sortby = '', $orderby = '', $limit = '', $offset = '', $$join_str = array(), $groupby);
         // echo "<pre>";print_r($businesstype);

           $contition_array = array('status' => '1', 'is_delete' => '0');


         $industrytype= $this->data['results'] =  $this->common->select_data_by_condition('industry_type', $contition_array, $data = 'industry_name', $sortby = '', $orderby = '', $limit = '', $offset = '', $$join_str = array(), $groupby);
          // echo "<pre>";print_r($industrytype);die();
 $unique=array_merge($businessdata,$businesstype,$industrytype);
  foreach($unique as $key=>$value){
          foreach($value as $ke=>$val){
            if($val != ""){


            $result[] = $val;
          }
          }
          }

          $this->data['demo']=$result; 





 
              
      $this->load->view('business_profile/business_followers', $this->data);
    
    }



public function following()
    {
         $this->data['userid'] =  $userid = $this->session->userdata('aileenuser');
       
       $contition_array = array('user_id' => $userid, 'is_deleted' => 0, 'status' => 1);

         $artdata=  $this->data['artisticdata'] = $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

         $contition_array = array('user_id' => $userid);
             $this->data['artisticdata'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

        
             $join_str[0]['table'] = 'follow';
             $join_str[0]['join_table_id'] = 'follow.follow_from';
             $join_str[0]['from_table_id'] = 'business_profile.business_profile_id';
             $join_str[0]['join_type'] = '';

         $contition_array = array('follow_from' =>$artdata[0]['business_profile_id'], 'follow_status' => 1,'follow_type' => 2);
            
             $this->data['userlist'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str, $groupby = '');


 $contition_array = array('status' => '1', 'is_deleted' => '0');


         $businessdata= $this->data['results'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = 'company_name,other_industrial,other_business_type', $sortby = '', $orderby = '', $limit = '', $offset = '', $$join_str = array(), $groupby);
         // echo "<pre>";print_r($businessdata);die();


           $contition_array = array('status' => '1', 'is_delete' => '0');


         $businesstype= $this->data['results'] =  $this->common->select_data_by_condition('business_type', $contition_array, $data = 'business_name', $sortby = '', $orderby = '', $limit = '', $offset = '', $$join_str = array(), $groupby);
         // echo "<pre>";print_r($businesstype);

           $contition_array = array('status' => '1', 'is_delete' => '0');


         $industrytype= $this->data['results'] =  $this->common->select_data_by_condition('industry_type', $contition_array, $data = 'industry_name', $sortby = '', $orderby = '', $limit = '', $offset = '', $$join_str = array(), $groupby);
          // echo "<pre>";print_r($industrytype);die();
 $unique=array_merge($businessdata,$businesstype,$industrytype);
  foreach($unique as $key=>$value){
          foreach($value as $ke=>$val){
            if($val != ""){


            $result[] = $val;
          }
          }
          }

          $this->data['demo']=$result; 




             
             
 
      $this->load->view('business_profile/business_following', $this->data);
    
    }





// end of user list

     //deactivate user start
    public function deactivate($id)
    {
     

       $data = array(
                'status' => 0         
                 ); 
             
        $update =   $this->common->update_data($data,'business_profile','user_id',$id);
     
       if($update){ 

           
            $this->session->set_flashdata('success', 'You are deactivate successfully.');
             redirect('dashboard', 'refresh');
       }else{
                $this->session->flashdata('error','Sorry!! Your are not deactivate!!');
               redirect('business_profile', 'refresh');
       }
    }


// deactivate user end

 public function image_upload_ajax(){
  
 

session_start();


  $session_uid = $this->session->userdata('aileenuser');


include_once 'getExtension.php';

$valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP"); 
if(isset($_POST) && $_SERVER['REQUEST_METHOD'] == "POST" && isset($session_uid))
{ 
$name = $_FILES['photoimg']['name'];
$size = $_FILES['photoimg']['size'];

if($name)
{
$ext = $this->common->getExtension($name); 
if(in_array($ext,$valid_formats))
{
if($size<(1024*1024))
{
$actual_image_name = time().$session_uid.".".$ext;
$tmp = $_FILES['photoimg']['tmp_name'];
$bgSave='<div id="uX'.$session_uid.'" class="bgSave wallbutton blackButton">Save Cover</div>';


// khyati start


                $config['upload_path'] = 'uploads/user_image/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif|mp4|3gp|mpeg|mpg|mpe|qt|mov|avi|pdf';
             
                $config['file_name'] = $_FILES['photoimg']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
               
                if($this->upload->do_upload('photoimg'))
                {
                    $uploadData = $this->upload->data();
                   
                     $picture = $uploadData['file_name']; 
                }
                else
                {
                    $picture = '';
                }


     $data = array(
                'profile_background' => $picture
                  ); 
       
        $update = $this->common->update_data($data,'business_profile','user_id',$session_uid); 
if($update){
    $path= base_url('uploads/user_image/');
echo $bgSave.'<img src="'.$path.$picture.'"  id="timelineBGload" class="headerimage ui-corner-all" style="top:0px"/>';

}               
else
{
echo "Fail upload folder with read access.";
}
}
else
echo "Image file size max 1 MB";
}
else
echo "Invalid file format.";
} 

else
echo "Please select image..!";

exit;

}

}

public function image_saveBG_ajax(){
  
 

session_start();

 $session_uid = $this->session->userdata('aileenuser');

if(isset($_POST['position']) && isset($session_uid))
{

$position=$_POST['position'];

 $data = array(
                'profile_background_position' => $position
                  ); 
       
        $update = $this->common->update_data($data,'business_profile','user_id',$session_uid);
if($update){
   
echo $position;


}
}
 

}

    // khyati change end 15 2 


function slug_script(){

        $this->db->select('business_profile_id,company_name');
        $res = $this->db->get('business_profile')->result();
        foreach ($res as $k => $v){
            $data = array('business_slug'=>$this->setcategory_slug($v->company_name,'business_slug','business_profile'));
            $this->db->where('business_profile_id',$v->business_profile_id);
            $this->db->update('business_profile', $data);
        }
        echo "yes";
    }


// create pdf start

  public function creat_pdf($id)
    {
         
           $contition_array = array('business_profile_post_id' => $id, 'status' =>'1');
          $this->data['businessdata'] = $this->common->select_data_by_condition('business_profile_post', $contition_array , $data, $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');
          $this->load->view('business_profile/business_pdfdispaly', $this->data);

    }
//create pdf end

    // cover pic controller

 public function ajaxpro()
    {  
        $userid = $this->session->userdata('aileenuser');
        
      $data = $_POST['image'];


$imageName = time().'.png';
      $base64string = $data;
file_put_contents('uploads/bus_bg/'.$imageName, base64_decode(explode(',',$base64string)[1]));
      
      $data = array(
                'profile_background' => $imageName
                  ); 
       
        $update = $this->common->update_data($data,'business_profile','user_id',$userid);

        $this->data['busdata'] = $this->common->select_data_by_id('business_profile', 'user_id', $userid, $data = '*', $join_str = array());

echo '<img src="' . $this->data['busdata'][0]['profile_background'] . '" />';
    }


public function imagedata()
    {  
             $userid = $this->session->userdata('aileenuser');

                $config['upload_path'] = 'uploads/bus_bg';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
             
                $config['file_name'] = $_FILES['image']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
               
                if($this->upload->do_upload('image'))
                {
                      
                    $uploadData = $this->upload->data();
                   
                    $image = $uploadData['file_name'];
                    
                }
                else
                {
                   
                    $image = '';
                }


        $data = array(
                  'profile_background_main' => $image,
                  'modified_date' => date('Y-m-d h:i:s',time())
                  
        ); 
     
           
      $updatedata =   $this->common->update_data($data,'business_profile','user_id',$userid);

      if($updatedata){ 
        echo $userid;
      }else{
       echo "welcome";
      }

    }

    // cover pic end

// busienss_profile like comment ajax start

public function like_comment()
        {

          $userid = $this->session->userdata('aileenuser');
          $post_id =  $_POST["post_id"];  
          

          $contition_array = array('business_profile_post_comment_id' =>  $_POST["post_id"], 'status' =>'1');
          $businessprofiledata =   $this->data['businessprofiledata'] = $this->common->select_data_by_condition('business_profile_post_comment', $contition_array , $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');
          

            $business_comment_likes_count = $businessprofiledata[0]['business_comment_likes_count'];
            $likeuserarray = explode(',', $businessprofiledata[0]['business_comment_like_user']);
            
            if(!in_array($userid, $likeuserarray)){ //echo "falguni"; die();
            $user_array =   array_push($likeuserarray,$userid);

                if($businessprofiledata[0]['business_comment_likes_count'] == 0){  
                    $userid = implode('', $likeuserarray);
                }else{  
                    $userid = implode(',', $likeuserarray);
                    }

               $data = array(
                    'business_comment_likes_count' => $business_comment_likes_count + 1,
                    'business_comment_like_user' =>   $userid,
                    'modify_date' => date('y-m-d h:i:s')
                  ); 
              
               

            $updatdata =   $this->common->update_data($data,'business_profile_post_comment','business_profile_post_comment_id',$post_id );
             $contition_array = array('business_profile_post_comment_id' =>  $_POST["post_id"], 'status' =>'1');
          $businessprofiledata1 =   $this->data['businessprofiledata1'] = $this->common->select_data_by_condition('business_profile_post_comment', $contition_array , $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');

            if($updatdata){
              

              $cmtlike1 = '<a id="'. $businessprofiledata1[0]['business_profile_post_comment_id'].'" onClick="comment_like(this.id)">';
              $cmtlike1 .= ' <i class="fa fa-thumbs-up" aria-hidden="true">';
              $cmtlike1 .= '</i>';
              $cmtlike1 .= '<span>';
              if($businessprofiledata1[0]['business_comment_likes_count'] > 0){
              $cmtlike1 .= $businessprofiledata1[0]['business_comment_likes_count'] . '';
                }
              $cmtlike1 .= '</span>';
              $cmtlike1 .= '</a>';
              echo $cmtlike1;
            }else{ 
          }
              
            }else{  
              
              foreach($likeuserarray as $key=>$val){
                if($val==$userid){ 
             $user_array =  array_splice($likeuserarray,$key,1); 
                }
             } 
            
                $data = array(
                    'business_comment_likes_count' => $business_comment_likes_count - 1,
                    'business_comment_like_user' =>   implode(',', $likeuserarray),
                    'modify_date' => date('y-m-d h:i:s')
                  ); 
             

            $updatdata =   $this->common->update_data($data,'business_profile_post_comment','business_profile_post_comment_id',$post_id);
             $contition_array = array('business_profile_post_comment_id' =>  $_POST["post_id"], 'status' =>'1');
          $businessprofiledata2 =   $this->data['businessprofiledata2'] = $this->common->select_data_by_condition('business_profile_post_comment', $contition_array , $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');

                       if($updatdata){
                     
                      $cmtlike1 = '<a id="'. $businessprofiledata2[0]['business_profile_post_comment_id'].'" onClick="comment_like(this.id)">';
                      $cmtlike1 .= '<i class="fa fa-thumbs-o-up fa-1x" aria-hidden="true">';
                      $cmtlike1 .= '</i>';
                      $cmtlike1 .= '<span>';
     					if($businessprofiledata2[0]['business_comment_likes_count'] > 0){
                      $cmtlike1 .= $businessprofiledata2[0]['business_comment_likes_count'] . '';
                       }
                      $cmtlike1 .= '</span>';
                      $cmtlike1 .= '</a>';
                      echo $cmtlike1;
                    }else{
                  }
               }

           
        }


public function like_comment1()
    {

          $userid = $this->session->userdata('aileenuser');
          $post_id =  $_POST["post_id"];  
          

          $contition_array = array('business_profile_post_comment_id' =>  $_POST["post_id"], 'status' =>'1');
          $businessprofiledata =   $this->data['businessprofiledata'] = $this->common->select_data_by_condition('business_profile_post_comment', $contition_array , $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');
          

            $business_comment_likes_count = $businessprofiledata[0]['business_comment_likes_count'];
            $likeuserarray = explode(',', $businessprofiledata[0]['business_comment_like_user']);
            
            if(!in_array($userid, $likeuserarray)){ //echo "falguni"; die();
            $user_array =   array_push($likeuserarray,$userid);

                if($businessprofiledata[0]['business_comment_likes_count'] == 0){  
                    $userid = implode('', $likeuserarray);
                }else{  
                    $userid = implode(',', $likeuserarray);
                    }

               $data = array(
                    'business_comment_likes_count' => $business_comment_likes_count + 1,
                    'business_comment_like_user' =>   $userid,
                    'modify_date' => date('y-m-d h:i:s')
                  ); 
              
               

            $updatdata =   $this->common->update_data($data,'business_profile_post_comment','business_profile_post_comment_id',$post_id );
             $contition_array = array('business_profile_post_comment_id' =>  $_POST["post_id"], 'status' =>'1');
          $businessprofiledata1 =   $this->data['businessprofiledata1'] = $this->common->select_data_by_condition('business_profile_post_comment', $contition_array , $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');

            if($updatdata){
              

              $cmtlike1 = '<a id="'. $businessprofiledata1[0]['business_profile_post_comment_id'].'" onClick="comment_like1(this.id)">';
              $cmtlike1 .= ' <i class="fa fa-thumbs-up" aria-hidden="true">';
              $cmtlike1 .= '</i>';
              $cmtlike1 .= '<span>';
              if($businessprofiledata1[0]['business_comment_likes_count'] > 0){
              $cmtlike1 .= $businessprofiledata1[0]['business_comment_likes_count'] . '';
                }
              $cmtlike1 .= '</span>';
              $cmtlike1 .= '</a>';
              echo $cmtlike1;
            }else{ 
          }
              
            }else{  
              
              foreach($likeuserarray as $key=>$val){
                if($val==$userid){ 
             $user_array =  array_splice($likeuserarray,$key,1); 
                }
             } 
            
                $data = array(
                    'business_comment_likes_count' => $business_comment_likes_count - 1,
                    'business_comment_like_user' =>   implode(',', $likeuserarray),
                    'modify_date' => date('y-m-d h:i:s')
                  ); 
             

            $updatdata =   $this->common->update_data($data,'business_profile_post_comment','business_profile_post_comment_id',$post_id);
             $contition_array = array('business_profile_post_comment_id' =>  $_POST["post_id"], 'status' =>'1');
          $businessprofiledata2 =   $this->data['businessprofiledata2'] = $this->common->select_data_by_condition('business_profile_post_comment', $contition_array , $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');

                       if($updatdata){
                     
                      $cmtlike1 = '<a id="'. $businessprofiledata2[0]['business_profile_post_comment_id'].'" onClick="comment_like1(this.id)">';
                      $cmtlike1 .= '<i class="fa fa-thumbs-o-up fa-1x" aria-hidden="true">';
                      $cmtlike1 .= '</i>';
                      $cmtlike1 .= '<span>';
     					if($businessprofiledata2[0]['business_comment_likes_count'] > 0){
                      $cmtlike1 .= $businessprofiledata2[0]['business_comment_likes_count'] . '';
                       }
                      $cmtlike1 .= '</span>';
                      $cmtlike1 .= '</a>';
                      echo $cmtlike1;
                    }else{
                  }
               }

           
        }

// Business_profile comment like end 
//Business_profile comment delete start
public function delete_comment()
        { 
           $userid = $this->session->userdata('aileenuser');
           $post_id =  $_POST["post_id"];
           $post_delete =  $_POST["post_delete"]; 

          $data = array(
                    'status' => 0,
                    
            ); 
                   
       
      $updatdata =   $this->common->update_data($data,'business_profile_post_comment','business_profile_post_comment_id',$post_id);
        

      $contition_array = array('business_profile_post_id' =>  $post_delete, 'status' =>'1');
          $businessprofiledata =   $this->data['businessprofiledata'] = $this->common->select_data_by_condition('business_profile_post_comment', $contition_array , $data = '*', $sortby = 'business_profile_post_comment_id', $orderby = 'DESC', $limit = '', $offset = '', $join_str= array(), $groupby = '');
                
                //echo '<pre>'; print_r($businessprofiledata); die();
// khyati changes start

                     foreach($businessprofiledata as $business_profile){

                      $companyname =  $this->db->get_where('business_profile',array('user_id' => $business_profile['user_id']))->row()->company_name;

                      $business_userimage =  $this->db->get_where('business_profile',array('user_id' => $business_profile['user_id'], 'status' => 1))->row()->business_user_image;

                    $cmtinsert .= '<div class="post-design-pro-comment-img">'; 
               
                  
                     $cmtinsert .= '<img  src="'.base_url(USERIMAGE . $business_userimage) .  '" alt="">  </div>';

                      $cmtinsert .=  '<div class="comment-name"><b>' . $companyname . '</b>';
                      $cmtinsert .=  '</div>';
                      $cmtinsert .=  '<div class="comment-details" id= "showcomment' . $business_profile['business_profile_post_comment_id'].'"" >';
                      $cmtinsert .=  $business_profile['comments'];
                      $cmtinsert .=  '</div>';
                      $cmtinsert .=  '<input type="text" name="' . $business_profile['business_profile_post_comment_id'] . '" id="editcomment' . $business_profile['business_profile_post_comment_id'].'"style="display:none;" value="'.$business_profile['comments'].' " onClick="commentedit(this.name)">';
                      $cmtinsert .=  '<button id="editsubmit' . $business_profile['business_profile_post_comment_id'].'" style="display:none;" onClick="edit_comment('.$business_profile['business_profile_post_comment_id'].')">Comment</button><div class="art-comment-menu-design"> <div class="comment-details-menu" id="likecomment1' . $business_profile['business_profile_post_comment_id'].'">';

                      $cmtinsert .=  '<a id="' . $business_profile['business_profile_post_comment_id'] . '"';
                      $cmtinsert .= 'onClick="comment_like1(this.id)">';

                      $userid = $this->session->userdata('aileenuser');
          $contition_array = array('business_profile_post_comment_id' => $business_profile['business_profile_post_comment_id'], 'status' =>'1');
          $businesscommentlike =   $this->data['businesscommentlike'] = $this->common->select_data_by_condition('business_profile_post_comment', $contition_array , $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');
          $likeuserarray = explode(',', $businesscommentlike[0]['business_comment_like_user']);

                      if(!in_array($userid, $likeuserarray)){
                     

                      $cmtinsert .=  '<i class="fa fa-thumbs-o-up fa-1x" aria-hidden="true"></i>'; 

                      }else{
                  $cmtinsert .=  '<i class="fa fa-thumbs-up" aria-hidden="true"></i>'; 
                 }


                      $cmtinsert .= '<span>';
                      if($business_profile['business_comment_likes_count'] > 0){
                      $cmtinsert .=  '' . $business_profile['business_comment_likes_count'];
                        }
                      $cmtinsert .= '</span>';
                      $cmtinsert .=  '</a></div>';


                    $userid  = $this->session->userdata('aileenuser');
                      if($business_profile['user_id'] == $userid){ 
                           
                      $cmtinsert .=  '<span role="presentation" aria-hidden="true"> · </span>';
                      $cmtinsert .=   '<div class="comment-details-menu">';


                      $cmtinsert .=  '<div id="editcommentbox' . $business_profile['business_profile_post_comment_id'] . '"style="display:block;">';

                      $cmtinsert .=  '<a id="' . $business_profile['business_profile_post_comment_id'] . '"';
                      $cmtinsert .= 'onClick="comment_editbox(this.id)">';
                      $cmtinsert .=  'Edit'; 
                      $cmtinsert .=  '</a></div>';

                      $cmtinsert .=  '<div id="editcancle' . $business_profile['business_profile_post_comment_id'] . '"style="display:none;">';

                      $cmtinsert .=  '<a id="' . $business_profile['business_profile_post_comment_id'] . '"';
                      $cmtinsert .= 'onClick="comment_editcancle(this.id)">';
                      $cmtinsert .=  'Cancle'; 
                      $cmtinsert .=  '</a></div>';

                      $cmtinsert .=  '</div>';

                       }

 

 
       $userid  = $this->session->userdata('aileenuser');

       $business_userid =  $this->db->get_where('business_profile_post',array('business_profile_post_id' => $business_profile['business_profile_post_id'], 'status' => 1))->row()->user_id;


          if($business_profile['user_id'] == $userid ||  $business_userid == $userid){ 
             
                      $cmtinsert .= '<span role="presentation" aria-hidden="true"> · </span>';
                      $cmtinsert .= '<div class="comment-details-menu">';


                      $cmtinsert .=  '<input type="hidden" name="post_delete"';
                      $cmtinsert .=  'id="post_delete"'; 
                     $cmtinsert .= 'value= "'. $business_profile['business_profile_post_id'] .'">';    
                      $cmtinsert .=  '<a id="' . $business_profile['business_profile_post_comment_id'] . '"';
                      $cmtinsert .= 'onClick="comment_delete(this.id)">';
                      $cmtinsert .=  'Delete'; 
                      $cmtinsert .=  '</a></div>';
                      
                       }

                      $cmtinsert .= '<span role="presentation" aria-hidden="true"> · </span>';
                      $cmtinsert .= '<div class="comment-details-menu">';
                      $cmtinsert .=   '<p>' . $business_profile['created_date'] . '</p></div></div>';
                                 }
                         echo $cmtinsert;

        }
//second page manage in manage post for function start

public function delete_comment1()
{ 
           $userid = $this->session->userdata('aileenuser');
           $post_id =  $_POST["post_id"];
           $post_delete =  $_POST["post_delete"]; 

          $data = array(
                    'status' => 0,
                    
            ); 
                   
       
      $updatdata =   $this->common->update_data($data,'business_profile_post_comment','business_profile_post_comment_id',$post_id);
        

      $contition_array = array('business_profile_post_id' =>  $post_delete, 'status' =>'1');
          $businessprofiledata =   $this->data['businessprofiledata'] = $this->common->select_data_by_condition('business_profile_post_comment', $contition_array , $data = '*', $sortby = 'business_profile_post_comment_id', $orderby = 'DESC', $limit = '', $offset = '', $join_str= array(), $groupby = '');
                
                //echo '<pre>'; print_r($businessprofiledata); die();
// khyati changes start

                     foreach($businessprofiledata as $business_profile){

                      $companyname =  $this->db->get_where('business_profile',array('user_id' => $business_profile['user_id']))->row()->company_name;

                      $business_userimage =  $this->db->get_where('business_profile',array('user_id' => $business_profile['user_id'], 'status' => 1))->row()->business_user_image;

                    $cmtinsert .= '<div class="post-design-pro-comment-img">'; 
               
                  
                     $cmtinsert .= '<img  src="'.base_url(USERIMAGE . $business_userimage) .  '" alt="">  </div>';

                      $cmtinsert .=  '<div class="comment-name"><b>' . $companyname . '</b>';
                      $cmtinsert .=  '</div>';
                      $cmtinsert .=  '<div class="comment-details" id= "showcomment' . $business_profile['business_profile_post_comment_id'].'"" >';
                      $cmtinsert .=  $business_profile['comments'];
                      $cmtinsert .=  '</div>';
                      $cmtinsert .=  '<input type="text" name="' . $business_profile['business_profile_post_comment_id'] . '" id="editcomment' . $business_profile['business_profile_post_comment_id'].'"style="display:none;" value="'.$business_profile['comments'].' " onClick="commentedit(this.name)">';
                      $cmtinsert .=  '<button id="editsubmit' . $business_profile['business_profile_post_comment_id'].'" style="display:none;" onClick="edit_comment('.$business_profile['business_profile_post_comment_id'].')">Comment</button><div class="art-comment-menu-design"> <div class="comment-details-menu" id="likecomment1' . $business_profile['business_profile_post_comment_id'].'">';

                      $cmtinsert .=  '<a id="' . $business_profile['business_profile_post_comment_id'] . '"';
                      $cmtinsert .= 'onClick="comment_like1(this.id)">';

                      $userid = $this->session->userdata('aileenuser');
          $contition_array = array('business_profile_post_comment_id' => $business_profile['business_profile_post_comment_id'], 'status' =>'1');
          $businesscommentlike =   $this->data['businesscommentlike'] = $this->common->select_data_by_condition('business_profile_post_comment', $contition_array , $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');
          $likeuserarray = explode(',', $businesscommentlike[0]['business_comment_like_user']);

                      if(!in_array($userid, $likeuserarray)){
                     

                      $cmtinsert .=  '<i class="fa fa-thumbs-o-up fa-1x" aria-hidden="true"></i>'; 

                      }else{
                  $cmtinsert .=  '<i class="fa fa-thumbs-up" aria-hidden="true"></i>'; 
                 }


                      $cmtinsert .= '<span>';
                      if($business_profile['business_comment_likes_count'] > 0){
                      $cmtinsert .=  '' . $business_profile['business_comment_likes_count'];
                        }
                      $cmtinsert .= '</span>';
                      $cmtinsert .=  '</a></div>';


                    $userid  = $this->session->userdata('aileenuser');
                      if($business_profile['user_id'] == $userid){ 
                           
                      $cmtinsert .=  '<span role="presentation" aria-hidden="true"> · </span>';
                      $cmtinsert .=   '<div class="comment-details-menu">';


                      $cmtinsert .=  '<div id="editcommentbox' . $business_profile['business_profile_post_comment_id'] . '"style="display:block;">';

                      $cmtinsert .=  '<a id="' . $business_profile['business_profile_post_comment_id'] . '"';
                      $cmtinsert .= 'onClick="comment_editbox(this.id)">';
                      $cmtinsert .=  'Edit'; 
                      $cmtinsert .=  '</a></div>';

                      $cmtinsert .=  '<div id="editcancle' . $business_profile['business_profile_post_comment_id'] . '"style="display:none;">';

                      $cmtinsert .=  '<a id="' . $business_profile['business_profile_post_comment_id'] . '"';
                      $cmtinsert .= 'onClick="comment_editcancle(this.id)">';
                      $cmtinsert .=  'Cancle'; 
                      $cmtinsert .=  '</a></div>';

                      $cmtinsert .=  '</div>';

                       }

 

 
       $userid  = $this->session->userdata('aileenuser');

       $business_userid =  $this->db->get_where('business_profile_post',array('business_profile_post_id' => $business_profile['business_profile_post_id'], 'status' => 1))->row()->user_id;


          if($business_profile['user_id'] == $userid ||  $business_userid == $userid){ 
             
                      $cmtinsert .= '<span role="presentation" aria-hidden="true"> · </span>';
                      $cmtinsert .= '<div class="comment-details-menu">';


                      $cmtinsert .=  '<input type="hidden" name="post_delete1"';
                      $cmtinsert .=  'id="post_delete"'; 
                     $cmtinsert .= 'value= "'. $business_profile['business_profile_post_id'] .'">';    
                      $cmtinsert .=  '<a id="' . $business_profile['business_profile_post_comment_id'] . '"';
                      $cmtinsert .= 'onClick="comment_delete1(this.id)">';
                      $cmtinsert .=  'Delete'; 
                      $cmtinsert .=  '</a></div>';
                      
                       }

                      $cmtinsert .= '<span role="presentation" aria-hidden="true"> · </span>';
                      $cmtinsert .= '<div class="comment-details-menu">';
                      $cmtinsert .=   '<p>' . $business_profile['created_date'] . '</p></div></div>';
                                 }
                         echo $cmtinsert;

        }

//Business_profile comment delete end     

// Business_profile post like start

 public function like_post()
        {

          $userid = $this->session->userdata('aileenuser');
         
          $post_id =  $_POST["post_id"];  
          
          $contition_array = array('business_profile_post_id' =>  $_POST["post_id"], 'status' =>'1');
          $businessprofiledata =   $this->data['businessprofiledata'] = $this->common->select_data_by_condition('business_profile_post', $contition_array , $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');
         

            $business_likes_count = $businessprofiledata[0]['business_likes_count'];
            $likeuserarray = explode(',', $businessprofiledata[0]['business_like_user']);
            
            if(!in_array($userid, $likeuserarray)){ 

            $user_array =   array_push($likeuserarray,$userid);

                if($businessprofiledata[0]['business_likes_count'] == 0){ 
                    $userid = implode('', $likeuserarray);
                }else{  
                    $userid = implode(',', $likeuserarray);
                    }

               $data = array(
                    'business_likes_count' => $business_likes_count + 1,
                    'business_like_user' =>   $userid,
                    'modify_date' => date('y-m-d h:i:s')
                  ); 
              

            $updatdata =   $this->common->update_data($data,'business_profile_post','business_profile_post_id',$post_id );
             $contition_array = array('business_profile_post_id' =>  $_POST["post_id"], 'status' =>'1');
          $businessprofiledata1 =   $this->data['businessprofiledata1'] = $this->common->select_data_by_condition('business_profile_post', $contition_array , $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');

            if($updatdata){
              //echo"add";

              $cmtlike = '<li>';
              $cmtlike .= '<a id="'. $businessprofiledata1[0]['business_profile_post_id'].'" onClick="post_like(this.id)">';
              $cmtlike .= ' <i class="fa fa-thumbs-up" aria-hidden="true">';
              $cmtlike .= '</i>';
              $cmtlike .= '<span>';
              if($businessprofiledata1[0]['business_likes_count'] > 0){
              $cmtlike .= $businessprofiledata1[0]['business_likes_count'] . '';
              }
              $cmtlike .= '</span>';
              $cmtlike .= '</a>';
              $cmtlike .= '</li>';
              echo $cmtlike;
            }else{}
              
            }else{
             
              foreach($likeuserarray as $key=>$val){
                if($val==$userid){ //echo $key;
             $user_array =  array_splice($likeuserarray,$key,1); 
                }
             } 
             
                $data = array(
                    'business_likes_count' => $business_likes_count - 1,
                    'business_like_user' =>   implode(',', $likeuserarray),
                    'modify_date' => date('y-m-d h:i:s')
                  ); 
              

            $updatdata =   $this->common->update_data($data,'business_profile_post','business_profile_post_id',$post_id);
             $contition_array = array('business_profile_post_id' =>  $_POST["post_id"], 'status' =>'1');
          $businessprofiledata2 =   $this->data['businessprofiledata2'] = $this->common->select_data_by_condition('business_profile_post', $contition_array , $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');

                       if($updatdata){
                     
                      $cmtlike = '<li>';
                      $cmtlike .= '<a id="'. $businessprofiledata2[0]['business_profile_post_id'].'" onClick="post_like(this.id)">';
                      $cmtlike .= '<i class="fa fa-thumbs-o-up fa-1x" aria-hidden="true">';
                      $cmtlike .= '</i>';
                      $cmtlike .= '<span>';
                      if($businessprofiledata2[0]['business_likes_count'] > 0){
                      $cmtlike .= $businessprofiledata2[0]['business_likes_count'] . '';
                      }
                      $cmtlike .= '</span>';
                      $cmtlike .= '</a>';
                      $cmtlike .= '</li>';
                      echo $cmtlike;
                    }else{}
               }

           
        }
// business_profile post  like end

//business_profile comment insert start

  public function insert_comment()
    {

          $userid = $this->session->userdata('aileenuser');
         
          $post_id =  $_POST["post_id"]; 
          $post_comment =  $_POST["comment"]; 

          $data = array(
                            'user_id' => $userid,
                            'business_profile_post_id'=>$post_id,
                            'comments' => $post_comment,
                            'created_date' => date('Y-m-d',time()),
                            'status'=>1,
                            'is_delete'=>0
                            
                    ); 
                    
                        
                       
        $insert_id =   $this->common->insert_data_getid($data,'business_profile_post_comment');

        $contition_array = array('business_profile_post_id' =>  $_POST["post_id"], 'status' =>'1');
          $businessprofiledata =   $this->data['businessprofiledata'] = $this->common->select_data_by_condition('business_profile_post_comment', $contition_array , $data = '*', $sortby = 'business_profile_post_comment_id', $orderby = 'ASC', $limit = '', $offset = '', $join_str= array(), $groupby = '');
          //echo "<pre>"; print_r($businessprofiledata); die();
                  
// khyati changes start

                     foreach($businessprofiledata as $business_profile){

                     $company_name =  $this->db->get_where('business_profile',array('user_id' => $business_profile['user_id']))->row()->company_name;

                     $business_userimage =  $this->db->get_where('business_profile',array('user_id' => $business_profile['user_id'], 'status' => 1))->row()->business_user_image;

                     // $cmtinsert = '<div class="all-comment-comment-box">';
                     $cmtinsert .= '<div class="post-design-pro-comment-img">'; 
               
                  
                     $cmtinsert .= '<img  src="'.base_url(USERIMAGE . $business_userimage) .  '" alt="">  </div>';

                      $cmtinsert .=  '<div class="comment-name"><b>' . $company_name . '</b>';
                      $cmtinsert .=  '</div>';
                      $cmtinsert .=  '<div class="comment-details" id= "showcomment' . $business_profile['business_profile_post_comment_id'].'"" >';
                      $cmtinsert .=  $business_profile['comments'];
                      $cmtinsert .=  '</div>';
                      $cmtinsert .=  '<input type="text" name="' . $business_profile['business_profile_post_comment_id'] . '" id="editcomment' . $business_profile['business_profile_post_comment_id'].'"style="display:none;" value="'.$business_profile['comments'].' " onClick="commentedit(this.name)">';
                      $cmtinsert .=  '<button id="editsubmit' . $business_profile['business_profile_post_comment_id'].'" style="display:none;" onClick="edit_comment('.$business_profile['business_profile_post_comment_id'].')">Comment</button><div class="art-comment-menu-design"> <div class="comment-details-menu" id="likecomment1' . $business_profile['business_profile_post_comment_id'].'">';

                      $cmtinsert .=  '<a id="' . $business_profile['business_profile_post_comment_id'] . '"';
                      $cmtinsert .= 'onClick="comment_like1(this.id)">';

                      $userid = $this->session->userdata('aileenuser');
          $contition_array = array('business_profile_post_comment_id' => $business_profile['business_profile_post_comment_id'], 'status' =>'1');
          $businesscommentlike =   $this->data['businesscommentlike'] = $this->common->select_data_by_condition('business_profile_post_comment', $contition_array , $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');
          $likeuserarray = explode(',', $businesscommentlike[0]['business_comment_like_user']);

                      if(!in_array($userid, $likeuserarray)){
                     

                      $cmtinsert .=  '<i class="fa fa-thumbs-o-up fa-1x" aria-hidden="true"></i>'; 

                      }else{
                  $cmtinsert .=  '<i class="fa fa-thumbs-up" aria-hidden="true"></i>'; 
                 }


                      $cmtinsert .= '<span>';
                      if($business_profile['business_comment_likes_count'] > 0){
                      $cmtinsert .=  '' . $business_profile['business_comment_likes_count'];
                        }
                      $cmtinsert .= '</span>';
                      $cmtinsert .=  '</a></div>';


                    $userid  = $this->session->userdata('aileenuser');
                      if($business_profile['user_id'] == $userid){ 
                           
                      $cmtinsert .=  '<span role="presentation" aria-hidden="true"> · </span>';
                      $cmtinsert .=   '<div class="comment-details-menu">';


                      $cmtinsert .=  '<div id="editcommentbox' . $business_profile['business_profile_post_comment_id'] . '"style="display:block;">';

                      $cmtinsert .=  '<a id="' . $business_profile['business_profile_post_comment_id'] . '"';
                      $cmtinsert .= 'onClick="comment_editbox(this.id)">';
                      $cmtinsert .=  'Edit'; 
                      $cmtinsert .=  '</a></div>';

                      $cmtinsert .=  '<div id="editcancle' . $business_profile['business_profile_post_comment_id'] . '"style="display:none;">';

                      $cmtinsert .=  '<a id="' . $business_profile['business_profile_post_comment_id'] . '"';
                      $cmtinsert .= 'onClick="comment_editcancle(this.id)">';
                      $cmtinsert .=  'Cancle'; 
                      $cmtinsert .=  '</a></div>';

                      $cmtinsert .=  '</div>';

                       }

 

 
       $userid  = $this->session->userdata('aileenuser');

       $business_userid =  $this->db->get_where('business_profile_post',array('business_profile_post_id' => $business_profile['business_profile_post_id'], 'status' => 1))->row()->user_id;


          if($business_profile['user_id'] == $userid ||  $business_userid == $userid){ 
             
                      $cmtinsert .= '<span role="presentation" aria-hidden="true"> · </span>';
                      $cmtinsert .= '<div class="comment-details-menu">';


                      $cmtinsert .=  '<input type="hidden" name="post_delete"';
                      $cmtinsert .=  'id="post_delete"'; 
                     $cmtinsert .= 'value= "'. $business_profile['business_profile_post_id'] .'">';    
                      $cmtinsert .=  '<a id="' . $business_profile['business_profile_post_comment_id'] . '"';
                      $cmtinsert .= 'onClick="comment_delete(this.id)">';
                      $cmtinsert .=  'Delete'; 
                      $cmtinsert .=  '</a></div>';
                      
                       }

                      $cmtinsert .= '<span role="presentation" aria-hidden="true"> · </span>';
                      $cmtinsert .= '<div class="comment-details-menu">';
                      $cmtinsert .=   '<p>' . $business_profile['created_date'] . '</p></div></div>';
                                 }
                         echo $cmtinsert;
                     // khyati chande 

      }



public function insert_comment1()
{

          $userid = $this->session->userdata('aileenuser');
         
          $post_id =  $_POST["post_id"]; 
          $post_comment =  $_POST["comment"]; 

          $data = array(
                            'user_id' => $userid,
                            'business_profile_post_id'=>$post_id,
                            'comments' => $post_comment,
                            'created_date' => date('Y-m-d',time()),
                            'status'=>1,
                            'is_delete'=>0
                            
                    ); 
                    
                        
                       
        $insert_id =   $this->common->insert_data_getid($data,'business_profile_post_comment');

        $contition_array = array('business_profile_post_id' =>  $_POST["post_id"], 'status' =>'1');
          $businessprofiledata =   $this->data['businessprofiledata'] = $this->common->select_data_by_condition('business_profile_post_comment', $contition_array , $data = '*', $sortby = 'business_profile_post_comment_id', $orderby = 'ASC', $limit = '', $offset = '', $join_str= array(), $groupby = '');
          //echo "<pre>"; print_r($businessprofiledata); die();
                  
// khyati changes start

                     foreach($businessprofiledata as $business_profile){

                     $company_name =  $this->db->get_where('business_profile',array('user_id' => $business_profile['user_id']))->row()->company_name;

                     $business_userimage =  $this->db->get_where('business_profile',array('user_id' => $business_profile['user_id'], 'status' => 1))->row()->business_user_image;

                     // $cmtinsert = '<div class="all-comment-comment-box">';
                     $cmtinsert .= '<div class="post-design-pro-comment-img">'; 
               
                  
                     $cmtinsert .= '<img  src="'.base_url(USERIMAGE . $business_userimage) .  '" alt="">  </div>';

                      $cmtinsert .=  '<div class="comment-name"><b>' . $company_name . '</b>';
                      $cmtinsert .=  '</div>';
                      $cmtinsert .=  '<div class="comment-details" id= "showcomment' . $business_profile['business_profile_post_comment_id'].'"" >';
                      $cmtinsert .=  $business_profile['comments'];
                      $cmtinsert .=  '</div>';
                      $cmtinsert .=  '<input type="text" name="' . $business_profile['business_profile_post_comment_id'] . '" id="editcomment' . $business_profile['business_profile_post_comment_id'].'"style="display:none;" value="'.$business_profile['comments'].' " onClick="commentedit(this.name)">';
                      $cmtinsert .=  '<button id="editsubmit' . $business_profile['business_profile_post_comment_id'].'" style="display:none;" onClick="edit_comment('.$business_profile['business_profile_post_comment_id'].')">Comment</button><div class="art-comment-menu-design"> <div class="comment-details-menu" id="likecomment1' . $business_profile['business_profile_post_comment_id'].'">';

                      $cmtinsert .=  '<a id="' . $business_profile['business_profile_post_comment_id'] . '"';
                      $cmtinsert .= 'onClick="comment_like1(this.id)">';

                      $userid = $this->session->userdata('aileenuser');
          $contition_array = array('business_profile_post_comment_id' => $business_profile['business_profile_post_comment_id'], 'status' =>'1');
          $businesscommentlike =   $this->data['businesscommentlike'] = $this->common->select_data_by_condition('business_profile_post_comment', $contition_array , $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');
          $likeuserarray = explode(',', $businesscommentlike[0]['business_comment_like_user']);

                      if(!in_array($userid, $likeuserarray)){
                     

                      $cmtinsert .=  '<i class="fa fa-thumbs-o-up fa-1x" aria-hidden="true"></i>'; 

                      }else{
                  $cmtinsert .=  '<i class="fa fa-thumbs-up" aria-hidden="true"></i>'; 
                 }


                      $cmtinsert .= '<span>';
                      if($business_profile['business_comment_likes_count'] > 0){
                      $cmtinsert .=  '' . $business_profile['business_comment_likes_count'];
                        }
                      $cmtinsert .= '</span>';
                      $cmtinsert .=  '</a></div>';


                    $userid  = $this->session->userdata('aileenuser');
                      if($business_profile['user_id'] == $userid){ 
                           
                      $cmtinsert .=  '<span role="presentation" aria-hidden="true"> · </span>';
                      $cmtinsert .=   '<div class="comment-details-menu">';


                      $cmtinsert .=  '<div id="editcommentbox' . $business_profile['business_profile_post_comment_id'] . '"style="display:block;">';

                      $cmtinsert .=  '<a id="' . $business_profile['business_profile_post_comment_id'] . '"';
                      $cmtinsert .= 'onClick="comment_editbox(this.id)">';
                      $cmtinsert .=  'Edit'; 
                      $cmtinsert .=  '</a></div>';

                      $cmtinsert .=  '<div id="editcancle' . $business_profile['business_profile_post_comment_id'] . '"style="display:none;">';

                      $cmtinsert .=  '<a id="' . $business_profile['business_profile_post_comment_id'] . '"';
                      $cmtinsert .= 'onClick="comment_editcancle(this.id)">';
                      $cmtinsert .=  'Cancle'; 
                      $cmtinsert .=  '</a></div>';

                      $cmtinsert .=  '</div>';

                       }

 

 
       $userid  = $this->session->userdata('aileenuser');

       $business_userid =  $this->db->get_where('business_profile_post',array('business_profile_post_id' => $business_profile['business_profile_post_id'], 'status' => 1))->row()->user_id;


          if($business_profile['user_id'] == $userid ||  $business_userid == $userid){ 
             
                      $cmtinsert .= '<span role="presentation" aria-hidden="true"> · </span>';
                      $cmtinsert .= '<div class="comment-details-menu">';


                      $cmtinsert .=  '<input type="hidden" name="post_delete"';
                      $cmtinsert .=  'id="post_delete1' . $business_profile['business_profile_post_comment_id'] . '"'; 
                     $cmtinsert .= 'value= "'. $business_profile['business_profile_post_id'] .'">';    
                      $cmtinsert .=  '<a id="' . $business_profile['business_profile_post_comment_id'] . '"';
                      $cmtinsert .= 'onClick="comment_delete1(this.id)">';
                      $cmtinsert .=  'Delete'; 
                      $cmtinsert .=  '</a></div>';
                      
                       }

                      $cmtinsert .= '<span role="presentation" aria-hidden="true"> · </span>';
                      $cmtinsert .= '<div class="comment-details-menu">';
                      $cmtinsert .=   '<p>' . $business_profile['created_date'] . '</p></div></div>';
                                 }
                         echo $cmtinsert;
                     // khyati chande 

      }
        
//business_profile comment insert end  

//business_profile comment edit start
public function edit_comment_insert()
    { 

          $userid = $this->session->userdata('aileenuser');
         
          $post_id =  $_POST["post_id"]; 
          $post_comment =  $_POST["comment"]; 

             $data = array(
                    'comments' => $post_comment,
                    'modify_date' => date('y-m-d h:i:s')
                  ); 
              

            $updatdata =   $this->common->update_data($data,'business_profile_post_comment','business_profile_post_comment_id',$post_id);
            if($updatdata){

               $contition_array = array('business_profile_post_comment_id' =>  $_POST["post_id"], 'status' =>'1');
          $businessprofiledata =   $this->data['businessprofiledata'] = $this->common->select_data_by_condition('business_profile_post_comment', $contition_array , $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');
             
                    $cmtlike = '<div>';
                    $cmtlike .= $businessprofiledata[0]['comments'] . "<br>";
                    $cmtlike .= '</div>';
                   echo $cmtlike;
            }
   
    }

//business_profile like commnet ajax end 


// click on post after post open on new page start


  public function postnewpage($id)
    {

      $userid  = $this->session->userdata('aileenuser');
      $contition_array = array('user_id' => $userid, 'status' =>'1' );
      $this->data['businessdata'] =  $this->common->select_data_by_condition('business_profile', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

      $contition_array = array('business_profile_post_id' => $id, 'status' =>'1');

      $this->data['busienss_data'] = $this->common->select_data_by_condition('business_profile_post', $contition_array, $data='*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');
        
        //echo "<pre>"; print_r($this->data['art_data']);die();
      $this->load->view('business_profile/postnewpage', $this->data);

    }
// click on post after post open on new page end 


//edit post start

  public function edit_post_insert()
        { 

           $userid = $this->session->userdata('aileenuser');
          
            $post_id =  $_POST["business_profile_post_id"]; 
            $business_post =  $_POST["product_name"]; 
            $business_description =  $_POST["product_description"]; 

             $data = array(
                    'product_name' => $business_post,
                    'product_description' =>$business_description,
                    'modify_date' => date('y-m-d h:i:s')
                  ); 
               
            $updatdata =   $this->common->update_data($data,'business_profile_post','business_profile_post_id',$post_id);
            if($updatdata){

               $contition_array = array('business_profile_post_id' =>  $_POST["business_profile_post_id"], 'status' =>'1');
          $businessdata =   $this->data['businessdata'] = $this->common->select_data_by_condition('business_profile_post', $contition_array , $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');
              
              //echo "<pre>"; print_r($artdata); die();
                    if($this->data['businessdata'][0]['product_name']){ 
                    $editpost = '<div>';
                    $editpost .= $businessdata[0]['product_name'] . "<br>";
                    $editpost .= '</div>';

                     
                  }
                  if($this->data['businessdata'][0]['product_description']){  
                     
                    $editpostdes = '<div>';
                    $editpostdes.= $businessdata[0]['product_description'] . "<br>";
                    $editpostdes .= '</div>';

                   
                  }
                 //echo $editpost;   echo $editpostdes;
          echo json_encode(
      array("title" => $editpost, 
      "description" => $editpostdes)); 
            }
   
    }
//edit post end

//reactivate account start

 public function reactivate()
  {

    $userid = $this->session->userdata('aileenuser');
      $data = array(
                    'status' => 1,
                    'modified_date' => date('y-m-d h:i:s')
                  ); 
               
    $updatdata =   $this->common->update_data($data,'business_profile','user_id',$userid);
      if($updatdata){

        redirect('business_profile/business_profile_post', refresh);

      }else{

          $this->load->view('business_profile/reactivate', $this->data);
      }

  }

//reactivate accont end    

//delete post particular user start
 public function del_particular_userpost()
  {

         $userid = $this->session->userdata('aileenuser');
          
           $post_id =  $_POST['business_profile_post_id']; 
           
$contition_array = array('business_profile_post_id' =>  $post_id, 'status' =>'1');
          $businessdata =   $this->data['businessdata'] = $this->common->select_data_by_condition('business_profile_post', $contition_array , $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str= array(), $groupby = '');

$likeuserarray = explode(',', $businessdata[0]['delete_post']);

           $user_array =   array_push($likeuserarray,$userid);

                if($businessdata[0]['delete_post'] == 0){ 
                    $userid = implode('', $likeuserarray);
                }else{  
                    $userid = implode(',', $likeuserarray);
                    }

             $data = array(
                    'delete_post' =>  $userid,
                    'modify_date' => date('y-m-d h:i:s')
                    
                  ); 
    
    $updatdata =   $this->common->update_data($data,
      'business_profile_post','business_profile_post_id',$post_id);
    
   
  } 

//delete post particular user end     
     
}
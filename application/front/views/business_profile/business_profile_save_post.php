<!-- start head -->
<?php  echo $head; ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/timeline.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('css/select2-4.0.3.min.css'); ?>">
    <!-- END HEAD -->
    <!-- start header -->
<?php echo $header; ?>
    <!-- END HEADER -->

   <!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script>
$(document).ready(function()
{


/* Uploading Profile BackGround Image */
$('body').on('change','#bgphotoimg', function()
{

$("#bgimageform").ajaxForm({target: '#timelineBackground',
beforeSubmit:function(){},
success:function(){

$("#timelineShade").hide();
$("#bgimageform").hide();
},
error:function(){

} }).submit();
});



/* Banner position drag */
$("body").on('mouseover','.headerimage',function ()
{
var y1 = $('#timelineBackground').height();
var y2 =  $('.headerimage').height();
$(this).draggable({
scroll: false,
axis: "y",
drag: function(event, ui) {
if(ui.position.top >= 0)
{
ui.position.top = 0;
}
else if(ui.position.top <= y1 - y2)
{
ui.position.top = y1 - y2;
}
},
stop: function(event, ui)
{
}
});
});


/* Bannert Position Save*/
$("body").on('click','.bgSave',function ()
{
var id = $(this).attr("id");
var p = $("#timelineBGload").attr("style");
var Y =p.split("top:");
var Z=Y[1].split(";");
var dataString ='position='+Z[0];
$.ajax({
type: "POST",
url: "<?php echo base_url('business_profile/image_saveBG_ajax'); ?>",
data: dataString,
cache: false,
beforeSend: function(){ },
success: function(html)
{
if(html)
{
  window.location.reload();
$(".bgImage").fadeOut('slow');
$(".bgSave").fadeOut('slow');
$("#timelineShade").fadeIn("slow");
$("#timelineBGload").removeClass("headerimage");
$("#timelineBGload").css({'margin-top':html});
return false;
}
}
});
return false;
});



});
</script>
</head>
<body>

<!-- cover pic start -->
<div class="container" id="container">

<div id="timelineContainer">

<!-- timeline background -->
<div id="timelineBackground">
<img src="<?php echo base_url(USERIMAGE . $businessdata[0]['profile_background']);?>" class="bgImage" style="margin-top: <?php echo $businessdata[0]['profile_background_position']; ?>;">

</div>

<!-- timeline background -->
<div  id="timelineShade" style="background:url(<?php echo base_url('images/timeline_shade.png'); ?>">
<form id="bgimageform" method="post" enctype="multipart/form-data" action="<?php echo base_url('business_profile/image_upload_ajax'); ?>">

<!-- <label for="bgphotoimg"><i class=" fa fa-camera fa-2x">  </i></label>
 -->


<div class="uploadFile timelineUploadBG" style="background:url(<?php echo base_url('images/whitecam.png'); ?>">

<input type="file" name="photoimg" id="bgphotoimg" class=" custom-file-input" original-title="Change Cover Picture"  ">




</div>
</form>
</div>

<!-- timeline profile picture -->


<!-- timeline title -->


<!-- timeline nav -->
<div id="timelineNav"></div>

</div>

</div>
<!-- cover pic end -->
        <div>
            <div class="container">
                
                <div class="profile-photo">
                    <div class="profile-pho">

                        <div class="user-pic">
                        <?php if($businessdata[0]['business_user_image'] != ''){ ?>
                           <img src="<?php echo base_url(USERIMAGE . $businessdata[0]['business_user_image']);?>" alt="" >
                            <?php } else { ?>
                            <img alt="" class="img-circle" src="<?php echo base_url(NOIMAGE); ?>" alt="" />
                            <?php } ?>
                            <a href="#popup-form" class="fancybox"><i class="fa fa-camera" aria-hidden="true"></i> Update Profile Picture</a>

                        </div>
                        <div id="popup-form">
                        <?php echo form_open_multipart(base_url('business_profile/user_image_insert'), array('id' => 'userimage','name' => 'userimage', 'class' => 'clearfix')); ?>
                        <input type="file" name="profilepic" accept="image/gif, image/jpeg, image/png" id="profilepic">
                        <input type="hidden" name="hitext" id="hitext" value="1">
                        <input type="submit" name="cancel1" id="cancel1" value="Cancel">
                        <input type="submit" name="profilepicsubmit" id="profilepicsubmit" value="Save">
                    </form>
                </div>

                    </div>
                    <div class="col-md-2 col-sm-2"></div>
                      <div class="job-menu-profile">
                          <h5> <?php echo ucwords($userdata[0]['first_name']) .' '. ucwords($userdata[0]['last_name']); ?></h5>
                            
                </div>
            </div>
        </div>
        </div>
         <?php echo $business_search; ?>
        <div class="user-midd-section">
            <div class="container">
                <div class="row">
                    <?php echo $business_left; ?> 
                    <div class="col-md-7 col-sm-7">

                    <div>
                        <?php
                                        if ($this->session->flashdata('error')) {
                                            echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
                                        }
                                        if ($this->session->flashdata('success')) {
                                            echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
                                        }?>
                    </div>

                 


                        <div class="common-form">
                            <div class="job-saved-box">
                                <h3>Saved Post</h3>
                                <div class="contact-frnd-post">
                                    <div class="job-contact-frnd ">

<?php
    foreach($business_profile_data as $row)
    { 
 $userid = $this->session->userdata('aileenuser');
       $contition_array = array('user_id'=> $userid,'post_id' => $row['business_profile_post_id'],'is_delete' =>0);

$jobdata =  $this->common->select_data_by_condition('business_profile_save', $contition_array, $data = '*', $sortby = '', $orderby = 'desc', $limit = '', $offset = '', $join_str = array(), $groupby = ''); 

                            
if($jobdata[0]['business_save'] == 1)
{ 

                                        ?>

                                        <div class="profile-job-post-detail clearfix">

                                            <div class="profile-job-post-title-inside clearfix">
                                                <div class="profile-job-post-location-name">
                                                    <ul>

                                                    
                                                     <?php 
                 $companyname =  $this->db->get_where('business_profile',array('user_id' => $row['user_id']))->row()->company_name;

                  ?>

                                                 <li><h4><a href="<?php echo base_url('business_profile/business_resume/'.$row['business_slug']); ?>"><?php echo ucwords($companyname); ?></a><h4></li>
                                                 
                                                      <li><?php echo $row['product_name']; ?></li> 
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="profile-job-post-title clearfix">
                                                <div class="profile-job-profile-button clearfix">
                                                    <div class="profile-job-details">
                                                        <ul>
                                                            
                                                            <li>
                                                            <?php  print $row['product_description'];  ?>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="profile-job-profile-menu">
                                                    <ul class="clearfix">
                                                        
                                                        <li>
                                                         <!--video and audio display start -->

                                                        
                                                            <?php
                                                       $allowed =  array('gif','png','jpg');
                                                       $allowespdf = array('pdf');
                                                       
                                                       $filename = $row['product_image'];
                                                       

                                                       $ext = pathinfo($filename, PATHINFO_EXTENSION);
                                                      

                                                       if(in_array($ext,$allowed) ) 
                                                       { 
                                                         
                                                          ?>
                                                         
                                                       <img src="<?php echo base_url(BUSINESSPROFILEIMAGE.$row['product_image'])?>" style="width:100px;height:100px;"> 
                                                          <?php
                                                       }
                                                       elseif(in_array($ext,$allowespdf))
                                                       { ?>

                                                        <a href="<?php echo base_url('business_profile/creat_pdf/'.$row['business_profile_post_id']) ?>">PDF</a>
                                                       <?php }
                                                       else
                                                       {
                                                        
                                                       ?>

                                                        <video width="320" height="240" controls>
                                                          <source src="<?php echo base_url(BUSINESSPROFILEIMAGE.$row['product_image']); ?>" type="video/mp4">
                                                          <source src="movie.ogg" type="video/ogg">
                                                          Your browser does not support the video tag.
                                                       </video>
                                                       <?php
                                                        }
                                                       ?>
                                                         
<!--video and audio display end -->
                                                        </li>
                                                        
                                                    </ul>
                                                </div>
                                                <div class="profile-job-profile-button clearfix">
                                                   <div class="apply-btn">

                                                   <a href="<?php echo base_url('business_profile/business_profile_delete/'.$jobdata[0]['save_id']); ?>">Remove</a>


                                                    <a href="<?php echo base_url('business_profile/business_profile_contactperson/'.$row['user_id'].''); ?>">contact person</a>


                                                    <a href="<?php echo base_url('business_profile/business_profile_likepost/'.$row['business_profile_post_id']. '/save'); ?>">Like <?php 
                                                        if($row['business_likes_count'] > 0)
                                                        {
                                                             echo $row['business_likes_count'];
                                                        } ?></a>



                                                   <a href="<?php echo base_url('business_profile/business_profile_commentpost/'.$row['business_profile_post_id']. ''); ?>">Comment</a> 

                                                   <a href="<?php echo base_url('business_profile/business_profile_commentpost/'.$row['business_profile_post_id']. ''); ?>">view all comments</a>


                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                        }}
                    
                        ?>
                                        <div class="col-md-1">
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
    </section>
    <footer>

        <footer>
             <?php echo $footer;  ?>
        </footer>




</body>

</html>
<!-- script for skill textbox automatic start (option 2)-->
<script src="<?php echo base_url('js/jquery-ui.min.js'); ?>"></script>
<script src="<?php echo base_url('js/jquery.wallform.js'); ?>"></script>
<script src="<?php echo base_url('js/select2-4.0.3.min.js'); ?>"></script>
<!-- script for skill textbox automatic end (option 2)-->

<script src="<?php echo base_url('js/fb_login.js'); ?>"></script>
<script>

//select2 autocomplete start for Location
$('#searchplace').select2({
        
        placeholder: 'Find Your Location',
         maximumSelectionLength: 1,
        ajax:{

         
          url: "<?php echo base_url(); ?>business_profile/location",
          dataType: 'json',
          delay: 250,
          
          processResults: function (data) {
            
            return {
             
              results: data


            };
            
          },
           cache: true
        }
      });
//select2 autocomplete End for Location

</script>


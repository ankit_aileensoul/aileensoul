<!--start head -->
<?php  echo $head; ?>
    <!-- END HEAD -->
    <!-- start header -->
<?php echo $header; ?>
    <!-- END HEADER -->
    <body class="page-container-bg-solid page-boxed">

      <section>
        
        <div class="user-midd-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <div class="left-side-bar">
                            <ul>
                                <li><a href="<?php echo base_url('business_profile/business_information_update'); ?>">Business information</a></li>
                                <li><a href="<?php echo base_url('business_profile/contact_information'); ?>">Contact information</a></li>
                                <li><a href="<?php echo base_url('business_profile/description'); ?>">Description</a></li>
                                <li><a href="<?php echo base_url('business_profile/image'); ?>">Images</a></li>
                                <li <?php if($this->uri->segment(1) == 'business_profile'){?> class="active" <?php } ?>><a href="#">Add more</a></li>
                                
                            </ul>
                        </div>
                    </div>

                    <!-- middle section start -->
 
                    <div class="col-md-6 col-sm-8">

                    <div>
                        <?php
                                        if ($this->session->flashdata('error')) {
                                            echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
                                        }
                                        if ($this->session->flashdata('success')) {
                                            echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
                                        }?>
                    </div>

                        <div class="common-form">
                        <h3>Add More</h3>
                        
                            <?php echo form_open_multipart(base_url('business_profile/addmore_insert'), array('id' => 'businessaddmore','name' => 'businessaddmore','class' => 'clearfix')); ?>
                            
                            

                                <fieldset class="full-width">
                                    <label>Add More:</label>

                                     <textarea name="addmore" id="addmore" rows="4" cols="50" placeholder="Enter More Detail" style="resize: none;"><?php if($addmore1){ echo $addmore1; } ?></textarea>
                                    
                                </fieldset>
                                

                                <fieldset class="hs-submit full-width">
                                    

                                     <input type="reset">
                                    <a href="<?php echo base_url('business_profile/image'); ?>">Previous</a>
                                    
                                    <input type="submit"  id="submit" name="submit" value="Submit">
                                   
                                </fieldset>
                            </form>
                        </div>
                    </div>

                    <!-- middle section end -->

                    
                </div>
            </div>
        </div>
    </section>
   <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <!-- footer start -->
    
 <script src="<?php echo base_url('js/fb_login.js'); ?>"></script>  
</body>
</html>

    <!-- footer end -->
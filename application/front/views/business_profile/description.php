 <!--start head -->
 

<?php  echo $head; ?>
    <!-- END HEAD -->
    <!-- start header -->
<?php echo $header; ?>
    <!-- END HEADER -->
    <body class="page-container-bg-solid page-boxed">
 
      <section>
        
        <div class="user-midd-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <div class="left-side-bar">
                            <ul>
                                <li><a href="<?php echo base_url('business_profile/business_information_update'); ?>">Business information</a></li>

                                <li><a href="<?php echo base_url('business_profile/contact_information'); ?>">Contact information</a></li>

                                <li <?php if($this->uri->segment(1) == 'business_profile'){?> class="active" <?php } ?>><a href="#">Description</a></li>

                                <li class="<?php if($businessdata[0]['business_step'] < '3'){echo "khyati";}?>"><a href="<?php echo base_url('business_profile/image'); ?>">Images</a></li>

                                <li class="<?php if($businessdata[0]['business_step'] < '3'){echo "khyati";}?>"><a href="<?php echo base_url('business_profile/addmore'); ?>">Add more</a></li>
                                
                            </ul>
                        </div>
                    </div>

                    <!-- middle section start -->
 
                    <div class="col-md-6 col-sm-8">

                     <div>
                        <?php
                                        if ($this->session->flashdata('error')) {
                                            echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
                                        }
                                        if ($this->session->flashdata('success')) {
                                            echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
                                        }?>
                    </div>

                        <div class="common-form">
                        <h3>
                        Description
                        </h3>
                        
                            <?php echo form_open(base_url('business_profile/description_insert'), array('id' => 'businessdis','name' => 'businessdis','class' => 'clearfix')); ?>

                            <div><span style="color:red">Fields marked with asterisk (*) are mandatory</span></div>


                                <?php
                                 $business_type =  form_error('business_type');
                                 $industriyal =  form_error('industriyal');
                                 $subindustriyal =  form_error('subindustriyal');
                                 $business_details =  form_error('business_details');
                                

                         ?> 

                                <fieldset <?php if($business_type) {  ?> class="error-msg" <?php } ?>>
                                    <label>Business Type:<span style="color:red">*</span></label>
                                     <select name="business_type" id="business_type" onchange="busSelectCheck(this);">
                                        <!-- <option value="" selected option disabled>Select Business type</option> -->
                                   <?php 
                                    if($business_type1){
                $businessname =  $this->db->get_where('business_type',array('type_id' => $business_type1))->row()->business_name; 
                ?>
            <option value="<?php echo $business_type1; ?>"><?php echo $businessname; ?></option>
                                           <?php
                                        foreach($businesstypedata as $cnt){
                                        ?>
                     <option value="<?php echo $cnt['type_id']; ?>"><?php echo $cnt['business_name']; ?></option>
                     
                                 <?php   }?>
                <option id="busOption" value="0" <?php if($business_type1 == 0){ echo "selected";} ?>>Any Other</option>
                             <?php  }
                                     else{
                                        if(count($businesstypedata) > 0){ ?>
                                             <option value="">Select Business Type</option>
                                      <?php  foreach($businesstypedata as $cnt){
                                        ?>
                                       
                                        <option value="<?php echo $cnt['type_id']; ?>"><?php echo $cnt['business_name']; ?></option>
                                    <?php }} ?>
                                    <option id="busOption" value="0" <?php if($business_type1 == 0){ echo "selected";} ?>>Any Other</option>
                                    <?php }
                                    ?>

                                    </select>
                                    <?php echo form_error('business_type'); ?>
                                </fieldset>



                             
                                 <fieldset <?php if($industriyal) {  ?> class="error-msg" <?php } ?>>
                                    <label>Category:<span style="color:red">*</span></label>
                                   
                                    <select name="industriyal" id="industriyal" onchange="indSelectCheck(this);">
                                       
                                     
    <!-- <option id="indOption" value="0" <?php if($industriyal1 == 0){ echo "selected";} ?>>Any Other</option> -->  
    
                                        <?php 
                                        if($industriyal1){ 
        
                 $industryname =  $this->db->get_where('industry_type',array('industry_id' => $industriyal1))->row()->industry_name; ?>

                                    <option value="<?php echo $industriyal1; ?>"><?php echo $industryname; ?></option>
                                    <?php 

                                    foreach($industriyaldata as $cnt){
                                        ?>
                                        <option value="<?php echo $cnt['industry_id']; ?>"><?php echo $cnt['industry_name']; ?></option>

                                        <?php }
                                    ?>
                                <option id="indOption" value="0" <?php if($industriyal1 == 0){ echo "selected";} ?>>Any Other</option>  
                                        <?php }

                                else{ 

                                    ?>
                                    <option value="">Select Category</option>
                                    <?php
                                        if(count($industriyaldata) > 0){ 
                                        foreach($industriyaldata as $cnt){
                                        ?>
                                        
                                        <option value="<?php echo $cnt['industry_id']; ?>"><?php echo $cnt['industry_name']; ?></option>
                                    <?php }} ?>
                                    <option id="indOption" value="0" <?php if($industriyal1 == 0){ echo "selected";} ?>>Any Other</option>

                                <?php }   
                                    ?>
                                    </select>

                                    <?php echo form_error('industriyal'); ?>
                                </fieldset>

      


<div id="busDivCheck" <?php if($business_type1 != 0){ ?>style="display:none" <?php } ?>>
 <fieldset <?php if($subindustrial) {  ?> class="error-msg" <?php } ?> class="full-width">
                                    <label>Add Here Your Other Business type:<span style="color:red">*</span></label>

                <input type="text" name="bustype" value="<?php echo $other_business; ?>">
                                    <?php echo form_error('subindustriyal'); ?>
                                </fieldset>
                                 </div>
<!-- khyati changes 7-4 start -->
<div id="indDivCheck" <?php if($industriyal1 != 0){ ?>style="display:none" <?php } ?>>
 <!-- khyati changes 7-4 emnd -->
 <fieldset <?php if($subindustrial) {  ?> class="error-msg" <?php } ?> class="full-width">
                                    <label>Add Here Your Other Category type:<span style="color:red">*</span></label>

<input type="text" name="indtype" value="<?php echo $other_industry; ?>">
                                    <?php echo form_error('subindustriyal'); ?>
                                </fieldset>
                                </div>
                                

                               
                                

                                <fieldset <?php if($business_details) {  ?> class="error-msg" <?php } ?> class="full-width">
                                    <label>Details of your business:<span style="color:red">*</span></label>


                                     <textarea name="business_details" id="business_details" rows="4" cols="50" placeholder="Enter Business Detail" style="resize: none;"><?php if($business_details1){ echo $business_details1; } ?></textarea>
                                    <?php echo form_error('business_details'); ?>
                                   
                                    
                                </fieldset>
                                

                                <fieldset class="hs-submit full-width">
                                    
                                    <input type="reset">
                                    <a href="<?php echo base_url('business_profile/contact_information'); ?>">Previous</a>

                                    <input type="submit"  id="next" name="next" value="Next">
                                    
                                    
                                </fieldset>
                                
                            </form>
                        </div>
                    </div>

                    <!-- middle section end -->

                    
                </div>
            </div>
        </div>
    </section>
   <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <!-- footer start -->
    <footer>
        
        <?php echo $footer;  ?>
    </footer>
   
</body>
</html>

  <script type="text/javascript" src="<?php echo site_url('js/jquery-ui.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.js'); ?>"></script>


<script src="<?php echo base_url('js/fb_login.js'); ?>"></script> 

<script>
function busSelectCheck(nameSelect)
{//alert("hi");
    if(nameSelect){
        busOptionValue = document.getElementById("busOption").value;
        if(busOptionValue == nameSelect.value){
            document.getElementById("busDivCheck").style.display = "block";
        }
        else{
            document.getElementById("busDivCheck").style.display = "none";
        }
    }
    else{
        document.getElementById("busDivCheck").style.display = "none";
    }
}
</script>


<script>
function indSelectCheck(nameSelect)
{//alert("ind");
    if(nameSelect){
        indOptionValue = document.getElementById("indOption").value;
        if(indOptionValue == nameSelect.value){
            document.getElementById("indDivCheck").style.display = "block";
        }
        else{
            document.getElementById("indDivCheck").style.display = "none";
        }
    }
    else{
        document.getElementById("indDivCheck").style.display = "none";
    }
}
</script>


<script type="text/javascript">
$(document).ready(function(){
    $('#industriyal').on('change',function(){ 
        var industriyalID = $(this).val();
        if(industriyalID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url() . "business_profile/ajax_data"; ?>',
                data:'industry_id='+industriyalID,
                success:function(html){
                    $('#subindustriyal').html(html);
                   
                }
            }); 
        }else{
            $('#subindustriyal').html('<option value="">Select industriyal first</option>');
             
        }
    });
    
    
});
</script>





<script type="text/javascript">

            //validation for edit email formate form

            $(document).ready(function () { 

                $("#businessdis").validate({

                    rules: {

                        business_type: {

                            required: true,
                             
                           
                        },

                         industriyal: {

                            required: true,
                            
                           
                        },
                       subindustriyal: {

                            required: true,
                            
                           
                        },

                         business_details: {

                            required: true,
                           
                        },

                     //khyati changes 7-4 start 

                        bustype: {

                            required: true,
                           
                        },
                        indtype: {

                            required: true,
                           
                        },

         //khyati changes 7-4 end

                    },

                    messages: {

                        business_type: {

                            required: "Business type Is Required.",
                            
                        },

                        industriyal: {

                            required: "Industrial Is Required.",
                            
                        },
                        subindustriyal: {

                            required: "Subindustrial Is Required.",
                            
                        },
                     //khyati changes 7-4 start 

                        bustype: {

                            required: "Other busineess type Is Required.",
                            
                        },
                        indtype: {

                    required: "Other industrial type Is Required.",
                            
                        },

                     //khyati changes 7-4 end 

                        business_details: {

                            required: "Business details Is Required.",
                            
                        },
                    },

                });
                   });
  </script>




    <!-- footer end -->
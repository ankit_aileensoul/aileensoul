<!--start head -->
<?php  echo $head; ?>
    <!-- END HEAD -->
    <!-- start header -->
<?php echo $header; ?>
    <!-- END HEADER -->
    <body class="page-container-bg-solid page-boxed">

      <section>
        
        <div class="user-midd-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <div class="left-side-bar">
                            <ul>
                                <li><a href="<?php echo base_url('business_profile/business_information_update'); ?>">Business information</a></li>

                                <li><a href="<?php echo base_url('business_profile/contact_information'); ?>">Contact information</a></li>

                                <li><a href="<?php echo base_url('business_profile/description'); ?>">Description</a></li>

                                <li <?php if($this->uri->segment(1) == 'business_profile'){?> class="active" <?php } ?>><a href="#">Images</a></li>

                                <li class="<?php if($businessdata[0]['business_step'] < '4'){echo "khyati";}?>"><a href="<?php echo base_url('business_profile/addmore'); ?>">Add more</a></li>
                                
                            </ul>
                        </div>
                    </div>

                    <!-- middle section start -->
 
                    <div class="col-md-6 col-sm-8">

                     <div>
                        <?php
                                        if ($this->session->flashdata('error')) {
                                            echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
                                        }
                                        if ($this->session->flashdata('success')) {
                                            echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
                                        }?>
                    </div>
                    
                        <div class="common-form"> 
                            <h3>Images</h3>
                        
                            <?php echo form_open_multipart(base_url('business_profile/image_insert'), array('id' => 'businessimage','name' => 'businessimage','class' => 'clearfix')); ?>
                           

                                <fieldset class="full-width">
                                    <label>Images:</label>
                                    <input type="file"  name="image1" id="image1"> 

                                    <?php if($image1){ 
                                        ?>
                                            <input type="hidden" name="filename" id="filename" value="<?php echo $image1; ?>">
                                            <img src="<?php echo base_url(BUSINESSPROFILEIMAGE.$image1)?>" style="width:100px;height:100px;"><br/><br/>
                                        <?php } ?>
                                   
                                </fieldset>
                                
                               <fieldset class="hs-submit full-width">
                                   

                                    <input type="reset">

                                    <a href="<?php echo base_url('business_profile/description'); ?>">Previous</a>
                                    
                                    <input type="submit"  id="next" name="next" value="Next">
                                   
                                    
                                </fieldset>

                            </form>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </section>
   <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <!-- footer start -->
    <footer>
        
        <?php echo $footer;  ?>
    </footer>
    
</body>
</html>

  <script type="text/javascript" src="<?php echo site_url('js/jquery-ui.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.js'); ?>"></script>

<script src="<?php echo base_url('js/fb_login.js'); ?>"></script>

    <!-- footer end -->
<!--start head -->
<?php  echo $head; ?>
    <!-- END HEAD -->
    <!-- start header -->
<?php echo $header; ?>
    <!-- END HEADER -->
    <body class="page-container-bg-solid page-boxed">

      <section>
        
        <div class="user-midd-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <div class="left-side-bar">
                            <ul>
                                <li><a href="<?php echo base_url('business_profile/business_information_update'); ?>">Business information</a></li> 

                                <li <?php if($this->uri->segment(1) == 'business_profile'){?> class="active" <?php } ?>><a href="#">Contact information</a></li>

                                <li class="<?php if($businessdata[0]['business_step'] < '2'){echo "khyati";}?>"><a href="<?php echo base_url('business_profile/description'); ?>">Description</a></li>

                                <li class="<?php if($businessdata[0]['business_step'] < '2'){echo "khyati";}?>"><a href="<?php echo base_url('business_profile/image'); ?>">Images</a></li>

                                <li class="<?php if($businessdata[0]['business_step'] < '2'){echo "khyati";}?>"><a href="<?php echo base_url('business_profile/addmore'); ?>">Add more</a></li>
                                
                            </ul>
                        </div>
                    </div>

                    <!-- middle section start -->
 
                    <div class="col-md-6 col-sm-8">

                     <div>
                        <?php
                                        if ($this->session->flashdata('error')) {
                                            echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
                                        }
                                        if ($this->session->flashdata('success')) {
                                            echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
                                        }?>
                    </div>

                        <div class="common-form">
                        <h3>
                            Contact information
                        </h3>
                        
                            <?php echo form_open(base_url('business_profile/contact_information_insert'), array('id' => 'contactinfo','name' => 'contactinfo','class' => 'clearfix')); ?>

                            <div><span style="color:red">Fields marked with asterisk (*) are mandatory</span></div> 

                                <?php
                                     $contactname =  form_error('contactname');
                                     $contactmobile =  form_error('contactmobile');
                                     $contactemail =  form_error('email');
                                     $contactwebsite =  form_error('contactwebsite');
                                ?>

                                <fieldset <?php if($contactname) {  ?> class="error-msg" <?php } ?>>
                                    <label>Contact Person:<span style="color:red">*</span></label>
                                    <input name="contactname" type="text" id="contactname" placeholder="Enter contact Name" value="<?php if($contactname1){ echo $contactname1; } ?>"/>
                                    <?php echo form_error('contactname'); ?>
                                </fieldset>
                                
<!-- khyati changes 7-4 start -->
                                 <fieldset <?php if($contactmobile) {  ?> class="error-msg" <?php } ?>>
                                    <label>Contact numberer:<span style="color:red">*</span></label>
                                    <input name="contactmobile" type="text" id="contactmobile" placeholder="Enter contact Mobile" value="<?php if($contactmobile1){ echo $contactmobile1; } ?>"/>
                                     <?php echo form_error('contactmobile'); ?> 
                                </fieldset>
                               
<!-- khyati changes 7-4 end -->


                                <fieldset <?php if($contactemail) {  ?> class="error-msg" <?php } ?>>
                                    <label>Contact Email:<span style="color:red">*</span></label>
                                    <input name="email" type="text" id="email" placeholder="Enter Contact Email" value="<?php if($contactemail1){ echo $contactemail1; } ?>"/>
                                    <?php echo form_error('email'); ?>
                                </fieldset>
                                

                                <fieldset>
                                    <label>Contact Website:</label>
                                    <input name="contactwebsite" type="text" id="contactwebsite" placeholder="Enter Contact website" value="<?php if($contactwebsite1){ echo $contactwebsite1; } ?>"/>
                                 
                                </fieldset>
                                

                                <fieldset class="hs-submit full-width">
                                   

                                     <input type="reset">
                                     
                                    <a href="<?php echo base_url('business_profile/business_information_update'); ?>">Previous</a>

                                    <input type="submit"  id="next" name="next" value="Next">
                                    
                                   
                                </fieldset>

                            </form>
                        </div>
                    </div>



 

                </div>
            </div>
        </div>
    </section>
   <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <!-- footer start -->
    
<?php echo $footer; ?>
     
</body>
</html>

  <script type="text/javascript" src="<?php echo site_url('js/jquery-ui.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.js'); ?>"></script>


<script src="<?php echo base_url('js/fb_login.js'); ?>"></script>
<script type="text/javascript">

            //validation for edit email formate form

            $(document).ready(function () { 

                $("#contactinfo").validate({

                    rules: {

                        contactname: {

                            required: true,
                             
                           
                        },

                         contactmobile: {

                            required: true,
                            minlength:10,
                            maxlength:11,
                            number: true,
                            
                           
                        },
                       email: {
                            required: true,
                            email: true,
                            remote: {
                                url: "<?php echo site_url() . 'business_profile/check_email' ?>",
                                type: "post",
                                data: {
                                    email: function () {
                                        return $("#email").val();
                                    },
                                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                                },
                            },
                        },

                       
                    },
// khyati changes 7-4 start
                    messages: {

                        contactname: {

                            required: "Contact name Is Required.",
                            
                        },

                        contactmobile: {

                            required: "Contact number Is Required.",
                            
                        },
                        email: {
                            required: "Contact Email is required",
                            email: "Please enter valid email id",
                            remote: "Email already exists"
                        },

                        
                    },

// khyati changes 7-4 end


                });
                   });
  </script>


 
    <!-- footer end -->
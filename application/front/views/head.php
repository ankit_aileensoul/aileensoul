<head> 
    <meta charset="utf-8" />
<!-- seo changes start -->
<meta name="google-site-verification" content="BKzvAcFYwru8LXadU4sFBBoqd0Z_zEVPOtF0dSxVyQ4" />

<!-- seo changes start -->

<!--Need to add following TAG in Header.-->

<link rel="canonical" href="http://www.aileensoul.com" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<meta name="description" content=" " />
<meta name="keywords" content=" " />
<!-- <link href="css/bootstrap.css" rel="stylesheet" type="text/css"> -->

<!-- Add following GoogleAnalytics tracking code in Header.-->
<!-- 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-91486853-1', 'auto');
  ga('send', 'pageview');

</script>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-6060111582812113",
    enable_page_level_ads: true
  });
</script>

<!-- seo changes end --> 

<!-- seo changes end -->
    <title><?php echo $title; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
      <link rel="icon" href="<?php echo base_url('images/favicon.png'); ?>">
    <!-- css start -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/common-style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/media.css'); ?>">
    <!-- <link rel="stylesheet" type="text/css" href="<?php// echo base_url('css/bootstrap.css'); ?>"> -->

    <link href="<?php echo base_url('css/lato.css'); ?>" rel="stylesheet"> 
  
    
</head>
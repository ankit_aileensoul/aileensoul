<!-- start head -->
<?php echo $head; ?>



<!--post save success pop up style strat -->
<style>
    body {
        font-family: Arial, sans-serif;
        background-size: cover;
        height: 100vh;
    }

    .box {
        width: 40%;
        margin: 0 auto;
        background: rgba(255,255,255,0.2);
        padding: 35px;
        border: 2px solid #fff;
        border-radius: 20px/50px;
        background-clip: padding-box;
        text-align: center;
    }



    .overlay {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background: rgba(0, 0, 0, 0.3);
        transition: opacity 500ms;
        visibility: hidden;
        opacity: 0;
        z-index: 10;
    }
    .overlay:target {
        visibility: visible;
        opacity: 1;
    }

    .popup {
        margin: 70px auto;
        padding: 20px;
        background: #fff;
        border-radius: 5px;
        width: 30%;
        height: 200px;
        position: relative;
        transition: all 5s ease-in-out;
    }

    .okk{
        text-align: center;
    }

    .popup .okbtn {
        position: absolute;
        transition: all 200ms;
        font-size: 18px;
        font-weight: bold;
        text-decoration: none;
        color: #fff;
        padding: 8px 18px;
        background-color: darkcyan;
        left: 25px;
        margin-top: 15px;
        width: 100px; 
        border-radius: 8px;
    }

    .popup .cnclbtn {
        position: absolute;
        transition: all 200ms;
        font-size: 18px;
        font-weight: bold;
        text-decoration: none;
        color: #fff;
        padding: 8px 18px;
        background-color: darkcyan;
        right: 25px;
        margin-top: 15px;
        width: 100px;
        border-radius: 8px;
    }

    .popup .pop_content {
        text-align: center;
        margin-top: 40px;

    }

    @media screen and (max-width: 700px){
        .box{
            width: 70%;
        }
        .popup{
            width: 70%;
        }
    }
</style>

<!--post save success pop up style end -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-3.min.css'); ?>">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<script src="<?php echo base_url('js/fb_login.js'); ?>"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />


<style type="text/css" media="screen">
    #row2 { overflow: hidden; width: 100%; }
    #row2 img { height: 350px;width: 100%; }
    .upload-img{    float: right;
                    position: relative;
                    margin-top: -135px;
                    right: 50px; }

    label.cameraButton {
        display: inline-block;
        margin: 1em 0;
        cursor: pointer;
        /* Styles to make it look like a button */
        padding: 0.5em;
        border: 2px solid #666;
        border-color: #EEE #CCC #CCC #EEE;
        background-color: #DDD;
        opacity: 0.7;
    }

    /* Look like a clicked/depressed button */
    label.cameraButton:active {
        border-color: #CCC #EEE #EEE #CCC;
    }

    /* This is the part that actually hides the 'Choose file' text box for camera inputs */
    label.cameraButton input[accept*="camera"] {
        display: none;
    }




</style>
<!-- END HEAD -->
<!-- start header -->
<?php echo $header; ?>



<!-- END HEADER -->
<?php echo $recruiter_header2; ?>

<body   class="page-container-bg-solid page-boxed">

    <section>
        <div class="container">
            <div class="row" id="row1" style="display:none;">
                <div class="col-md-12 text-center">
                    <div id="upload-demo" style="width:100%"></div>
                </div>
                <div class="col-md-12 cover-pic" style="padding-top: 25px;text-align: center;">
                    <button class="btn btn-success  cancel-result" onclick="">Cancel</button>

                    <button class="btn btn-success upload-result set-btn" onclick="myFunction()">Upload Image</button>

                    <div id="message1" style="display:none;">
                        <div id="floatBarsG">
                            <div id="floatBarsG_1" class="floatBarsG"></div>
                            <div id="floatBarsG_2" class="floatBarsG"></div>
                            <div id="floatBarsG_3" class="floatBarsG"></div>
                            <div id="floatBarsG_4" class="floatBarsG"></div>
                            <div id="floatBarsG_5" class="floatBarsG"></div>
                            <div id="floatBarsG_6" class="floatBarsG"></div>
                            <div id="floatBarsG_7" class="floatBarsG"></div>
                            <div id="floatBarsG_8" class="floatBarsG"></div>
                        </div>

                    </div>
                </div>
                <div class="col-md-12"  style="visibility: hidden; ">
                    <div id="upload-demo-i" style="background:#e1e1e1;width:100%;padding:30px;height:300px;margin-top:30px"></div>
                </div>
            </div>


            <div class="container">
                <div class="row" id="row2">
                    <?php
                    $userid = $this->session->userdata('aileenuser');
                    $contition_array = array('user_id' => $userid, 'is_delete' => '0', 're_status' => '1');
                    $image = $this->common->select_data_by_condition('recruiter', $contition_array, $data = 'profile_background', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');
                    //echo "<pre>";print_r($image);
                    $image_ori = $image[0]['profile_background'];
                    if ($image_ori) {
                        ?>

                        <div class="bg-images">
                            <img src="<?php echo base_url(RECBGIMAGE . $recdata[0]['profile_background']); ?>" name="image_src" id="image_src" / ></div>
                        <?php
                    } else {
                        ?>

                        <div class="bg-images">
                            <img src="<?php echo base_url(WHITEIMAGE); ?>" name="image_src" id="image_src" / ></div>
                    <?php }
                    ?>


                </div>
            </div>
        </div>
    </div>
</div>   

<div class="container">    
    <div class="upload-img">


        <label class="cameraButton"><i class="fa fa-camera" aria-hidden="true"></i>
            <input type="file" id="upload" name="upload" accept="image/*;capture=camera" onclick="showDiv()">
        </label>
    </div>
    <!--     </div>
    -->
    <div class="profile-photo">
        <div class="profile-pho">

            <div class="user-pic">
                <?php if ($recdata[0]['recruiter_user_image'] != '') { ?>
                    <img src="<?php echo base_url(USERIMAGE . $recdata[0]['recruiter_user_image']); ?>" alt="" >
                <?php } else { ?>
                    <img alt="" class="img-circle" src="<?php echo base_url(NOIMAGE); ?>" alt="" />
                <?php } ?>
                <a href="#popup-form" class="fancybox"><i class="fa fa-camera" aria-hidden="true"></i> Update Profile Picture</a>

            </div>

            <div id="popup-form">
                <?php echo form_open_multipart(base_url('recruiter/user_image_insert'), array('id' => 'userimage', 'name' => 'userimage', 'class' => 'clearfix')); ?>
                <input type="file" name="profilepic" accept="image/gif, image/jpeg, image/png" id="profilepic">
                <input type="hidden" name="hitext" id="hitext" value="3">
                <input type="submit" name="cancel3" id="cancel3" value="Cancel">
                <input type="submit" name="profilepicsubmit" id="profilepicsubmit" value="Save">
                </form>

            </div>


        </div>


        <!-- menubar --><div class="profile-main-rec-box-menu  col-md-12 ">

            <div class="left-side-menu col-md-2">  </div>
            <div class="right-side-menu col-md-9">
                <ul class="">



                    <li <?php if ($this->uri->segment(1) == 'recruiter' && $this->uri->segment(2) == 'rec_profile') { ?> class="active" <?php } ?>><a href="<?php echo base_url('recruiter/rec_profile'); ?>">Profile</a>
                    </li>


                    <?php if (($this->uri->segment(1) == 'recruiter') && ($this->uri->segment(2) == 'rec_post' || $this->uri->segment(2) == 'rec_profile' || $this->uri->segment(2) == 'add_post' || $this->uri->segment(2) == 'save_candidate') && ($this->uri->segment(3) == $this->session->userdata('aileenuser') || $this->uri->segment(3) == '')) { ?>

                        <li <?php if ($this->uri->segment(1) == 'recruiter' && $this->uri->segment(2) == 'rec_post') { ?> class="active" <?php } ?>><a href="<?php echo base_url('recruiter/rec_post'); ?>">Post</a>
                        </li>


                        <li <?php if ($this->uri->segment(1) == 'recruiter' && $this->uri->segment(2) == 'save_candidate') { ?> class="active" <?php } ?>><a href="<?php echo base_url('recruiter/save_candidate'); ?>">Saved </a>
                        </li> 
                        <fa>
                            </li> 


                        <?php } ?>   
                </ul>
            </div>

        </div>  
        <!-- menubar -->    
    </div>                        <div class="job-menu-profile1">
        <a href="<?php echo site_url('recruiter/rec_profile/' . $recruiterdata[0]['user_id']); ?>"><h5><?php echo $recdata[0]['rec_firstname'] . ' ' . $recdata[0]['rec_lastname']; ?></h5></a>
        <!-- text head start -->
        <div class="profile-text" >

            <?php
            if ($recdata[0]['designation'] == '') {
                ?>
                <a id="myBtn">Designation</a>
            <?php } else { ?> 
                <a id="myBtn"><?php echo ucwords($recdata[0]['designation']); ?></a>
            <?php } ?>


            <!-- The Modal -->
            <div id="myModal" class="modal">
                <!-- Modal content --><div class="col-md-2"></div>
                <div class="modal-content col-md-8">
                    <span class="close">&times;</span>
                    <fieldset></fieldset>
                    <?php echo form_open(base_url('recruiter/recruiter_designation/'), array('id' => 'recdesignation', 'name' => 'recdesignation', 'class' => 'clearfix')); ?>

                    <fieldset class="col-md-8"> <input type="text" name="designation" id="designation" placeholder="Enter Your Designation" value="<?php echo $recruiterdata[0]['designation']; ?>"></fieldset>
                    <input type="hidden" name="hitext" id="hitext" value="4">
                    <fieldset class="col-md-2"><input type="submit"  id="submitdes" name="submitdes" value="Submit"></fieldset>
                    <?php echo form_close(); ?>



                </div>

                <div class="col-md-2">

                </div>

            </div>

        </div>
        <div  class="add-post-button">
<!--   <a href="" class="btn btn-2 btn-2i"><i class="fa fa-plus" aria-hidden="true"></i></a> -->
            <a class="btn btn-3 btn-3b" href="<?php echo base_url('recruiter/add_post'); ?>"><i class="fa fa-plus" aria-hidden="true"></i>  Add Post</a>
        </div>

        <!-- text head end -->
    </div>
    <div class="col-md-7 col-sm-7">
        <div class="common-form">
            <div class="job-saved-box">

                <h3>Post</h3>
                <div class="contact-frnd-post">
                    <div class="job-contact-frnd ">

                        <!-- khyati start -->


                        <?php

                        function text2link($text) {
                            $text = preg_replace('/(((f|ht){1}t(p|ps){1}:\/\/)[-a-zA-Z0-9@:%_\+.~#?&\/\/=]+)/i', '<a href="\\1" target="_blank" rel="nofollow">\\1</a>', $text);
                            $text = preg_replace('/([[:space:]()[{}])(www.[-a-zA-Z0-9@:%_\+.~#?&\/\/=]+)/i', '\\1<a href="http://\\2" target="_blank" rel="nofollow">\\2</a>', $text);
                            $text = preg_replace('/([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})/i', '<a href="mailto:\\1" rel="nofollow" target="_blank">\\1</a>', $text);
                            return $text;
                        }
                        ?>

                        <?php
                        if ($falguni == 1) {

                            foreach ($postdata as $post_key => $post_value) {
                                foreach ($post_value as $post) {
                                    ?>
                                    <div class="profile-job-post-detail clearfix" id="<?php echo "removepost" . $post['post_id']; ?>">
                                        <div class="profile-job-post-title-inside clearfix">

                                            <div class="profile-job-post-location-name">
                                                <ul>
                                                    <li><?php echo $post['rec_firstname']; ?></li>
                                                    <li><?php echo $post['post_name']; ?></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="profile-job-post-title clearfix">

                                            <!-- pop up box start-->
                                            <div id="popup1" class="overlay">
                                                <div class="popup">

                                                    <div class="pop_content">
                                                        Are You Sure want to delete this post?.

                                                        <p class="okk"><a class="okbtn" id="<?php echo $post['post_id']; ?>" onClick="remove_post(this.id)">ok</a></p>

                                                        <p class="okk"><a class="cnclbtn" href="#">Cancle</a></p>

                                                    </div>

                                                </div>
                                            </div>
                                            <!-- pop up box end-->
                                            <div class="profile-job-profile-button clearfix">
                                                <div class="profile-job-details">
                                                    <ul>
                                                        <li>
                                                            <!-- khyati 5-4 changes start -->                                      
                                                            <?php
                                                            if ($post['min_year'] != 0 && $post['min_month'] != 0) {
                                                                echo "Min :" . $post['min_year'] . " year -" . $post['min_month'] . " month <br>";
                                                            } elseif ($post['min_year'] == 0) {
                                                                echo "Min :" . $post['min_month'] . " month <br>";
                                                            } elseif ($post['min_month'] == 0) {
                                                                echo "Min :" . $post['min_year'] . " year <br>";
                                                            }


                                                            if ($post['max_year'] != 0 && $post['max_month'] != 0) {
                                                                echo "Max : " . $post['max_year'] . " year-" . $post['max_month'] . " month <br>";
                                                            } elseif ($post['max_year'] == 0) {
                                                                echo "Max : " . $post['max_year'] . " year-" . $post['max_month'] . " month <br>";
                                                            } elseif ($post['max_month'] == 0) {
                                                                echo " Max : " . $post['max_year'] . " year <br>";
                                                            }

                                                            if ($post['max_year'] == 0 && $post['max_month'] == 0 && $post['max_year'] == 0 && $post['max_month'] == 0) {
                                                                echo "Experience is not required";
                                                            }

// khyati changes end

                                                            if ($post['fresher'] == 1) {
                                                                echo "Fresher can also apply.. !!";
                                                            }
                                                            ?></p>
                                                        </li>
                                                        <li>
                                                            <p><?php echo $post['city']; ?></p>
                                                        </li>
                                                        <!-- <li>
                                                            <p> <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                                            </p>
                                                        </li> -->


                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="profile-job-profile-menu">
                                                <ul class="clearfix">
                                                    <li> <b> Skills: </b> <span> 
                                                            <?php
                                                            $skills = explode(',', $post['post_skill']);
                                                            foreach ($skills as $skil) {
                                                                $abc = $this->db->get_where('skill', array('skill_id' => $skil['post_skill']))->row()->skill;

                                                                echo $abc . " ";
                                                            }
                                                            ?>


                                                        </span>
                                                    </li>

                                                    <li> <b>Location:</b><span> <?php echo $this->db->get_where('cities', array('city_id' => $post['city']))->row()->city_name; ?>  </span>
                                                    </li>

                                                    <li> <b>Job Description:</b><span> 
                                                            <?php echo text2link($post['post_description']); ?>   </span>
                                                    </li>
                                                    <li><b>  <?php echo $post['min_sal'] . '-' . $post['max_sal'] . ' P.A'; ?> </b> <span></span> </li>

                                                </ul>
                                            </div>
                                            <?php if ($post['user_id'] == $userid) { ?>
                                                <div class="profile-job-profile-button clearfix">
                                                    <div class="apply-btn">

                                                        <a href="<?php echo base_url('recruiter/edit_post/' . $post['post_id']); ?>" class="button">Edit</a>

                                                        <a href="#popup1" class="button">Remove </a>

                                                        <a href="<?php echo base_url('recruiter/view_apply_list/' . $post['post_id']); ?>" class="button">Applied  Candidate : <?php echo count($this->common->select_data_by_id('job_apply', 'post_id', $post['post_id'], $data = '*', $join_str = array())); ?></a>

                                                    </div>
                                                </div>
                                            <?php } ?>

                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        } else {
//                            echo '<pre>';
//                            print_r($postdata);
//                            exit;
                            if ($postdata) {
                                foreach ($postdata as $post) {
                                    ?>

                                    <div class="profile-job-post-detail clearfix" id="<?php echo "removepost" . $post['post_id']; ?>">


                                        <div class="profile-job-post-title-inside clearfix">

                                            <!-- pop up box start-->
                                            <div id="popup1" class="overlay">
                                                <div class="popup">

                                                    <div class="pop_content">
                                                        Are you sure want to delete this post?.

                                                        <p class="okk"><a class="okbtn" id="<?php echo $post['post_id']; ?>" onClick="remove_post(this.id)">ok</a></p>

                                                        <p class="okk"><a class="cnclbtn" href="#">Cancle</a></p>

                                                    </div>

                                                </div>
                                            </div>
                                            <!-- pop up box end-->

                                            <div class="profile-job-post-location-name">
                                                <ul>
                                                    <li> <h5> <?php echo $post['rec_firstname']; ?> </h5></li>
                                                    <li> <h5>  <?php echo $post['post_name']; ?></h5></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="profile-job-post-title clearfix">
                                            <div class="profile-job-profile-button clearfix">
                                                <div class="profile-job-details">
                                                    <ul>


                                                        <li>
                                                            <p  title="  <?php echo $post['exp_month'] . "Month -" . $post['exp_year'] . " Year Required"; ?>"> <i class="fa fa-lock" aria-hidden="true"></i>
                                                                <!-- khyati 5-4 changes start -->                                      
                                                                <?php
                                                                if ($post['min_year'] != 0 && $post['min_month'] != 0) {
                                                                    echo "Min :" . $post['min_year'] . " year -" . $post['min_month'] . " month <br>";
                                                                } elseif ($post['min_year'] == 0 && $post['min_month'] != 0) {
                                                                    echo "Min :" . $post['min_month'] . " month <br>";
                                                                } elseif ($post['min_month'] == 0 && $post['min_year'] != 0) {
                                                                    echo "Min :" . $post['min_year'] . " year <br>";
                                                                }


                                                                if ($post['max_year'] != 0 && $post['max_month'] != 0) {
                                                                    echo "Max : " . $post['max_year'] . " year-" . $post['max_month'] . " month <br>";
                                                                } elseif ($post['max_year'] == 0 && $post['max_month'] != 0) {
                                                                    echo "Max : " . $post['max_year'] . " year-" . $post['max_month'] . " month <br>";
                                                                } elseif ($post['max_month'] == 0 && $post['max_year'] != 0) {
                                                                    echo " Max : " . $post['max_year'] . " year <br>";
                                                                }

                                                                if ($post['max_year'] == 0 && $post['max_month'] == 0 && $post['max_year'] == 0 && $post['max_month'] == 0) {
                                                                    echo "Experience is not required";
                                                                }
// khyati changes end
                                                                if ($post['fresher'] == 1) {
                                                                    echo "Fresher can also apply.. !!";
                                                                }
                                                                ?></p>
                                                        </li>
                                                        <!--  <li>
                                                             <p><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $post['city']; ?></p>
                                                         </li> -->
                                                        <!-- <li>
                                                            <p> <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                                            </p>
                                                        </li> -->


                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="profile-job-profile-menu">
                                                <ul class="clearfix">
                                                    <li> <b> Skills </b> <span>
                                                            <?php
                                                            $comma = ",";
                                                            $k = 0;
                                                            $aud = $post['post_skill'];
                                                            $aud_res = explode(',', $aud);
                                                            foreach ($aud_res as $skill) {
                                                                if ($k != 0) {
                                                                    echo $comma;
                                                                }
                                                                $cache_time = $this->db->get_where('skill', array('skill_id' => $skill))->row()->skill;
                                                                //$skill1[]= $cache_time;

                                                                echo $cache_time;
                                                                $k++;
                                                            }
                                                            ?>    </span>
                                                    </li>

                                                    <li> <b>Location</b><span> <?php echo $this->db->get_where('cities', array('city_id' => $post['city']))->row()->city_name; ?>  </span>
                                                    </li>

                                                    <li> <b>Job Description</b><span> <?php echo $post['post_description']; ?>   </span>
                                                    </li>
                                                    <li><b> Salary  </b> <span> <?php echo $post['min_sal'] . '-' . $post['max_sal'] . ' P.A'; ?> </span></li>
                                                    <!-- 4 4 changes start -->
                                                    <li><b> Other skill  </b> <span><?php echo $post['post_description']; ?>    </span></li>
                                                    <li><b> Fresher can also applys  </b> <span> <?php echo $post['fresher']; ?>   </span></li>
                                                    <li><b> No of position </b> <span> <?php echo $post['post_position']; ?>   </span></li>
                                                    <li><b> Interview process </b> <span>  <?php echo $post['interview_process']; ?>  </span></li>
                                                    <li><b> Last date of apply </b> <span><?php echo $post['post_last_date']; ?>    </span></li>
                                                    <?php
                                                    $country = $this->db->get_where('countries', array('country_id' => $post['country']))->row()->country_name;
                                                    $state = $this->db->get_where('states', array('state_id' => $post['state']))->row()->state_name;
                                                    $city = $this->db->get_where('cities', array('city_id' => $post['city']))->row()->city_name;
                                                    ?>            
                                                    <li><b>Country</b> <span> <?php echo $country; ?>   </span></li>
                                                    <li><b> State </b> <span> <?php echo $state; ?>   </span></li>
                                                    <li><b> City </b> <span> <?php echo $city; ?>   </span></li>
                                                    <!-- 4 4 changes end -->


                                                </ul>
                                            </div>
                                            <?php if ($post['user_id'] == $userid) { ?>
                                                <div class="profile-job-profile-button clearfix">
                                                    <div class="apply-btn">

                                                        <a href="<?php echo base_url('recruiter/edit_post/' . $post['post_id']); ?>" class="button">Edit</a>

                                                        <a href="#popup1" class="button">Remove </a>
                                                        <a href="<?php echo base_url('recruiter/view_apply_list/' . $post['post_id']); ?>" class="button">Applied  Candidate : <?php echo count($this->common->select_data_by_id('job_apply', 'post_id', $post['post_id'], $data = '*', $join_str = array())); ?></a>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                            } else {
                                ?>
                                <div class="text-center rio">
                                    <h4 class="page-heading  product-listing" style="border:0px;margin-bottom: 11px;">No Post Found.</h4>
                                </div>

                                <?php
                            }
                        }
                        ?>
                        <!-- khyati end -->
                        <div class="col-md-1">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>





<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


    </head>
    <body>

        <div class="user-midd-section">
            <div class="container">
                <div class="row">


                    <div class="col-md-4">



                    </div>


                    <!--- search end -->

                </div>
            </div>
        </div>
</section>
<footer>

    <?php echo $footer; ?>


</body>

</html>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> --> -->




<!-- script for skill textbox automatic end (option 2)-->

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<script src="<?php echo base_url('js/jquery-ui.min.js'); ?>"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script src="<?php echo base_url('assets/js/croppie.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/css/croppie.css'); ?>">

<script>
                                                $(function () {

                                                    var complex = <?php echo json_encode($demo); ?>;


                                                    var availableTags = complex;
                                                    $("#tags").autocomplete({
                                                        source: availableTags
                                                    });
                                                });
</script> 


<script type="text/javascript">
    function checkvalue() {
        //alert("hi");
        var searchkeyword = document.getElementById('tags').value;
        var searchplace = document.getElementById('searchplace').value;
        // alert(searchkeyword);
        // alert(searchplace);
        if (searchkeyword == "" && searchplace == "") {
            // alert('Please enter Keyword');
            return false;
        }
    }
</script>



<!-- script for skill textbox automatic end (option 2)--> 
<script>
    $(function () {

        var complex = <?php echo json_encode($demo); ?>;

        // alert(complex);
        var availableTags = complex;
        $("#tags").autocomplete({
            source: availableTags
        });
    });
</script>

<script>
//select2 autocomplete start for skill
    $('#searchskills').select2({

        placeholder: 'Find Your Skills',

        ajax: {

            url: "<?php echo base_url(); ?>recruiter/keyskill",
            dataType: 'json',
            delay: 250,

            processResults: function (data) {

                return {
                    //alert(data);

                    results: data


                };

            },
            cache: true
        }
    });
//select2 autocomplete End for skill

//select2 autocomplete start for Location
    $('#searchplace').select2({

        placeholder: 'Find Your Location',
        maximumSelectionLength: 1,
        ajax: {

            url: "<?php echo base_url(); ?>recruiter/location",
            dataType: 'json',
            delay: 250,

            processResults: function (data) {

                return {
                    //alert(data);

                    results: data


                };

            },
            cache: true
        }
    });
//select2 autocomplete End for Location

</script>
<script>
// Get the modal
    var modal = document.getElementById('myModal');

// Get the button that opens the modal
    var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
    btn.onclick = function () {
        modal.style.display = "block";
    }

// When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }

// When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>

<!-- cover image start -->
<script>
    function myFunction() {
        document.getElementById("upload-demo").style.visibility = "hidden";
        document.getElementById("upload-demo-i").style.visibility = "hidden";
        document.getElementById('message1').style.display = "block";

        //setTimeout(function () { location.reload(1); }, 5000);

    }


    function showDiv() {
        document.getElementById('row1').style.display = "block";
        document.getElementById('row2').style.display = "none";
    }
</script>

<script type="text/javascript">
    $uploadCrop = $('#upload-demo').croppie({
        enableExif: true,
        viewport: {
            width: 1250,
            height: 350,
            type: 'square'
        },
        boundary: {
            width: 1250,
            height: 350
        }
    });


    $('.upload-result').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {

            $.ajax({
                url: "https://www.aileensoul.com/recruiter/ajaxpro",
                type: "POST",
                data: {"image": resp},
                success: function (data) {
                    html = '<img src="' + resp + '" />';
                    if (html) {
                        window.location.reload();
                    }
                }
            });

        });
    });

    $('.cancel-result').on('click', function (ev) {

        document.getElementById('row2').style.display = "block";
        document.getElementById('row1').style.display = "none";
        document.getElementById('message1').style.display = "none";


    });

//aarati code start
    $('#upload').on('change', function () {
        //alert("hi");


        var reader = new FileReader();
        //alert(reader);
        reader.onload = function (e) {
            $uploadCrop.croppie('bind', {
                url: e.target.result
            }).then(function () {
                console.log('jQuery bind complete');
            });

        }
        reader.readAsDataURL(this.files[0]);



    });

    $('#upload').on('change', function () {
        //alert("hi");
        // var data=new formData();
        // //alert(data);
        // $.each($("#upload")[0].files,function(i,file)
        //  {
        //    data.append("image",file);
        //  });
        //var fd = new FormData();

        //fd.append( "fileInput", $("#upload")[0].files[0]);
// var file = $('#upload')[0].files[0];
        var fd = new FormData();
        fd.append("image", $("#upload")[0].files[0]);

        files = this.files;
        size = files[0].size;

        //alert(size);

        if (size > 4194304)
        {
            //show an alert to the user
            alert("Allowed file size exceeded. (Max. 4 MB)")

            document.getElementById('row1').style.display = "none";
            document.getElementById('row2').style.display = "block";

            // window.location.href = "https://www.aileensoul.com/dashboard"
            //reset file upload control
            return false;
        }


        $.ajax({

            url: "<?php echo base_url(); ?>recruiter/image",
            type: "POST",
            data: fd,
            processData: false,
            contentType: false,
            success: function (response) {
                //alert(response);

            }
        });
    });

//aarati code end
</script>
<!-- cover image end -->
<!-- remove post start -->

<script type="text/javascript">
    function remove_post(abc)
    {


        $.ajax({
            type: 'POST',
            url: '<?php echo base_url() . "recruiter/remove_post" ?>',
            data: 'post_id=' + abc,
            success: function (data) {

                $('#' + 'removepost' + abc).html(data);


            }
        });




    }
</script>

<!-- remove  post end -->
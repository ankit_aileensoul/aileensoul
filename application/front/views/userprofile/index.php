<!-- <!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/common-style.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/media.css">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i" rel="stylesheet"> 
</head> -->
<?php  echo $head; ?>
<body class="profile-page">
	<header>
		<div class="header">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-5">
						<div class="logo"><a href="index.html"><img src="images/logo.png"></a></div>
					</div>
					<div class="col-md-8 col-sm-7">
						<ul>
							<li><a href="#">Dashborad</a></li>
							<li><a href="#">Notification <i class="fa fa-bell-slash-o" aria-hidden="true"></i></a></li>
							<li><a href="#">Inbox <i class="fa fa-commenting" aria-hidden="true"></i></a></li>
							<li><a href="#">Friend Request <i class="fa fa-user" aria-hidden="true"></i></a></li>
							<li><a href="#">Masari Odedara <i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section>
		<div class="">
        	<div class="profile-banner"></div>
        	<div class="profile-photo">
              <div class="profile-pho">
              	<div class="user-pic">
              		<img src="images/imgpsh_fullsize (2).jpg" alt="" >
              	</div>
               	<h6 align="center" class="profile-head-text">  Dhaval Shah </h6>
               		<div class="profile-text" >
         				<p align="center">Jr. Owner</p>
         			</div>
        	  </div>
			</div>
        </div>
		<div class="user-midd-section">
			<div class="container">
				<div class="row">
				<div class="col-md-2"></div>
					<div class="col-md-8 col-sm-8">
						<div class="mid-bar">
							<ul class="clearfix">
								<li><a href="#">Job Seeker</a></li>
								<li><a href="#">Recruiter</a></li>
								<li><a href="#">Freelancer</a></li>
								<li><a href="#">Bussiness Profile</a></li>
								<li><a href="#">Artistic Person</a></li>
								<li><a href="#">Artistic Person</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2">
					</div>

				</div>
			</div>
		</div>
	</section>
	<footer>
		<div class="footer text-center">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="footer-logo">
							<a href="index.html"><img src="http://www.aileensoul.com/images/logo-white.png"></a>
						</div>
						<ul>
							<li>E-912 Titanium City Center Anandngar Ahmedabad-380015</li>
							<li><a href="mailto:AileenSoul@gmail.com">AileenSoul@gmail.com</a></li>
							<li>+91 903-353-8102</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6">
						<p><i class="fa fa-copyright" aria-hidden="true"></i> 2017 All Rights Reserved </p>
					</div>
					<div class="col-md-6 col-sm-6">
						<ul>
							<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</footer>
</body>
</html>
<!-- HEAD Start -->

<?php echo $head; ?>
<!-- END HEAD -->

<!-- start header -->
<?php echo $header; ?>

<body>
    <section>

        <div class="user-midd-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <div class="left-side-bar">
                            <ul>
                                <li><a href="<?php echo base_url('freelancer/freelancer_post_basic_information'); ?>">Basic Info</a></li> 

                                <li><a href="<?php echo base_url('freelancer/freelancer_post_address_information'); ?>">Address Info</a></li>

                                <li><a href="<?php echo base_url('freelancer/freelancer_post_professional_information'); ?>">Professional Info</a></li>

                                <li <?php if ($this->uri->segment(1) == 'freelancer') { ?> class="active" <?php } ?>><a href="#">Rate</a></li>

                                <li class="<?php if ($freepostdata[0]['free_post_step'] < '4') {
    echo "khyati";
} ?>"><a href="<?php echo base_url('freelancer/freelancer_post_avability'); ?>">ADD Your Avability</a></li>

                                <li class="<?php if ($freepostdata[0]['free_post_step'] < '4') {
    echo "khyati";
} ?>"><a href="<?php echo base_url('freelancer/freelancer_post_education'); ?>"> Education</a></li>		    
                                <li class="<?php if ($freepostdata[0]['free_post_step'] < '4') {
    echo "khyati";
} ?>"><a href="<?php echo base_url('freelancer/freelancer_post_portfolio'); ?>">Portfolio</a></li>
                            </ul>

                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9">
                        <div>
                            <?php
                            if ($this->session->flashdata('error')) {
                                echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
                            }
                            if ($this->session->flashdata('success')) {
                                echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
                            }
                            ?>
                        </div>

                        <div class="common-form">
                            <h3>Rate</h3>
<?php echo form_open(base_url('freelancer/freelancer_post_rate_insert'), array('id' => 'freelancer_post_rate', 'name' => 'freelancer_post_rate', 'class' => 'clearfix')); ?>


                                <?php
                                $hourly = form_error('hourly');
                                ?>

                            <fieldset <?php if ($hourly) { ?> class="error-msg" <?php } ?>>
                                <label>Hourly:</label>
                                <input type="text" name="hourly" placeholder="Enter hourly"  value="<?php if ($hourly1) {
                                    echo $hourly1;
                                } ?>">
                                    <?php echo form_error('hourly'); ?>
                            </fieldset>

                            <fieldset>
                                <label>Currency:</label>
                                <select name="state">
                                    <option value="" selected option disabled>Select your currency</option>

                                    <?php
                                    if (count($currency) > 0) {
                                        foreach ($currency as $cnt) {
                                            if ($currency1) {
                                                ?>
                                                <option value="<?php echo $cnt['currency_id']; ?>" <?php if ($cnt['currency_id'] == $currency1) echo 'selected'; ?>><?php echo $cnt['currency_name']; ?></option>

                                                <?php
                                            }

                                            else {
                                                ?>
                                                <option value="<?php echo $cnt['currency_id']; ?>"><?php echo $cnt['currency_name']; ?></option>
        <?php }
    }
}
?>
                                </select> 
                                <?php ?>
                            </fieldset>

                            <fieldset>
                                <?php
                                if ($fixed_rate1 == 1) {
                                    ?>

                                    <input type="checkbox" name="fixed_rate" value="1" checked> Work On Fixed Rate<br>
                                    <?php
                                } else {
                                    ?>
                                    <input type="checkbox" name="fixed_rate" value="1"> Also Work On Fixed Rate<br>
    <?php
}
?>
                            </fieldset>


                            <fieldset class="hs-submit full-width">


<!--                                <input type="reset">
                                <a href="<?php echo base_url('freelancer/freelancer_post_professional_information'); ?>">Previous</a>-->

                                <input type="submit"  id="next" name="next" value="Next">


                            </fieldset>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer>

<?php echo $footer; ?>
    </footer>
</body>
</html>


<script type="text/javascript" src="<?php echo site_url('js/jquery-ui.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.js'); ?>"></script>

<script type="text/javascript">

    //validation for edit email formate form

    $(document).ready(function () {

        $("#freelancer_post_rate").validate({

            rules: {

                hourly: {

                    number: true,

                },

            },

            messages: {

            },

        });
    });
</script>

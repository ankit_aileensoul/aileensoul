<?php  echo $head; ?>
    <!-- END HEAD -->
    <!-- start header -->

   

<?php echo $header; ?>

<body>
	
	<section>
		
		<div class="user-midd-section">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-3">
						<div class="left-side-bar">
							<ul>
							<li><a href="<?php echo base_url('freelancer_hire/freelancer_hire_basic_info'); ?>">Basic Information</a></li>

                                <li><a href="<?php echo base_url('freelancer_hire/freelancer_hire_address_info'); ?>">Address Information</a></li>

								<li <?php if($this->uri->segment(1) == 'freelancer_hire'){?> class="active" <?php } ?>><a href="#">Professional Information</a></li>

                                
								
							</ul>
						</div>
					</div>
					<div class="col-md-9 col-sm-9">

					<div>
                        <?php
                                        if ($this->session->flashdata('error')) {
                                            echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
                                        }
                                        if ($this->session->flashdata('success')) {
                                            echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
                                        }?>
                    </div>
						<div class="common-form">
							<h3>Proessional Information</h3>
							<?php echo form_open_multipart(base_url('freelancer_hire/freelancer_hire_professional_info_insert'), array('id' => 'professional_info','name' => 'professional_info','class' => 'clearfix')); ?>

                     <div><span style="color:red">Fields marked with asterisk (*) are mandatory</span></div>

                     	 <?php
                         $professional_info =  form_error('professional_info');
                         ?>  
								
                            	<fieldset class="full-width">
									<label>Professional Info:<span style="color:red">*</span></label>
									
									<textarea name ="professional_info" id="professional_info" rows="4" cols="50" placeholder="Enter Professional Information" style="resize: none;"><?php if($professional_info1){ echo $professional_info1; } ?></textarea>
									 <?php echo form_error('professional_info'); ?> 
									 
								</fieldset>
								<fieldset class="hs-submit full-width">
                                    

                                     <input type="reset">
                                     <a href="<?php echo base_url('freelancer_hire/freelancer_hire_address_info'); ?>">Previous</a>
                                     
                                    <input type="submit"  id="next" name="next" value="Submit">
                                   
                                   
                                </fieldset>

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<footer>
		
		<?php echo $footer;  ?>
	</footer>
	<script src="<?php echo base_url('assets/ckeditor/ckeditor.js'); ?>"></script>
</body>
</html>


  <script type="text/javascript" src="<?php echo site_url('js/jquery-ui.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.js'); ?>"></script>

<script type="text/javascript">

            //validation for edit email formate form

            $(document).ready(function () { 

                $("#professional_info").validate({

                    rules: {

                        professional_info: {

                            required: true,
                           
                        },

                    },

                    messages: {

                        professional_info: {

                            required: "Professional Information Is Required.",
                            
                        },
                    },

                });
                   });
  </script>

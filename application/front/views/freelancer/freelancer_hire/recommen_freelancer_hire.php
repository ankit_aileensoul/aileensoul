<!-- start head -->
<?php echo $head; ?>
<!-- END HEAD -->

<!-- start header -->
<?php echo $header; ?>
<!-- END HEADER -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/timeline.css'); ?>">

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/demo.css'); ?>">



<?php echo $freelancer_hire_header2; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <script>
            $(document).ready(function ()
            {


                /* Uploading Profile BackGround Image */
                $('body').on('change', '#bgphotoimg', function ()
                {

                    $("#bgimageform").ajaxForm({target: '#timelineBackground',
                        beforeSubmit: function () {},
                        success: function () {

                            $("#timelineShade").hide();
                            $("#bgimageform").hide();
                        },
                        error: function () {

                        }}).submit();
                });


                /* Banner position drag */
                $("body").on('mouseover', '.headerimage', function ()
                {
                    var y1 = $('#timelineBackground').height();
                    var y2 = $('.headerimage').height();
                    $(this).draggable({
                        scroll: false,
                        axis: "y",
                        drag: function (event, ui) {
                            if (ui.position.top >= 0)
                            {
                                ui.position.top = 0;
                            } else if (ui.position.top <= y1 - y2)
                            {
                                ui.position.top = y1 - y2;
                            }
                        },
                        stop: function (event, ui)
                        {
                        }
                    });
                });


                /* Bannert Position Save*/
                $("body").on('click', '.bgSave', function ()
                {
                    var id = $(this).attr("id");
                    var p = $("#timelineBGload").attr("style");
                    var Y = p.split("top:");
                    var Z = Y[1].split(";");
                    var dataString = 'position=' + Z[0];
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url('freelancer/image_saveBG_ajax'); ?>",
                        data: dataString,
                        cache: false,
                        beforeSend: function () { },
                        success: function (html)
                        {
                            if (html)
                            {
                                window.location.reload();
                                $(".bgImage").fadeOut('slow');
                                $(".bgSave").fadeOut('slow');
                                $("#timelineShade").fadeIn("slow");
                                $("#timelineBGload").removeClass("headerimage");
                                $("#timelineBGload").css({'margin-top': html});
                                return false;
                            }
                        }
                    });
                    return false;
                });



            });
        </script>
    </head>
    <body>

        <!-- cover pic start -->
        <div class="user-midd-section">
            <div class="container">
                <div class="row">


                    <div class="col-md-4"><div class="profile-box profile-box-left">

                            <div class="full-box-module">    



                                <div class="profile-boxProfileCard  module">
                                    <div class="profile-boxProfileCard-cover">  
                                        <a class="profile-boxProfileCard-bg u-bgUserColor a-block"
                                           href="<?php echo base_url('freelancer/freelancer_hire_profile'); ?>"
                                           tabindex="-1"
                                           aria-hidden="true"
                                           rel="noopener">

                                            <!-- box image start -->
                                            <img src="<?php echo base_url(FREEHIREIMG . $freehiredata[0]['profile_background']); ?>" class="bgImage"  style="    height: 95px;
                                                 width: 100%; " >
                                            <!-- box image end -->

                                        </a></div>

                                    <div class="profile-box-menu  fr col-md-12">
                                        <div class="left- col-md-2"></div>
                                        <div  class="right-section col-md-10">
                                            <ul>

                                                <li <?php if (($this->uri->segment(1) == 'freelancer') && ($this->uri->segment(2) == 'freelancer_hire_profile')) { ?> class="active" <?php } ?>><a href="<?php echo base_url('freelancer/freelancer_hire_profile'); ?>"> Profile</a>

                                                </li>

                                                <li ><a href="<?php echo base_url('freelancer/freelancer_hire_post'); ?>"> Posts</a>
                                                </li>


                                                <li <?php if (($this->uri->segment(1) == 'freelancer') && ($this->uri->segment(2) == 'freelancer_save')) { ?> class="active" <?php } ?>><a href="<?php echo base_url('freelancer/freelancer_save'); ?>">Saved</a>
                                                </li>



                                            </ul>
                                        </div>
                                    </div>
                                    <div class="profile-boxProfileCard-content">
                                        <div class="buisness-profile-txext ">

                                            <a class="profile-boxProfileCard-avatarLink a-inlineBlock" href="<?php echo base_url('freelancer/freelancer_hire_profile'); ?>"" title="zalak" tabindex="-1" aria-hidden="true" rel="noopener">
                                                <img src="<?php echo base_url(USERIMAGE . $freehiredata[0]['freelancer_hire_user_image']); ?>"  alt="" >
                                            </a>
                                        </div>
                                        <div class="profile-box-user">
                                            <span class="profile-box-name ">
                                                <a href="<?php echo base_url('freelancer/freelancer_hire_profile'); ?>"> <?php echo ucwords($freehiredata[0]['username']) . ' ' . ucwords($freehiredata[0]['fullname']); ?></a>          </span>

                                        </div>
                                        <div class="profile-box-user">
                                            <span class="profile-box-name"><a href="<?php echo base_url('freelancer/freelancer_hire_profile'); ?>"><?php
if ($freehiredata[0]['designation']) {
    echo $freehiredata[0]['designation'];
} else {
    echo "Designation";
}
?></a></span>
                                        </div>

                                        <div id="profile-box-profile-prompt"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div  class="add-post-button">

                            <a class="btn btn-3 btn-3b" href="<?php echo base_url('freelancer/freelancer_add_post'); ?>"><i class="fa fa-plus" aria-hidden="true"></i>  Add Post</a>
                        </div>

                    </div>





                    <div class="col-md-7 col-sm-7 all-form-content">
                        <div class="common-form">
                            <div class="job-saved-box">
                                <h3>Search Results</h3>
                                <div class="contact-frnd-post">
                                    <div class="job-contact-frnd ">
                                        <!-- body tag inner data start-->

                                        <!-- @nk!t 7-4-2017 start -->
                                        <?php
                                        // foreach ($candidatefreelancer as $cand_key => $cand_value) {
                                        if ($freelancerpostdata) {
                                            ?>
                                            <!-- @nk!t 7-4-2017 end -->
                                            <?php
                                            foreach ($freelancerpostdata as $row) {
                                                ?> 

                                                <div class="profile-job-post-detail clearfix search">
                                                    <div class="profile-job-post-title-inside clearfix">
                                                        <div class="profile-job-profile-button clearfix">
                                                            <div class="profile-job-post-location-name-rec">
                                                                <ul>
                                                                    <ul>
                                                                        <li>

                                                                            <div  class="buisness-profile-pic-candidate"><img src="<?php echo base_url(USERIMAGE . $row['freelancer_post_user_image']); ?>" alt="" >
                                                                            </div></li>
                                                                        <li>
                                                                            <a href="<?php echo base_url('freelancer/freelancer_post_profile/' . $row['user_id']); ?>"><h6>
                                                                                    <?php echo ucwords($row['freelancer_post_fullname']) . ' ' . ucwords($row['freelancer_post_username']); ?></h6>
                                                                            </a></li>
                                                                        <li>
                                                                            <a href="<?php echo base_url('freelancer/freelancer_post_profile/' . $row['user_id']); ?>"><h6>
                                                                                    <?php
                                                                                    $cache_time = $this->db->get_where('category', array('category_id' => $row['freelancer_post_field']))->row()->category_name;
                                                                                    echo $cache_time;
                                                                                    ?></h6>
                                                                            </a></li>

                                                                    </ul>
                                                            </div>
                                                        </div>
                                                    </div>  <div class="profile-job-post-title clearfix">

                                                        <div class="profile-job-profile-menu">

                                                            <ul>

                                                                <li><b>Skills:</b>
                                                                    <?php
                                                                    $comma = ",";
                                                                    $k = 0;
                                                                    $aud = $row['freelancer_post_area'];
                                                                    $aud_res = explode(',', $aud);
                                                                    foreach ($aud_res as $skill) {
                                                                        if ($k != 0) {
                                                                            echo $comma;
                                                                        }
                                                                        $cache_time = $this->db->get_where('skill', array('skill_id' => $skill))->row()->skill;


                                                                        echo $cache_time;
                                                                        $k++;
                                                                    }
                                                                    ?>       

                                                                </li>
                                                                <li><b>Other Skill:</b>
        <?php echo $row['freelancer_post_otherskill']; ?>
                                                                </li>

        <?php $cityname = $this->db->get_where('cities', array('city_id' => $row['freelancer_post_city']))->row()->city_name; ?>

                                                                <li><b>Location:</b> <?php echo $cityname; ?></li>
                                                                <li><b>Skill Description:</b>
        <?php echo $row['freelancer_post_skill_description']; ?>
                                                                </li>
                                                                <li><b>Designation:</b>
                                                                    <?php echo $row['designation']; ?>
                                                                </li>
                                                                <li><b>Avaiability</b>
                                                                    <?php echo $row['freelancer_post_work_hour'] . "  " . "Hours per week "; ?>
                                                                </li>
                                                                <li><b>Rate Hourly:</b>
                                                                    <?php
                                                                    $currency = $this->db->get_where('currency', array('currency_id' => $row['freelancer_post_ratestate']))->row()->currency_name;
                                                                    echo $row['freelancer_post_hourly'] . "   " . $currency;
                                                                    ?>
                                                                </li>
                                                                <li><b>Total Experience:</b>
        <?php echo $row['freelancer_post_exp_year'] . ' ' . $row['freelancer_post_exp_month']; ?>
                                                                </li>


                                                                <input type="hidden" name="search" id="search" value="<?php echo $keyword; ?>">
                                                            </ul>
                                                        </div>

                                                        <div class="profile-job-profile-button clearfix">
                                                            <div class="apply-btn">

        <?php
        $userid = $this->session->userdata('aileenuser');
        $contition_array = array('from_id' => $userid, 'to_id' => $row['user_id']);
        $data = $this->common->select_data_by_condition('save', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');


        if ($data[0]['status'] != 0 || $data[0]['status'] == '') {
            ?> 
                                                                    <a href="<?php echo base_url('freelancer/save_user/' . $row['user_id'] . '/' . $data[0]['save_id']); ?>">Save User</a>
                                                                    <?php
                                                                } else {
                                                                    ?>

                                                                    <a href=" ">Saved User</a> 
                                                                    <?php }
                                                                ?> 

                                                                <a href="<?php echo base_url('message/message_chats/' . $row['user_id']); ?>">Message</a>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                        <?php }
                                        ?>
                                        <!-- @nk!t 7-4-2017 start -->
                                             <?php  }else{
                                            ?>
                                            <div class="text-center rio">
                                                <h1 class="page-heading  product-listing" style="border:0px;margin-bottom: 11px;">Oops No Data Found.</h1>
                                                <p style="margin-left:4%;text-transform:none !important;border:0px;">We couldn't find what you were looking for.</p>
                                                <ul>
                                                    <li style="text-transform:none !important; list-style: none;">Make sure you used the right keywords.</li>
                                                </ul>
                                            </div>
                                            <?php
                                        } ?>
                                        <!-- @nk!t 7-4-2017 end -->
                                            <!-- body tag inner data end -->


                                            <div class="col-md-1">
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        </section>
                        <footer>
    <?php echo $footer; ?>
                        </footer>




                        </body>

                        </html>
                        <script src="<?php echo base_url('js/jquery.wallform.js'); ?>"></script>
                        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                        <script src="<?php echo base_url('js/jquery-ui.min.js'); ?>"></script>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

                        <script src="<?php echo base_url('js/jquery.highlite.js'); ?>"></script>


                        <!-- script for skill textbox automatic end -->

                        <script type="text/javascript">
                var text = document.getElementById("search").value;
                //alert(text);

                $(".search").highlite({

                    text: text

                });

                        </script>

                        <script>
                            $(function () {

                                var complex = <?php echo json_encode($demo); ?>;


                                var availableTags = complex;
                                $("#tags").autocomplete({
                                    source: availableTags
                                });
                            });
                        </script>
                        <script type="text/javascript">
                            function checkvalue() {
                                //alert("hi");
                                var searchkeyword = document.getElementById('tags').value;
                                var searchplace = document.getElementById('searchplace').value;
                                // alert(searchkeyword);
                                // alert(searchplace);
                                if (searchkeyword == "" && searchplace == "") {
                                    //alert('Please enter Keyword');
                                    return false;
                                }
                            }
                        </script>


                        <!-- script for skill textbox automatic end (option 2)-->

                        <script>
                            //select2 autocomplete start for skill
                            $('#searchskills').select2({

                                placeholder: 'Find Your Skills',

                                ajax: {

                                    url: "<?php echo base_url(); ?>freelancer/keyskill",
                                    dataType: 'json',
                                    delay: 250,

                                    processResults: function (data) {

                                        return {
                                            //alert(data);

                                            results: data


                                        };

                                    },
                                    cache: true
                                }
                            });
                            //select2 autocomplete End for skill

                            //select2 autocomplete start for Location
                            $('#searchplace').select2({

                                placeholder: 'Find Your Location',
                                maximumSelectionLength: 1,

                                ajax: {

                                    url: "<?php echo base_url(); ?>freelancer/location",
                                dataType: 'json',
                                delay: 250,

                                processResults: function (data) {

                                    return {
                                        //alert(data);

                                        results: data


                                    };

                                },
                                cache: true
                            }
                        });
                        //select2 autocomplete End for Location

                    </script>

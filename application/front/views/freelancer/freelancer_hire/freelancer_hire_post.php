<!-- start head -->
<?php echo $head; ?>
<script src="<?php echo base_url('js/jquery.min.js'); ?>"></script>

<script src="<?php echo base_url('js/fb_login.js'); ?>"></script>

<!--post save success pop up style strat -->
<style>
    body {
        font-family: Arial, sans-serif;
        background-size: cover;
        height: 100vh;
    }

    .box {
        width: 40%;
        margin: 0 auto;
        background: rgba(255,255,255,0.2);
        padding: 35px;
        border: 2px solid #fff;
        border-radius: 20px/50px;
        background-clip: padding-box;
        text-align: center;
    }



    .overlay {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background: rgba(0, 0, 0, 0.3);
        transition: opacity 500ms;
        visibility: hidden;
        opacity: 0;
        z-index: 10;
    }
    .overlay:target {
        visibility: visible;
        opacity: 1;
    }

    .popup {
        margin: 70px auto;
        padding: 20px;
        background: #fff;
        border-radius: 5px;
        width: 30%;
        height: 200px;
        position: relative;
        transition: all 5s ease-in-out;
    }

    .okk{
        text-align: center;
    }

    .popup .okbtn {
        position: absolute;
        transition: all 200ms;
        font-size: 18px;
        font-weight: bold;
        text-decoration: none;
        color: #fff;
        padding: 8px 18px;
        background-color: darkcyan;
        left: 25px;
        margin-top: 15px;
        width: 100px; 
        border-radius: 8px;
    }

    .popup .cnclbtn {
        position: absolute;
        transition: all 200ms;
        font-size: 18px;
        font-weight: bold;
        text-decoration: none;
        color: #fff;
        padding: 8px 18px;
        background-color: darkcyan;
        right: 25px;
        margin-top: 15px;
        width: 100px;
        border-radius: 8px;
    }

    .popup .pop_content {
        text-align: center;
        margin-top: 40px;

    }

    @media screen and (max-width: 700px){
        .box{
            width: 70%;
        }
        .popup{
            width: 70%;
        }
    }
</style>

<!--post save success pop up style end -->


<style type="text/css" media="screen">
    #row2 { overflow: hidden; width: 100%; }
    #row2 img { height: 350px;width: 100%; }
    .upload-img{    float: right;
                    position: relative;
                    margin-top: -135px;
                    right: 50px; }

    label.cameraButton {
        display: inline-block;
        margin: 1em 0;

        /* Styles to make it look like a button */
        padding: 0.5em;
        border: 2px solid #666;
        border-color: #EEE #CCC #CCC #EEE;
        background-color: #DDD;
        opacity: 0.7;
    }

    /* Look like a clicked/depressed button */
    label.cameraButton:active {
        border-color: #CCC #EEE #EEE #CCC;
    }

    /* This is the part that actually hides the 'Choose file' text box for camera inputs */
    label.cameraButton input[accept*="camera"] {
        display: none;
    }




</style>
<!-- END HEAD -->
<!-- start header -->
<?php echo $header; ?>
<?php echo $freelancer_hire_header2; ?>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-3.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/croppie.css'); ?>">

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- END HEADER -->

<body   class="page-container-bg-solid page-boxed">

    <section>
        <div class="container">
            <div class="row" id="row1" style="display:none;">
                <div class="col-md-12 text-center">
                    <div id="upload-demo" style="width:100%"></div>
                </div>
                <div class="col-md-12 cover-pic" style="padding-top: 25px;text-align: center;">
                    <button class="btn btn-success cancel-result" onclick="" >Cancel</button>

                    <button class="btn btn-success upload-result set-btn" onclick="myFunction()">Upload Image</button>

                    <div id="message1" style="display:none;">
                        <div id="floatBarsG">
                            <div id="floatBarsG_1" class="floatBarsG"></div>
                            <div id="floatBarsG_2" class="floatBarsG"></div>
                            <div id="floatBarsG_3" class="floatBarsG"></div>
                            <div id="floatBarsG_4" class="floatBarsG"></div>
                            <div id="floatBarsG_5" class="floatBarsG"></div>
                            <div id="floatBarsG_6" class="floatBarsG"></div>
                            <div id="floatBarsG_7" class="floatBarsG"></div>
                            <div id="floatBarsG_8" class="floatBarsG"></div>
                        </div>

                    </div>
                </div>
                <div class="col-md-12"  style="visibility: hidden; ">
                    <div id="upload-demo-i" style="background:#e1e1e1;width:100%;padding:30px;height:1px;margin-top:30px"></div>
                </div>
            </div>


            <div class="container">
                <div class="row" id="row2">
                    <?php
                    $userid = $this->session->userdata('aileenuser');
                    $contition_array = array('user_id' => $userid, 'is_delete' => '0', 'status' => '1');
                    $image = $this->common->select_data_by_condition('freelancer_hire_reg', $contition_array, $data = 'profile_background', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');
                    // echo "<pre>";print_r($image);
                    $image_ori = $image[0]['profile_background'];
                    if ($image_ori) {
                        ?>
                        <div class="bg-images">
                            <img src="<?php echo base_url(FREEHIREIMG . $image[0]['profile_background']); ?>" name="image_src" id="image_src" / ></div>
                        <?php
                    } else {
                        ?>
                        <div class="bg-images">
                            <img src="<?php echo WHITEIMAGE; ?>" name="image_src" id="image_src" / ></div>
                    <?php }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>   

<div class="container">    
    <div class="upload-img">


        <label class="cameraButton"><i class="fa fa-camera" aria-hidden="true"></i>
            <input type="file" id="upload" name="upload" accept="image/*;capture=camera" onclick="showDiv()">
        </label>
    </div>







    <div class="profile-photo">
        <div class="profile-pho">

            <div class="user-pic">
                <?php if ($freelancerdata[0]['freelancer_hire_user_image'] != '') { ?>
                    <img src="<?php echo base_url(USERIMAGE . $freelancerdata[0]['freelancer_hire_user_image']); ?>" alt="" >
                <?php } else { ?>
                    <img alt="" class="img-circle" src="<?php echo base_url(NOIMAGE); ?>" alt="" />
                <?php } ?>
                <a href="#popup-form" class="fancybox"><i class="fa fa-camera" aria-hidden="true"></i> Update Profile Picture</a>

            </div>

            <div id="popup-form">
                <?php echo form_open_multipart(base_url('freelancer/user_image_insert'), array('id' => 'userimage', 'name' => 'userimage', 'class' => 'clearfix')); ?>
                <input type="file" name="profilepic" accept="image/gif, image/jpeg, image/png" id="profilepic">
                <input type="hidden" name="hitext" id="hitext" value="2">
                <input type="submit" name="cancel1" id="cancel1" value="Cancel">
                <input type="submit" name="profilepicsubmit" id="profilepicsubmit" value="Save">
                </form>
            </div>

        </div>
        <div class="profile-main-rec-box-menu  col-md-12 ">

            <div class="left-side-menu col-md-2">  </div>
            <div class="right-side-menu col-md-9">  
                <ul class="">                          
                    <li <?php if (($this->uri->segment(1) == 'freelancer') && ($this->uri->segment(2) == 'freelancer_hire_profile')) { ?> class="active" <?php } ?>><a href="<?php echo base_url('freelancer/freelancer_hire_profile'); ?>">Employer Profile</a>
                    </li>





                    <?php if (($this->uri->segment(1) == 'freelancer') && ($this->uri->segment(2) == 'freelancer_hire_post' || $this->uri->segment(2) == 'freelancer_hire_profile' || $this->uri->segment(2) == 'freelancer_add_post' || $this->uri->segment(2) == 'freelancer_save') && ($this->uri->segment(3) == $this->session->userdata('aileenuser') || $this->uri->segment(3) == '')) { ?>



                        <li <?php if (($this->uri->segment(1) == 'freelancer') && ($this->uri->segment(2) == 'freelancer_hire_post')) { ?> class="active" <?php } ?>><a href="<?php echo base_url('freelancer/freelancer_hire_post'); ?>">Post</a>
                        </li>
                        </li>
                        <li <?php if (($this->uri->segment(1) == 'freelancer') && ($this->uri->segment(2) == 'freelancer_save')) { ?> class="active" <?php } ?>><a href="<?php echo base_url('freelancer/freelancer_save'); ?>">Saved Freelancer</a>
                        </li>



                    <?php } ?>


                </ul>
            </div>
        </div>


        <div class="job-menu-profile">
            <h5 > <?php echo ucwords($freehiredata[0]['username']) . ' ' . ucwords($freehiredata[0]['fullname']); ?></h5>


            <div class="profile-text">
                <?php
                if ($freehiredata[0]['designation'] == '') {
                    ?>
                    <a id="myBtn">Designation</a>
                <?php } else { ?> 
                    <a id="myBtn"><?php echo ucwords($freehiredata[0]['designation']); ?></a>
                <?php } ?></div>

            <div  class="add-post-button">

                <a class="btn btn-3 btn-3b"><i class="fa fa-plus" aria-hidden="true"></i>  Add Post</a>
            </div> 
        </div>
        <!-- The Modal -->
        <div id="myModal" class="modal">
            <!-- Modal content --><div class="col-md-2"></div>
            <div class="modal-content col-md-8">
                <span class="close">&times;</span>
                <fieldset></fieldset>
                <?php echo form_open(base_url('freelancer/hire_designation'), array('id' => 'artdesignation', 'name' => 'artdesignation', 'class' => 'clearfix')); ?>

                <fieldset class="col-md-8"> <input type="text" name="designation" id="designation" placeholder="Enter Your Designation" value="<?php echo $freehiredata[0]['designation']; ?>">

                    <?php echo form_error('designation'); ?>
                </fieldset>

                <input type="hidden" name="hitext" id="hitext" value="3">
                <fieldset class="col-md-2"><input type="submit"  id="submitdes" name="submitdes" value="Submit"></fieldset>
                <?php echo form_close(); ?>



            </div>
        </div>

        <div class="col-md-7 col-sm-7 all-form-content">
            <div class="common-form">
                <div class="job-saved-box">
                    <h3>Post</h3>
                    <div class="contact-frnd-post">
                        <div class="job-contact-frnd ">


                            <?php

                            function text2link($text) {
                                $text = preg_replace('/(((f|ht){1}t(p|ps){1}:\/\/)[-a-zA-Z0-9@:%_\+.~#?&\/\/=]+)/i', '<a href="\\1" target="_blank" rel="nofollow">\\1</a>', $text);
                                $text = preg_replace('/([[:space:]()[{}])(www.[-a-zA-Z0-9@:%_\+.~#?&\/\/=]+)/i', '\\1<a href="http://\\2" target="_blank" rel="nofollow">\\2</a>', $text);
                                $text = preg_replace('/([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})/i', '<a href="mailto:\\1" rel="nofollow" target="_blank">\\1</a>', $text);
                                return $text;
                            }
                            ?>                                    
                            <?php
                            
                            if ($freelancerpostdata) {
                                foreach ($freelancerpostdata as $post) {

                                    $userid = $this->session->userdata('aileenuser');
                                    if ($post['user_id'] == $userid) {
                                        ?>
                                        <div class="profile-job-post-detail clearfix" id="<?php echo "removepostdata" . $post['post_id']; ?>">
                                            <div class="profile-job-post-title-inside clearfix">


                                                <!-- pop up box start-->
                                                <div id="<?php echo 'popup3' . $post['post_id']; ?>" class="overlay">
                                                    <div class="popup">

                                                        <div class="pop_content">
                                                            Are You Sure want to delete this post?.

                                                            <p class="okk"><a class="okbtn" id="<?php echo $post['post_id']; ?>" onClick="remove_post(this.id)" href="#">OK</a></p>

                                                            <p class="okk"><a class="cnclbtn" href="#">Cancle</a></p>

                                                        </div>

                                                    </div>
                                                </div>
                                                <!-- pop up box end-->
                                                <div class="profile-job-post-location-name">
                                                    <ul>

                                                        <?php
                                                        $firstname = $this->db->get_where('freelancer_hire_reg', array('user_id' => $post['user_id']))->row()->fullname;
                                                        $lastname = $this->db->get_where('freelancer_hire_reg', array('user_id' => $post['user_id']))->row()->username;
                                                        ?>

                                                        <li><a href="<?php echo base_url('freelancer/freelancer_hire_profile/' . $post['user_id']); ?>">  <?php echo ucwords($firstname); ?>&nbsp;<?php echo ucwords($lastname); ?></a></li>
                                                        <li>

                                                            <?php echo text2link($post['post_name']); ?></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="profile-job-post-title clearfix">
                                                <div class="profile-job-profile-button clearfix">
                                                    <div class="freelancer_work_detail">
                                                        <ul>

                                                            <li>Hourly-<?php
                                                                echo $post['post_rate'];
                                                                echo "&nbsp";
                                                                echo $this->db->get_where('currency', array('currency_id' => $post['post_currency']))->row()->currency_name;
                                                                ?>

                                                            </li>
                                                            <li>Expert-<?php echo $post['post_exp_month']; ?>month&nbsp;&nbsp;<?php echo $post['post_exp_year']; ?>year</a></li>
                                                            <li> Est. Time-<?php echo $post['post_est_time']; ?></li>
                                                            <li> Posted-<?php echo date('d-M-Y', strtotime($post['created_date'])); ?></li>

                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="profile-job-profile-menu">
                                                    <ul class="clearfix">
                                                        <li> <b> Skills:</b> <span>
                                                                <?php
                                                                $comma = ",";
                                                                $k = 0;
                                                                $aud = $post['post_skill'];
                                                                $aud_res = explode(',', $aud);
                                                                foreach ($aud_res as $skill) {
                                                                    if ($k != 0) {
                                                                        echo $comma;
                                                                    }
                                                                    $cache_time = $this->db->get_where('skill', array('skill_id' => $skill))->row()->skill;


                                                                    echo $cache_time;
                                                                    $k++;
                                                                }
                                                                ?>       
                                                            </span>
                                                        </li>


                                                        <?php
                                                        $countryname = $this->db->get_where('countries', array('country_id' => $post['country']))->row()->country_name;
                                                        $cityname = $this->db->get_where('cities', array('city_id' => $post['city']))->row()->city_name;
                                                        ?>
                                                        <li> <b> Country:</b> <span><?php echo $countryname; ?></span>
                                                        </li>
                                                        <li>
                                                            <b> City:</b> <span><?php echo $cityname; ?></span>
                                                        </li>

                                                        <li> <b>  Last Date:</b> <span><?php echo date('d-M-Y', strtotime($post['post_last_date'])); ?></span>
                                                        </li>

                                                        <li> <b>Job Description:</b><span> 
                                                                <?php echo text2link($post['post_description']); ?>  </span>
                                                        </li>

                                                    </ul>
                                                </div>
                                                <div class="profile-job-profile-button clearfix">


                                                    <div class="apply-btn">



                                                        <a href="<?php echo base_url('freelancer/freelancer_apply_list/' . $post['post_id']); ?>">Applied person :<?php echo count($this->common->select_data_by_id('freelancer_apply', 'post_id', $post['post_id'], $data = '*', $join_str = array())); ?></a>

                                                        <a href="<?php echo base_url('freelancer/freelancer_edit_post/' . $post['post_id']); ?>">Edit </a>

                                                        <a href="<?php echo '#popup3' . $post['post_id']; ?>">Remove</a>


                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <?php
                                    }
                                }
                            } else {
                                ?>
                                <div class="text-center rio">
                                    <h4 class="page-heading  product-listing" style="border:0px;margin-bottom: 11px;">No Post Found.</h4>
                                </div>
                            <?php }
                            ?> 

                            <div class="col-md-1">
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>


        <div class="user-midd-section">
            <div class="container">
                <div class="row">


                    <div class="col-md-4">


                    </div>





                </div>
            </div>
        </div>
        </section>
        <footer>
            <?php echo $footer; ?>
        </footer>




        </body>

        </html>

        <script src="<?php echo base_url('assets/js/jquery.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/croppie.js'); ?>"></script>

        <!-- script for skill textbox automatic start (option 2)-->

 <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
        <script src="<?php echo base_url('js/jquery-ui.min.js'); ?>"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script><!-- script for skill textbox automatic end (option 2)-->
        <script>
                                                    $(function () {

                                                        var complex = <?php echo json_encode($demo); ?>;


                                                        var availableTags = complex;
                                                        $("#tags").autocomplete({
                                                            source: availableTags
                                                        });
                                                    });
        </script>

        <script type="text/javascript">
            function checkvalue() {
                //alert("hi");
                var searchkeyword = document.getElementById('tags').value;
                var searchplace = document.getElementById('searchplace').value;
                // alert(searchkeyword);
                // alert(searchplace);
                if (searchkeyword == "" && searchplace == "") {
                    //alert('Please enter Keyword');
                    return false;
                }
            }
        </script>


        <script>
            //select2 autocomplete start for skill
            $('#searchskills').select2({

                placeholder: 'Find Your Skills',

                ajax: {

                    url: "<?php echo base_url(); ?>freelancer/keyskill",
                    dataType: 'json',
                    delay: 250,

                    processResults: function (data) {

                        return {
                            //alert(data);

                            results: data


                        };

                    },
                    cache: true
                }
            });
            //select2 autocomplete End for skill

            //select2 autocomplete start for Location
            $('#searchplace').select2({

                placeholder: 'Find Your Location',
                maximumSelectionLength: 1,

                ajax: {

                    url: "<?php echo base_url(); ?>freelancer/location",
                    dataType: 'json',
                    delay: 250,

                    processResults: function (data) {

                        return {
                            //alert(data);

                            results: data


                        };

                    },
                    cache: true
                }
            });
            //select2 autocomplete End for Location

        </script>

        <!-- cover image start -->
        <script>
            function myFunction() {
                document.getElementById("upload-demo").style.visibility = "hidden";
                document.getElementById("upload-demo-i").style.visibility = "hidden";
                document.getElementById('message1').style.display = "block";

                // setTimeout(function () { location.reload(1); }, 5000);

            }


            function showDiv() {
                document.getElementById('row1').style.display = "block";
                document.getElementById('row2').style.display = "none";
            }
        </script>



        <script type="text/javascript">
            $uploadCrop = $('#upload-demo').croppie({
                enableExif: true,
                viewport: {
                    width: 1250,
                    height: 350,
                    type: 'square'
                },
                boundary: {
                    width: 1250,
                    height: 350
                }
            });



            $('.upload-result').on('click', function (ev) {
                $uploadCrop.croppie('result', {
                    type: 'canvas',
                    size: 'viewport'
                }).then(function (resp) {

                    $.ajax({
                        url: "https://www.aileensoul.com/freelancer/ajaxpro_hire",
                        type: "POST",
                        data: {"image": resp},
                        success: function (data) {
                            html = '<img src="' + resp + '" />';
                            if (html) {
                                window.location.reload();
                            }
                        }
                    });

                });
            });


            $('.cancel-result').on('click', function (ev) {

                document.getElementById('row2').style.display = "block";
                document.getElementById('row1').style.display = "none";
                document.getElementById('message1').style.display = "none";


            });

            //aarati code start
            $('#upload').on('change', function () {
                //alert("hi");


                var reader = new FileReader();
                //alert(reader);
                reader.onload = function (e) {
                    $uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function () {
                        console.log('jQuery bind complete');
                    });

                }
                reader.readAsDataURL(this.files[0]);



            });

            $('#upload').on('change', function () {

                var fd = new FormData();
                fd.append("image", $("#upload")[0].files[0]);

                files = this.files;
                size = files[0].size;

                //alert(size);

                if (size > 4194304)
                {
                    //show an alert to the user
                    alert("Allowed file size exceeded. (Max. 4 MB)")

                    document.getElementById('row1').style.display = "none";
                    document.getElementById('row2').style.display = "block";


                    //reset file upload control
                    return false;
                }


                $.ajax({

                    url: "<?php echo base_url(); ?>freelancer/image_hire",
                    type: "POST",
                    data: fd,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        //alert(response);

                    }
                });
            });

            //aarati code end
        </script>
        <!-- cover image end -->

        <script>
            // Get the modal
            var modal = document.getElementById('myModal');

            // Get the button that opens the modal
            var btn = document.getElementById("myBtn");

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks the button, open the modal 
            btn.onclick = function () {
                modal.style.display = "block";
            }

            // When the user clicks on <span> (x), close the modal
            span.onclick = function () {
                modal.style.display = "none";
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }
        </script>


        <!-- popup form edit END -->


        <!-- remove own post start -->

        <script type="text/javascript">
            function remove_post(abc)
            {

                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url() . "freelancer/remove_post" ?>',
                    data: 'post_id=' + abc,
                    success: function (data) {

                        $('#' + 'removepostdata' + abc).html(data);


                    }
                });

            }
        </script>

        <!-- remove own post end -->
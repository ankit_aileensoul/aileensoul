<!-- start head -->
<?php  echo $head; ?>
    <!-- END HEAD -->
    <!-- start header -->
<?php echo $header; ?>
    <!-- END HEADER -->

    <!--  <rash code 7-4 start> -->
   <?php echo $freelancer_hire_header2; ?>
   <!--  <rash code 7-4 end> -->


   <!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <!--  <rash code 7-4 start> -->

 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
 <!--  <rash code 7-4 end> -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/timeline.css'); ?>">



<script>
$(document).ready(function()
{


/* Uploading Profile BackGround Image */
$('body').on('change','#bgphotoimg', function()
{

$("#bgimageform").ajaxForm({target: '#timelineBackground',
beforeSubmit:function(){},
success:function(){

$("#timelineShade").hide();
$("#bgimageform").hide();
},
error:function(){

} }).submit();
});


/* Banner position drag */
$("body").on('mouseover','.headerimage',function ()
{
var y1 = $('#timelineBackground').height();
var y2 =  $('.headerimage').height();
$(this).draggable({
scroll: false,
axis: "y",
drag: function(event, ui) {
if(ui.position.top >= 0)
{
ui.position.top = 0;
}
else if(ui.position.top <= y1 - y2)
{
ui.position.top = y1 - y2;
}
},
stop: function(event, ui)
{
}
});
});


/* Bannert Position Save*/
$("body").on('click','.bgSave',function ()
{
var id = $(this).attr("id");
var p = $("#timelineBGload").attr("style");
var Y =p.split("top:");
var Z=Y[1].split(";");
var dataString ='position='+Z[0];
$.ajax({
type: "POST",
url: "<?php echo base_url('freelancer/image_saveBG_ajax'); ?>",
data: dataString,
cache: false,
beforeSend: function(){ },
success: function(html)
{
if(html)
{
  window.location.reload();
$(".bgImage").fadeOut('slow');
$(".bgSave").fadeOut('slow');
$("#timelineShade").fadeIn("slow");
$("#timelineBGload").removeClass("headerimage");
$("#timelineBGload").css({'margin-top':html});
return false;
}
}
});
return false;
});



});
</script>
</head>
<body>

<!-- cover pic start -->
<div class="container" id="container">

<div id="timelineContainer">

<!-- timeline background -->
<div id="timelineBackground">
<img src="<?php echo base_url(USERIMAGE . $freehiredata[0]['profile_background']);?>" class="bgImage" style="margin-top: <?php echo $freehiredata[0]['profile_background_position']; ?>;">

</div>

<!-- timeline background -->
<div  id="timelineShade" style="background:url(<?php echo base_url('images/timeline_shade.png'); ?>">
<form id="bgimageform" method="post" enctype="multipart/form-data" action="<?php echo base_url('freelancer/image_upload_ajax'); ?>">




<div class="uploadFile timelineUploadBG" style="background:url(<?php echo base_url('images/whitecam.png'); ?>">

<input type="file" name="photoimg" id="bgphotoimg" class=" custom-file-input" original-title="Change Cover Picture"  ">



</div>
</form>
</div>


<!-- timeline nav -->
<div id="timelineNav"></div>

</div>

</div>
<!-- cover pic end -->
        <div>
            <div class="container">
               
                <div class="profile-photo">
                    <div class="profile-pho">

                        <div class="user-pic">
                        <?php if($freelancerdata[0]['freelancer_hire_user_image'] != ''){ ?>
                           <img src="<?php echo base_url(USERIMAGE . $freelancerdata[0]['freelancer_hire_user_image']);?>" alt="" >
                            <?php } else { ?>
                            <img alt="" class="img-circle" src="<?php echo base_url(NOIMAGE); ?>" alt="" />
                            <?php } ?>
                            <a href="#popup-form" class="fancybox"><i class="fa fa-camera" aria-hidden="true"></i> Update Profile Picture</a>

                        </div>
                        
                        <div id="popup-form">
                        <?php echo form_open_multipart(base_url('freelancer/user_image_insert'), array('id' => 'userimage','name' => 'userimage', 'class' => 'clearfix')); ?>
                        <input type="file" name="profilepic" accept="image/gif, image/jpeg, image/png" id="profilepic">
                        <input type="hidden" name="hitext" id="hitext" value="1">
                        <input type="submit" name="cancel1" id="cancel1" value="Cancel">
                        <input type="submit" name="profilepicsubmit" id="profilepicsubmit" value="Save">
                    </form>
                </div>

                    </div>

                    <div class="profile-main-rec-box-menu  col-md-12 ">

<div class="left-side-menu col-md-2">  </div>
<div class="right-side-menu col-md-9">  
 <ul class="">
                                  
                                     <li <?php if(($this->uri->segment(1) == 'freelancer') && ($this->uri->segment(2) == 'freelancer_hire_profile')){?> class="active" <?php } ?>><a href="<?php echo base_url('freelancer/freelancer_hire_profile'); ?>">Employer Profile</a>
                                    </li>

                                   
                                    


                                    <?php 

                                   
                                if(($this->uri->segment(1) == 'freelancer') && ($this->uri->segment(2) == 'freelancer_hire_post' || $this->uri->segment(2) == 'freelancer_hire_profile' || $this->uri->segment(2) == 'freelancer_add_post' || $this->uri->segment(2) == 'freelancer_save') && ($this->uri->segment(3) == $this->session->userdata('aileenuser')|| $this->uri->segment(3) == '')) { ?>

                                    <li <?php if(($this->uri->segment(1) == 'freelancer') && ($this->uri->segment(2) == 'freelancer_save')){?> class="active" <?php } ?>><a href="<?php echo base_url('freelancer/freelancer_hire_post'); ?>">Post</a>
                                    </li>
                                   

                                    <li <?php if(($this->uri->segment(1) == 'freelancer') && ($this->uri->segment(2) == 'freelancer_save')){?> class="active" <?php } ?>><a href="<?php echo base_url('freelancer/freelancer_save'); ?>">Saved Freelancer</a>
                                    </li>

                                    
                                    <?php }?>
                                   

    </ul>
    </div>

</div>

                    
                      <div class="job-menu-profile">
                           <h5 > <?php echo ucwords($userdata[0]['first_name']) . ' '.ucwords($userdata[0]['last_name']); ?></h5>
                           
                      </div>
                      <div class="col-md-7 col-sm-7">

                    <div>
                        <?php
                                        if ($this->session->flashdata('error')) {
                                            echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
                                        }
                                        if ($this->session->flashdata('success')) {
                                            echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
                                        }?>
                    </div>

                        <div class="common-form">
                            <div class="job-saved-box">
                                <h3>Post Your Project</h3> 
                                
                                
                           <?php echo form_open(base_url('freelancer/freelancer_add_post_insert'), array('id' => 'postinfo','name' => 'postinfo','class' => 'clearfix')); ?>
                            <div>
                                  <h4> Project Description</h4></div>
                        <div><span style="color:red">Fields marked with asterisk (*) are mandatory</span></div> 
                            


                            <?php
                         $post_name =  form_error('post_name');
                        
                         $skills =  form_error('skills');
                         
                         $post_desc =  form_error('post_desc');
                        
                         ?>


                        <fieldset class="full-width" <?php if($post_name) {  ?> class="error-msg" <?php } ?>>
                        <label >Post name:<span style="color:red">*</span></label>                 
                        <input name="post_name" type="text" id="post_name" placeholder="Enter Post Name"/>
                        <span id="fullname-error"></span>
                        <?php echo form_error('post_name'); ?>
                        </fieldset>

                         <fieldset class="full-width">
                        <label>Post description :<span style="color:red">*</span></label>

                        <textarea name="post_desc" id="post_desc" placeholder="Enter Description"></textarea>
                      
                        <?php echo form_error('post_desc'); ?>
                      </fieldset>

                       <fieldset class="full-width" <?php if($fields_req) {  ?> class="error-msg" <?php } ?>>
                  <label>Fields Of Requirmeant:<span style="color:red">*</span></label>
                   <select name="fields_req" id="fields_req">
                  <option value="">Select Fields of Requirement</option>
                  <?php
                                            if(count($category) > 0){
                                                foreach($category as $cnt){
                                          
                                            if($fields_req1)
                                            {
                                              ?>
                                                 <option value="<?php echo $cnt['category_id']; ?>" <?php if($cnt['category_id']==$fields_req1) echo 'selected';?>><?php echo $cnt['category_name'];?></option>
                                               
                                                <?php
                                                }
                                                else
                                                {
                                            ?>
                                              
                                                  <option value="<?php echo $cnt['category_id']; ?>"><?php echo $cnt['category_name'];?></option> 
                                                  <?php
                                            
                                            }
       
                                            }}
                                            ?>
              </select>
              <?php echo form_error('fields_req'); ?>
                  </fieldset>

                  <fieldset  <?php if($skills) {  ?> class="error-msg" <?php } ?>>
                        <label>Skills of requirements:<span style="color:red">*</span></label>
                         <select class="keyskil" name="skills[]" id="skills" multiple="multiple"></select>
                        <span id="fullname-error"></span>
                        <?php echo form_error('skills'); ?>
                       </fieldset>

                        <fieldset <?php if($other_skill) {  ?> class="error-msg" <?php } ?> >
                            <label class="control-label">Other Skill:<!-- <span style="color:red">*</span> --></label>
                            <input name="other_skill" class="keyskil"  type="text" id="other_skill" placeholder="Enter Your Other Skill" />
                                <span id="fullname-error"></span>
                                <?php echo form_error('other_skill'); ?>
                        </fieldset>


                    <fieldset class="full-width" <?php if($est_time) {  ?> class="error-msg" <?php } ?>>
                        <label>Estimated time of project:</label>
                        <input name="est_time" type="text" id="est_time" placeholder="Enter Estimated time in month/year" /><span id="fullname-error"></span>
                        <?php echo form_error('est_time'); ?>
                         </fieldset>

                       
                        <fieldset class="col-md-12">  
                        <b><h2>Payment : </h2></b>
                         </fieldset>
                         
                          <fieldset class="col-md-4" <?php if($rate) {  ?> class="error-msg" <?php } ?> >
                            <label class="control-label">Rate:<span style="color:red">*</span></label>
                            <input name="rate" type="number" id="rate" placeholder="Enter Your rate" />
                                <span id="fullname-error"></span>
                                <?php echo form_error('rate'); ?>
                        </fieldset>


                          <fieldset class="col-md-4" <?php if($csurrency) {  ?> class="error-msg" <?php } ?> class="two-select-box"> 
                     <label>Currency:</label>
                            <select name="currency" id="currency">

                            <?php foreach($currency as $cur){ ?>
                             <option value="<?php echo $cur['currency_id']; ?>"><?php echo $cur['currency_name']; ?></option>
                             <?php } ?>
                             </select>

          
                             <?php echo form_error('currency'); ?>
</fieldset>

<fieldset class="col-md-4">

<label> Work Type</label>  <input type="radio" name="rating" value="0" checked> Hourly
  <input type="radio" name="rating" value="1"> Fixed
  <?php echo form_error('rating'); ?>
                               </fieldset>





                         <fieldset <?php if($month) {  ?> class="error-msg" <?php } ?> class="two-select-box"> 
                     <label>Experience:</label>
                            <select name="month" id="month">
                            <option value="">Month</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                               </select>
                                <?php echo form_error('month'); ?>
                            <select name="year">
                            <option value="">Year</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            </select>
                            <span id="fullname-error"></span>
                            <?php echo form_error('year'); ?>
                    </fieldset>

                   
                    <fieldset <?php if($last_date) {  ?> class="error-msg" <?php } ?>>
                        <label>Last date for apply:</label>
                        <input type="date" name="last_date" placeholder="Enter date" id="last_date" value="">
                        <?php echo form_error('last_date'); ?> 
                    </fieldset>

                    <!-- <fieldset class="full-width" <?php if($location) {  ?> class="error-msg" <?php } ?>>
                        <label>Location:</label>
                        <input name="location" type="text" id="location" placeholder="Enter Location" /><span id="fullname-error"></span>
                         <?php echo form_error('location'); ?>
                    </fieldset> -->


                    <fieldset <?php if($country) {  ?> class="error-msg" <?php } ?>>
                <label>Country:<span style="color:red">*</span></label>
                
                        <select name="country" id="country">
                          <option value="">Select Country</option>
                          <?php
                                            if(count($countries) > 0){
                                                foreach($countries as $cnt){
                                          
                                            if($country1)
                                            {
                                              ?>
                                                 <option value="<?php echo $cnt['country_id']; ?>" <?php if($cnt['country_id']==$country1) echo 'selected';?>><?php echo $cnt['country_name'];?></option>
                                               
                                                <?php
                                                }
                                                else
                                                {
                                            ?>
                                                 <option value="<?php echo $cnt['country_id']; ?>"><?php echo $cnt['country_name'];?></option>
                                                  <?php
                                            
                                            }
       
                                            }}
                                            ?>
                      </select><span id="country-error"></span>
                   <?php echo form_error('country'); ?>
                  </fieldset>

                  <fieldset>
                    <label> City:</label>
                  <select name="city" id="city">
                    <?php

                                         if($city1)

                                            {
                                          foreach($cities as $cnt){
                                               
                                              ?>

                                               <option value="<?php echo $cnt['city_id']; ?>" <?php if($cnt['city_id']==$city1) echo 'selected';?>><?php echo $cnt['city_name'];?></option>

                                                <?php
                                                } }
                                              
                                                else
                                                {
                                            ?>
                                        <option value="">Select Country first</option>

                                         <?php
                                            
                                            }
                                            ?>
                  </select><span id="city-error"></span>
                                    <?php echo form_error('city'); ?>
                </fieldset>

             
                 <div class="fr">           

                    <fieldset class="hs-submit full-width">

                        <input type="reset">
                        <input type="submit" id="submit" name="submit" value="save">
                    
                    </fieldset>
                      </div>      
                      </form>
                                          
                                        </div>
                                        <div class="col-md-1">
                                        </div>
                                    </div>
                    
                      </div>
                </div>
            </div>
        </div>
        </div>
         
        <div class="user-midd-section">
            <div class="container">
                <div class="row">
                
                                </div>


                          
                        </div>
                    </div>
    </section>
    <footer>

        <footer>
            <?php echo $footer; ?>
        </footer>

       
</body>

</html>

<!-- Field Validation Js Start -->
<script type="text/javascript" src="<?php echo base_url('js/jquery-1.11.1.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate1.15.0..min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/additional-methods1.15.0.min.js'); ?>"></script>
<!-- Field Validation Js End -->

<!-- javascript validation start -->
   <script type="text/javascript">

           

            $(document).ready(function () { 

                $("#postinfo").validate({

                  ignore: '*:not([name])',

                    rules: {

                        post_name: {

                            required: true,
                           
                        },

                         'skills[]': {
                            
                          require_from_group: [1, ".keyskil"] 
                          //required:true 
                        }, 

                        other_skill: {
                            
                           require_from_group: [1, ".keyskil"]
                            // required:true 
                        },
                       
                      
                       post_desc: {

                            required: true,
                           
                        },
                      
                    },

                    messages: {

                        post_name: {

                            required: "Post name Is Required.",
                            
                        },

                       'skills[]': {

                            require_from_group: "You must either fill out 'Keyskills' or 'Other Skills'"

                        },

                        other_skill: {

                            require_from_group: "You must either fill out 'Keyskills' or 'Other Skills'"
                        },
                        
                        post_desc: {

                            required: "Post Description  Is Required.",
                            
                        },
                       

                    },

                });
                   });
</script>
<!-- javascript validation End -->
<!-- 
<rash code 7-4 start> -->

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
 <script src="<?php echo base_url('js/jquery-ui.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/croppie.js'); ?>"></script>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


 <script>
  $( function() {

    var complex = <?php echo json_encode($demo); ?>;
    

    var availableTags = complex; 
    $( "#tags" ).autocomplete({ 
      source: availableTags
    });
  } );
  </script>

  <script type="text/javascript">
function checkvalue(){
   //alert("hi");
  var searchkeyword=document.getElementById('tags').value;
  var searchplace=document.getElementById('searchplace').value;
  // alert(searchkeyword);
  // alert(searchplace);
  if(searchkeyword == "" && searchplace == ""){
     alert('Please enter Keyword');
    return false;
  }
}
</script>
 
 <!-- <rash code 7-4 end>  -->

<!-- country city dependent -->

<script type="text/javascript">
$(document).ready(function(){
    $('#country').on('change',function(){ 
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url() . "freelancer/ajax_dataforcity"; ?>',
                data:'country_id='+countryID,
                success:function(html){
                    $('#city').html(html); 
                }
            }); 
        }else{
            $('#city').html('<option value="">Select Country first</option>'); 
        }
    });
    
});
</script>


<script>
//select2 autocomplete start for skill
$('#searchskills').select2({
        
        placeholder: 'Find Your Skills',
       
        ajax:{

         
          url: "<?php echo base_url(); ?>freelancer/keyskill",
          dataType: 'json',
          delay: 250,
          
          processResults: function (data) {
            
            return {
              //alert(data);

              results: data


            };
            
          },
           cache: true
        }
      });
//select2 autocomplete End for skill

//select2 autocomplete start for Location
$('#searchplace').select2({
        
        placeholder: 'Find Your Location',
        maximumSelectionLength: 1,
       
        ajax:{

         
          url: "<?php echo base_url(); ?>freelancer/location",
          dataType: 'json',
          delay: 250,
          
          processResults: function (data) {
            
            return {
              //alert(data);

              results: data


            };
            
          },
           cache: true
        }
      });
//select2 autocomplete End for Location

//select2 autocomplete start for skill
$('#skills').select2({
        
        placeholder: 'Find Your Skills',
       
        ajax:{

         
          url: "<?php echo base_url(); ?>freelancer/keyskill",
          dataType: 'json',
          delay: 250,
          
          processResults: function (data) {
            
            return {
              //alert(data);

              results: data


            };
            
          },
           cache: true
        }
      });
//select2 autocomplete End for skill

</script>


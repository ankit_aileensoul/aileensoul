<!-- start head -->
<?php  echo $head; ?>
    <!-- END HEAD -->
    <!-- start header -->
     
<?php echo $header; ?>
    <!-- END HEADER -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/3.3.0/select2.css'); ?>">
     <link href="<?php echo base_url('css/jquery-ui.css') ?>" rel="stylesheet" type="text/css" />
    <body class="page-container-bg-solid page-boxed">

      <section>
        
        <div class="user-midd-section">
            <div class="container">
                <div class="row">
                    <?php echo $freelancer_hire_left; ?>
                    <div class="col-md-7 col-sm-8">


                    <div>
                        <?php
                                        if ($this->session->flashdata('error')) {
                                            echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
                                        }
                                        if ($this->session->flashdata('success')) {
                                            echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
                                        }?>
                    </div>

                    <div class="common-form">
                    <h3>Edit Post</h3>

                   
                    
                 <?php echo form_open(base_url('freelancer/freelancer_edit_post_insert/'.$freelancerpostdata[0]['post_id']), array('id' => 'postinfo','name' => 'postinfo','class' => 'clearfix')); ?>

                    <div><span style="color:red">Fields marked with asterisk (*) are mandatory</span></div> 

                            <fieldset class="full-width">
                                <label>Post name:<span style="color:red">*</span></label>
                                <input name="post_name" type="text" id="post_name" placeholder="Enter Post Name" value="<?php echo $freelancerpostdata[0]['post_name']?> "/>
                                <span id="fullname-error"></span>                        
                                <?php echo form_error('post_name'); ?>
                            </fieldset>

                            <fieldset class="full-width">
                                <label>Post description:<span style="color:red">*</span></label>

                                <textarea name="post_desc" id="post_desc" placeholder="Enter Description"><?php echo $freelancerpostdata[0]['post_description']; ?></textarea>

                                
                                <?php echo form_error('post_desc'); ?>
                           </fieldset>


                           <fieldset class="full-width" <?php if($fields_req) {  ?> class="error-msg" <?php } ?>>
                  <label>Fields Of Requirmeant:<span style="color:red">*</span></label>
                   <select name="fields_req" id="fields_req">
                 <?php  
                                            if(count($category) > 0){ 
                                                           foreach($category as $cnt){  
                                          
                                            if($freelancerpostdata[0]['post_field_req'])
                                            {  
                                              ?>
                                                 <option value="<?php echo $cnt['category_id']; ?>" <?php if($cnt['category_id'] == $freelancerpostdata[0]['post_field_req']) echo 'selected';?>><?php echo $cnt['category_name'];?></option>
                                               
                                                <?php
                                                }
                                                else
                                                {
                                            ?>
                                              
                                                  <option value="<?php echo $cnt['category_id']; ?>"><?php echo $cnt['category_name'];?></option> 
                                                  <?php
                                            
                                            }
       
                                            }}
                                            ?>
                            </select>
                            <?php echo form_error('fields_req'); ?>
                            </fieldset>

                            <fieldset class="full-width" <?php if($post_skill) {  ?> class="error-msg" <?php } ?>>
                                <label>Skills:<span style="color:red">*</span></label>
                               
                                  <select name="skills[]" id ="skill1" multiple="multiple" style="width:270px " class="skill1">

                                 <?php foreach ($skill1 as $skill) { ?>
                                <option value="<?php echo $skill['skill_id']; ?>"><?php echo $skill['skill']; ?></option>
                                  <?php } ?>

                                </select>
                                <?php echo form_error('skills'); ?>
                                </select>
                            </fieldset>

                            <fieldset <?php if($other_skill) {  ?> class="error-msg" <?php } ?> >
                            <label class="control-label">Other Skill:<span style="color:red">*</span></label>
                            <input name="other_skill" type="text" id="other_skill" placeholder="Enter Your Other Skill" value="<?php echo $freelancerpostdata[0]['post_other_skill']; ?>" />
                                <span id="fullname-error"></span>
                                <?php echo form_error('other_skill'); ?>
                        </fieldset>


                            <fieldset>
                                <label>Estimated time of project:</label>
                                <input name="est_time" type="text" id="est_time" placeholder="Enter Estimated time in month/year" value="<?php echo $freelancerpostdata[0]['post_est_time']?> "/>
                                <span id="fullname-error"></span>
                                <?php echo form_error('post_name'); ?>
                            </fieldset>

                            <fieldset class="col-md-12">  
                             <b><h2>Payment : </h2></b>
                            </fieldset>

                            <fieldset  class="col-md-4" <?php if($rate) {  ?> class="error-msg" <?php } ?> >
                            <label class="control-label">Rate:<span style="color:red">*</span></label>
                            <input name="rate" type="number" id="rate" placeholder="Enter Your rate" value="<?php echo $freelancerpostdata[0]['post_rate']; ?>" />
                                <span id="fullname-error"></span>
                                <?php echo form_error('rate'); ?>
                            </fieldset>

                            <fieldset class=" col-md-4"> 
                     <label>Currency:</label>
                            <select name="currency" id="currency">
                            <?php if(count($currency) > 0){
                            foreach($currency as $cur){ 

                                if($freelancerpostdata[0]['post_currency'])
                                            {?>

                                         <option value="<?php echo $cur['currency_id']; ?>" <?php if($cur['currency_id'] == $freelancerpostdata[0]['post_currency']) echo 'selected';?>><?php echo $cur['currency_name'];?></option>
                                            <?php }else{
                                ?>
                             <option value="<?php echo $cur['currency_id']; ?>"><?php echo $cur['currency_name']; ?></option>
                             <?php  } } }?>
                             </select>

          
                             <?php echo form_error('currency'); ?>

                    </fieldset>
                    <fieldset class="col-md-4">
                    <label>Work Type</label>
  <input type="radio" name="rating" value="0" checked> Hourly
  <input type="radio" name="rating" value="1"> Fixed
  <?php echo form_error('rating'); ?>
                               </fieldset>

                            <fieldset class="two-select-box"> 
                                <label>Experience:</label>
                                <select name="month" id="month">
                                    <option value="<?php echo $freelancerpostdata[0]['post_exp_month']?> "><?php echo $freelancerpostdata[0]['post_exp_month']?></option>
                                    <option value="#">Month</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                               </select>
                                <?php echo form_error('month'); ?>
                                <select name="year" id="year">
                                    <option value="<?php echo $freelancerpostdata[0]['post_exp_year']?>"><?php echo $freelancerpostdata[0]['post_exp_year']?></option>
                                    <option value="#">Year</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                </select>
                                <span id="fullname-error"></span>
                                <?php echo form_error('year'); ?>
                            </fieldset>
                            
                           

                        <fieldset <?php if($last_date) {  ?> class="error-msg" <?php } ?>>
                        <label>Last date for apply:</label>
                        <input type="date" name="last_date" placeholder="Enter date" id="last_date1" value="<?php echo $freelancerpostdata[0]['post_last_date']?>">
                        <?php echo form_error('last_date'); ?> 
                        </fieldset>

                           <!--  <fieldset>
                                <label class="full-width">Location:</label>
                                <input name="location" type="text" id="location" onblur="return full_name();" value="<?php echo $freelancerpostdata[0]['post_location']?>" /><span id="fullname-error"></span>
                                <?php  ?>
                            </fieldset> -->


                            <fieldset <?php if($country) {  ?> class="error-msg" <?php } ?>>
                <label>Country:<span style="color:red">*</span></label>
                
                        <select name="country" id="country">
                          <option value="">Select Country</option>
                          <?php
                                            if(count($countries) > 0){
                                                foreach($countries as $cnt){
                                          
                                            if($country1)
                                            {
                                              ?>
                                                 <option value="<?php echo $cnt['country_id']; ?>" <?php if($cnt['country_id']==$country1) echo 'selected';?>><?php echo $cnt['country_name'];?></option>
                                               
                                                <?php
                                                }
                                                else
                                                {
                                            ?>
                                                 <option value="<?php echo $cnt['country_id']; ?>"><?php echo $cnt['country_name'];?></option>
                                                  <?php
                                            
                                            }
       
                                            }}
                                            ?>
                      </select><span id="country-error"></span>
                   <?php echo form_error('country'); ?>
                  </fieldset>

                  <fieldset>
                    <label> City:</label>
                  <select name="city" id="city">
                    <?php

                                         if($city1)

                                            {
                                          foreach($cities as $cnt){
                                               
                                              ?>

                                               <option value="<?php echo $cnt['city_id']; ?>" <?php if($cnt['city_id']==$city1) echo 'selected';?>><?php echo $cnt['city_name'];?></option>

                                                <?php
                                                } }
                                              
                                                else
                                                {
                                            ?>
                                        <option value="">Select Country first</option>

                                         <?php
                                            
                                            }
                                            ?>
                  </select><span id="city-error"></span>
                                    <?php echo form_error('city'); ?>
                </fieldset>

                            <fieldset class="hs-submit full-width">

                                <input type="reset">
                                <input type="submit" id="submit" name="submit" value="save">
                                
                            </fieldset>
                            </div>

                            </form>
                            </div>
                    
                   
                </div>
            </div>
        </div>
    </section>

    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <!-- BEGIN INNER FOOTER -->
    <?php echo $footer; ?>
    <!-- end footer -->
 


         <script type="text/javascript" src="<?php echo site_url('js/jquery-1.11.1.min.js') ?>"></script>
         <script type="text/javascript" src="<?php echo site_url('js/jquery-ui.js') ?>"></script>
       <!-- Field Validation Js Start -->
<script type="text/javascript" src="<?php echo base_url('js/jquery.min.js') ?>"></script> 

<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.js'); ?>"></script>
<!-- Field Validation Js End -->

<!-- javascript validation start -->
   <script type="text/javascript">

           

            $(document).ready(function () { 

                $("#postinfo").validate({

                    rules: {

                        post_name: {

                            required: true,
                           
                        },

                         skills: {

                            required: true,
                           
                        },
                       
                       hourly: {

                            required: true,
                            
                        },
                       
                       est_time: {

                            required: true,
                           
                           
                        },
                        year: {

                            required: true,
                           
                        },
                       month: {

                            required: true,
                           
                        },
                        position: {

                            required: true,
                           
                        },
                       post_desc: {

                            required: true,
                           
                        },
                        interview: {

                            required: true,
                           
                        },
                       last_date1: {

                            required: true,
                           
                        }, 
                        country: {

                            required: true,
                           
                        },
                        city: {

                            required: true,
                           
                        },  
                    },

                    messages: {

                        post_name: {

                            required: "Post name Is Required.",
                            
                        },

                        skills: {

                            required: "Skill Is Required.",
                            
                        },

                         hourly: {

                            required: "Hour Selection Is Required.",
                            
                        },

                        est_time: {

                            required: "Estimated Time Is Required.",
                            
                        },
                         year: {

                            required: "Year Selection Is Required.",
                            
                        },
                        month: {

                            required: "Month Selection Is Required.",
                            
                        },
                        position: {

                            required: "Position Selection Is Required.",
                            
                        },
                        post_desc: {

                            required: "Post Description  Is Required.",
                            
                        },
                        interview: {

                            required: "Interview Selection Is Required.",
                            
                        },
                       last_date1: {

                            required: "Last Date Selection Is Required.",
                            
                        },
                        country: {

                            required: " Country Is Required.",
                            
                        },
                        city: {

                            required: " City Is Required.",
                            
                        },

                    },

                });
                   });
</script>


<!-- country city dependent -->

<script type="text/javascript">
$(document).ready(function(){
    $('#country').on('change',function(){ 
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url() . "freelancer/ajax_dataforcity"; ?>',
                data:'country_id='+countryID,
                success:function(html){
                    $('#city').html(html); 
                }
            }); 
        }else{
            $('#city').html('<option value="">Select Country first</option>'); 
        }
    });
    
});
</script>
<!-- script for skill textbox automatic start (option 2)-->
<script type="text/javascript" src="<?php echo base_url('js/3.3.0/select2.js'); ?>"></script>
<!-- script for skill textbox automatic end (option 2)-->

<script>

//select2 autocomplete start for skill
$('#skills').select2({
        
        placeholder: 'Find Your Skills',
       
        ajax:{

         
          url: "<?php echo base_url(); ?>freelancer/keyskill",
          dataType: 'json',
          delay: 250,
          
          processResults: function (data) {
            
            return {
              //alert(data);

              results: data


            };
            
          },
           cache: true
        }
      });
//select2 autocomplete End for skill

</script>
 <script>

var complex = <?php echo json_encode($selectdata); ?>;

$("#skill1").select2().select2('val',complex)

</script>
<!--start head -->
<?php  echo $head; ?>
    <!-- END HEAD -->
    <!-- start header -->
<?php echo $header; ?>
    <!-- END HEADER -->



	<section>
		<div class="user-midd-section">
			<div class="container">
				<div class="row">
					 <div class="col-md-2"></div>
          <div class="col-md-8">

     
						<div class="common-form">
							<h3>Edit Profile</h3>
  

 <?php echo form_open_multipart(base_url('profile/edit_profile'), array('id' => 'basicinfo','name' => 'basicinfo','class' => "clearfix")); ?>
								    <fieldset class="">
									    <label>First Name </label>
                      <input name="first_name" type="text" id="first_name" value="<?php echo $userdata[0]['first_name']?>" onblur="return full_name();"/><span id="fullname-error"></span>   <?php echo form_error('first_name'); ?>

								    </fieldset>
								    <fieldset class="">
                      <label>Last Name</label>
                     							<input name="last_name" type="text" id="last_name" value="<?php echo $userdata[0]['last_name']?>" onblur="return full_name();"/><span id="fullname-error"></span>
 <?php echo form_error('last_name'); ?>

                    </fieldset>
	   <fieldset>           
					
						<label >E-mail Address:</label>
							<input name="email"  type="text" id="email" class="form-control"  value="<?php echo $userdata[0]['user_email']?>"   onblur="return email_id();"/><span id="email-error"></span>	<?php echo form_error('email'); ?>

					</fieldset>
			<fieldset>				

						<label>Birthday:</label>
					<input name="dob"  type="date" id="date" class="form-control"  value="<?php echo date('Y-m-d', strtotime($userdata[0]['user_dob']))?>"   onblur="return email_id();"/><span id="email-error"></span>
					
					<?php echo form_error('email'); ?>
					
					</fieldset>

                    <fieldset>
                      <label>Gender</label>
                      <input type="radio" name="gender" value="M" <?php if($userdata[0]['user_gender'] == M){ echo 'checked'; } ?>>Male
                      <input type="radio" name="gender" value="F" <?php if($userdata[0]['user_gender'] == F){ echo 'checked'; } ?>>Female
                    <input type="radio" name="gender" value="O" <?php if($userdata[0]['user_gender'] == O){ echo 'checked'; } ?>> Other
  
                     
					<?php echo form_error('gender'); ?>
					</fieldset>
					<!-- <fieldset>
						<label >Image:</label>
							 <?php if($userdata[0]['user_image'] != ''){ ?>
                            <img alt="" class="" src="<?php echo base_url(USERIMAGE . $userdata[0]['user_image']);?>" height="100" width="100" alt="Smiley face" />
                        <?php } else { ?>
                            <img alt=""  src="<?php echo base_url(NOIMAGE); ?>" height="100" width="200" alt="Smiley face" />
                        <?php } ?>
                        <input type="file" name="profileimg" id="profileimg" value="">
					<?php echo form_error('profileimg'); ?>
					
					</fieldset> -->

					 
						
                    <fieldset class="hs-submit full-width">


                        <input type="reset" value="Reset" name="cancel">
                      <input type="submit" value="submit" name="submit" id="submit">
                      								
                    </fieldset>
				</div>
				</div>
				</div>
				</div>
				</div>
			

<footer>
        <!-- <div class="footer text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footer-logo">
                            <a href="index.html"><img src="images/logo-white.png"></a>
                        </div>
                        <ul>
                            <li>E-912 Titanium City Center Anandngar Ahmedabad-380015</li>
                            <li><a href="mailto:AileenSoul@gmail.com">AileenSoul@gmail.com</a></li>
                            <li>+91 903-353-8102</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <p><i class="fa fa-copyright" aria-hidden="true"></i> 2017 All Rights Reserved </p>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div> -->
        <?php echo $footer;  ?>
    </footer>
    
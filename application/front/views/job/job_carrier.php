
<!-- start head -->
<?php  echo $head; ?>
<!-- END HEAD -->

<!-- start header -->
<?php echo $header; ?>
<!-- END HEADER -->

    <body class="page-container-bg-solid page-boxed">

      <section>
      
        <div class="user-midd-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <div class="left-side-bar">
                            <ul>
                                <li><a href="<?php echo base_url('job/job_basicinfo_update'); ?>">Basic Information</a></li>
                                <li><a href="<?php echo base_url('job/job_address_update'); ?>">Address</a></li>
                                  <li><a href="<?php echo base_url('job/job_education_update'); ?>">Educational Qualification</a></li>

                                  <li><a href="<?php echo base_url('job/job_project_update'); ?>">Project And Training / Internship</a></li>

                                <li><a href="<?php echo base_url('job/job_skill_update'); ?>">Professional Skills</a></li>
                                <li><a href="<?php echo base_url('job/job_apply_for_update'); ?>">Apply For</a></li>
                              
                                <li><a href="<?php echo base_url('job/job_work_exp_update'); ?>">Work Experience</a></li>
                                <li><a href="<?php echo base_url('job/job_curricular_update'); ?>">Extra Curricular Activities</a></li>
                                <li><a href="<?php echo base_url('job/job_reference_update'); ?>">Interest & Reference</a></li>
                                <li <?php if($this->uri->segment(1) == 'job'){?> class="active" <?php } ?>><a href="#">Carrier Objectives</a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- middle section start -->
                    <div class="col-md-6 col-sm-8">

                    <div>
                        <?php
                                if ($this->session->flashdata('error')) {
                                    echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
                                }
                                if ($this->session->flashdata('success')) {
                                    echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
                                }
                                ?>
                    </div>

                     <div class="clearfix">
                            <div class="common-form">
                              <h3>Carrier Objectives</h3>
                             <?php echo form_open(base_url('job/job_carrier_insert'), array('id' => 'jobseeker_regform','name' => 'jobseeker_regform','class'=>'clearfix')); ?>

                            <div>
                                <span style="color:red">Fields marked with asterisk (*) are mandatory</span>
                            </div>
                                <fieldset class="full-width">
                                         <label>Carrier Objectives:<span style="color:red">*</span></label>
										
                                         

                                           <textarea name ="carrier" id="carrier" rows="4" cols="50" placeholder="Enter Carrier" style="resize: none;"><?php if($carrier1){ echo $carrier1; } ?></textarea>
                                         <?php echo form_error('carrier'); ?>
                          	         </fieldset>
                                    <fieldset class="full-width">                           
                                     <b> Declaration: </b>
                                     <div class="job_carrier_checkbox">
                                     <input type="checkbox" id="checkbox"  name="checkbox" >
                                         I here by Declare that all the above Information are true and correct to best of my knowledge                     
                                       
                                        <?php echo form_error('checkbox') ?>
                                    </div>

                                </fieldset>          

                                 <fieldset class="hs-submit full-width">

<!--                                  <input type="reset">
                                    <input type="submit"  id="previous" name="previous" value="previous">-->
                                    
                                    <input type="submit"  id="submit" name="submit" value="Submit">
                                   
                                </fieldset>

                                </form>
                            </div>    
                        </div>
                    </div>
                    <!-- middle section end -->

                   
                </div>
            </div>
        </div>
    </section>
    
    <footer>
          <?php echo $footer;  ?>
    </footer>
    
</body>
</html>


<!-- validation start-->
<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.js'); ?>"></script>

   <script type="text/javascript">

            //validation for edit email formate form

            $(document).ready(function () { 

                $("#jobseeker_regform").validate({

                    rules: {

                        carrier: {

                            required: true,
                           
                        }, 

                         checkbox: {

                            required: true,
                           
                        },  
                        
                    },

                    messages: {

                        carrier: {

                           required: "Carrier Is Required.",
                            
                        },
                         checkbox: {

                            required: "click on terms and condition Is Required.",
                            
                        },
                    
                    },

                });
                   });
  </script>
    

<!-- validation end--> 

    
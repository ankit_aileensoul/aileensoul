
<!-- start head -->
<?php  echo $head; ?>
<!-- END HEAD -->

<!-- start header -->
<?php echo $header; ?>
<!-- END HEADER -->

    <body class="page-container-bg-solid page-boxed">

      <section>
      
        <div class="user-midd-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <div class="left-side-bar">
                            <ul>
                                <li><a href="<?php echo base_url('job/job_basicinfo_update'); ?>">Basic Information</a></li>

                                <li><a href="<?php echo base_url('job/job_address_update'); ?>">Address</a></li>

                                  <li><a href="<?php echo base_url('job/job_education_update'); ?>">Educational Qualification</a></li>

                                   <li <?php if($this->uri->segment(1) == 'job'){?> class="active" <?php } ?>><a href="#">Project And Training / Internship</a></li>

                              
                                <li class="<?php if($jobdata[0]['job_step'] < '4'){echo "khyati";}?>"><a href="<?php echo base_url('job/job_skill_update'); ?>">Professional Skills</a></li>

                                <li class="<?php if($jobdata[0]['job_step'] < '4'){echo "khyati";}?>"><a href="<?php echo base_url('job/job_apply_for_update'); ?>">Apply For</a></li>
                               
                                <li class="<?php if($jobdata[0]['job_step'] < '4'){echo "khyati";}?>"><a href="<?php echo base_url('job/job_work_exp_update'); ?>">Work Experience</a></li>

                                <li class="<?php if($jobdata[0]['job_step'] < '4'){echo "khyati";}?>"><a href="<?php echo base_url('job/job_curricular_update'); ?>">Extra Curricular Activities</a></li>

                                <li class="<?php if($jobdata[0]['job_step'] < '4'){echo "khyati";}?>"><a href="<?php echo base_url('job/job_reference_update'); ?>">Interest & Reference</a></li>

                                <li class="<?php if($jobdata[0]['job_step'] < '4'){echo "khyati";}?>"><a href="<?php echo base_url('job/job_carrier_update'); ?>">Carrier Objectives</a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- middle section start -->
                    <div class="col-md-6 col-sm-8">

                    <div>
                        <?php
                                if ($this->session->flashdata('error')) {
                                    echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
                                }
                                if ($this->session->flashdata('success')) {
                                    echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
                                }
                                ?>
                    </div>

                     <div class="clearfix">
                            <div class="common-form">
                              <h3>Project And Training / Internship</h3>
                             <?php echo form_open(base_url('job/job_project_insert'), array('id' => 'jobseeker_regform','name' => 'jobseeker_regform','class'=>'clearfix')); ?>

                            <div>
                                <span style="color:red">Project</span>
                            </div>
                                <fieldset class="full-width">
                                         <label>Project Name (Title):</label>

                                          <input type="text" name="project_name"  id="project_name" placeholder="Enter Project Name" value="<?php if($project_name1){ echo $project_name1; } else { echo $job[0]['project_name']; }?>"/>
                                        
                                  </fieldset>

                                  <fieldset class="full-width">
                                         <label>Duration</label>

                                          <input type="text" name="project_duration"  id="project_duration" placeholder="Ex:- 3 month / 4 month" value="<?php if($project_duration1){ echo $project_duration1; } else { echo $job[0]['project_duration']; }?>"/>
                                        
                                  </fieldset>

                                     <fieldset class="full-width">
                                         <label>Project Description</label>

                                          <input type="text" name="project_description"  id="project_description" placeholder="Enter Project Description" value="<?php if($project_description1){ echo $project_description1; } else { echo $job[0]['project_description']; }?>"/>
                                        
                                  </fieldset>


                            <div>
                                <span style="color:red">Training / Internship</span>
                            </div>

                             <fieldset class="full-width">
                                         <label>Intern / Trainee as</label>

                                          <input type="text" name="training_as"  id="training_as" placeholder="Intern / Trainee as" value="<?php if($training_as1){ echo $training_as1; } else { echo $job[0]['training_as']; }?>"/>
                                        
                              </fieldset>

                               <fieldset class="full-width">
                                         <label>Duration</label>

                                          <input type="text" name="training_duration"  id="training_duration" placeholder="Ex:- 3 month / 4 month" value="<?php if($training_duration1){ echo $training_duration1; } else { echo $job[0]['training_duration']; }?>"/>
                                        
                                  </fieldset>

                                   <fieldset class="full-width">
                                         <label>Name of Organization</label>

                                          <input type="text" name="training_organization"  id="training_organization" placeholder="Enter Name of Organization" value="<?php if($training_organization1){ echo $training_organization1; } else { echo $job[0]['training_organization']; }?>"/>
                                        
                                  </fieldset>

                                    

                                  <fieldset class="hs-submit full-width">

<!--                                        <input type="reset">
                                        <input type="submit"  id="previous" name="previous" value="previous">-->
                                        <input type="submit"  id="next" name="next" value="Next">
                                 
                                    
                                </fieldset>

                                </form>
                            </div>    
                        </div>
                    </div>
                    <!-- middle section end -->

                   
                </div>
            </div>
        </div>
    </section>
    
    <footer>
          <?php echo $footer;  ?>
    </footer>
    
</body>
</html>




    
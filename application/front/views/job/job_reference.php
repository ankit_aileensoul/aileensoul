
<!-- start head -->
<?php  echo $head; ?>
<!-- END HEAD -->

<!-- start header -->
<?php echo $header; ?>
<!-- END HEADER -->

<body class="page-container-bg-solid page-boxed">

      <section>
        
        <div class="user-midd-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <div class="left-side-bar">
                            <ul>
                                <li><a href="<?php echo base_url('job/job_basicinfo_update'); ?>">Basic Information</a></li>

                                <li><a href="<?php echo base_url('job/job_address_update'); ?>">Address</a></li>

                                  <li><a href="<?php echo base_url('job/job_education_update'); ?>">Educational Qualification</a></li>


                                  <li><a href="<?php echo base_url('job/job_project_update'); ?>">Project And Training / Internship</a></li>

                                <li><a href="<?php echo base_url('job/job_skill_update'); ?>">Professional Skills</a></li>

                                <li><a href="<?php echo base_url('job/job_apply_for_update'); ?>">Apply For</a></li>
                              
                                <li><a href="<?php echo base_url('job/job_work_exp_update'); ?>">Work Experience</a></li>

                                <li><a href="<?php echo base_url('job/job_curricular_update'); ?>">Extra Curricular Activities</a></li>

                                <li <?php if($this->uri->segment(1) == 'job'){?> class="active" <?php } ?>><a href="#">Interest & Reference</a></li>

                                <li class="<?php if($jobdata[0]['job_step'] < '9'){echo "khyati";}?>"><a href="<?php echo base_url('job/job_carrier_update'); ?>">Carrier Objectives</a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- middle section start -->
                    <div class="col-md-6 col-sm-8">

                    <div>
                        <?php
                                if ($this->session->flashdata('error')) {
                                    echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
                                }
                                if ($this->session->flashdata('success')) {
                                    echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
                                }
                                ?>
                    </div>
                    
                       <div class="clearfix">
                            <div class="common-form">
                              <h3>Interest & References</h3>
                                      
                           <?php echo form_open(base_url('job/job_reference_insert'), array('id' => 'jobseeker_regform','name' => 'jobseeker_regform','class'=>'clearfix')); ?>

                            <div>
                                <span style="color:red">Fields marked with asterisk (*) are mandatory</span>
                            </div>

                                     <fieldset class="full-width">   <label>Interest:<span style="color:red">*</span></label>
                                         
                                          

                                          <textarea name ="interest" id="interest" rows="4" cols="50" placeholder="Enter Interest" style="resize: none;"><?php if($interest1){ echo $interest1; } ?></textarea>
                                         <?php echo form_error('interest'); ?>

                                        </fieldset>
                                        <fieldset class="full-width"><label>References:</label>
                                         
                                        

                                          <textarea name ="reference" id="reference" rows="4" cols="50" placeholder="Enter Reference" style="resize: none;"><?php if($reference1){ echo $reference1; } ?></textarea>
                                         <?php echo form_error('reference'); ?>

                                     </fieldset>
                                <fieldset class="hs-submit full-width">
<!--                                <input type="reset">
                                    <input type="submit"  id="previous" name="previous" value="previous">-->
                                    <input type="submit"  id="next" name="next" value="Next">
                                 
                                    
                                </fieldset>

                                </form>
                            </div>    
                        </div>
                    </div>
                    <!-- middle section end -->

                   
                </div>
            </div>
        </div>
    </section>
  
    
</body>
</html>

<!-- script for validation start -->
  <script type="text/javascript" src="<?php echo base_url('js/jquery-ui.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.js'); ?>"></script>

   <script type="text/javascript">

            //validation for edit email formate form

            $(document).ready(function () { 

                $("#jobseeker_regform").validate({

                    rules: {

                        interest: {

                            required: true,
                           
                        }, 
                       
                        
                    },

                    messages: {

                        interest: {

                           required: "Interest Is Required.",
                            
                        },

                    },

                });
                   });
  </script>
    

<!-- script for validation End -->
       

<!-- start head -->
<?php  echo $head; ?>
<!-- END HEAD -->

 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.3.0/select2.css" rel="stylesheet" />

<!-- start header -->
<?php echo $header; ?>
<!-- END HEADER -->

    <body class="page-container-bg-solid page-boxed">

      <section>
        
        <div class="user-midd-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <div class="left-side-bar">

                         <?php
                             $userid = $this->session->userdata('user_id');
                            $job  =  $this->db->get_where('job_reg',array('user_id' => $userid))->row()->job_step; 
                          
                        ?>
                            <ul>
                               <li><a href="<?php echo base_url('job/job_basicinfo_update'); ?>">Basic Information</a></li>

                                <li><a href="<?php echo base_url('job/job_address_update'); ?>">Address</a></li>

                                  <li><a href="<?php echo base_url('job/job_education_update'); ?>">Educational Qualification</a></li>


                                  <li><a href="<?php echo base_url('job/job_project_update'); ?>">Project And Training / Internship</a></li>

                                <li><a href="<?php echo base_url('job/job_skill_update'); ?>">Professional Skills</a></li>

                                <li <?php if($this->uri->segment(1) == 'job'){?> class="active" <?php } ?>><a href="#">Apply For</a></li>
                              
                                <li class="<?php if($jobdata[0]['job_step'] < '6'){echo "khyati";}?>"><a href="<?php echo base_url('job/job_work_exp_update'); ?>">Work Experience</a></li>

                                <li class="<?php if($jobdata[0]['job_step'] < '6'){echo "khyati";}?>"><a href="<?php echo base_url('job/job_curricular_update'); ?>">Extra Curricular Activities</a></li>

                                <li class="<?php if($jobdata[0]['job_step'] < '6'){echo "khyati";}?>"><a href="<?php echo base_url('job/job_reference_update'); ?>">Interest & Reference</a></li>

                                <li class="<?php if($jobdata[0]['job_step'] < '6'){echo "khyati";}?>"><a href="<?php echo base_url('job/job_carrier_update'); ?>">Carrier Objectives</a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- middle section start -->
                    <div class="col-md-6 col-sm-8">

                    <div>
                        <?php
                                if ($this->session->flashdata('error')) {
                                    echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
                                }
                                if ($this->session->flashdata('success')) {
                                    echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
                                }
                                ?>
                    </div>

                        <div class="clearfix">
                            <div class="common-form">
                                <h3>Apply For</h3>
                            <?php echo form_open(base_url('job/job_apply_for_insert'), array('id' => 'jobseeker_regform','name' => 'jobseeker_regform','class'=>'clearfix')); ?>

                            <div>
                                <span style="color:red">Fields marked with asterisk (*) are mandatory</span>
                            </div>
                                    <fieldset class="full-width">
                                         <label>Apply For<span style="color:red">*</span></label>
                                         

                                         <select name="ApplyFor" id="ApplyFor" style="width:500px">
                                       <?php 
                                       foreach($postskill as $post_key => $post_value){

                                               foreach($post_value as $post_val){

                                                    foreach($post_val as $ski){
                                        ?>


                                      <option value="<?php echo $ski['skill_id']; ?>"><?php echo $ski['skill']; ?></option>
                                    <?php } } }?>
                                      </select>

                                        &nbsp;&nbsp;&nbsp; 

                                     
                                        <?php echo form_error('ApplyFor'); ?>
                                    </fieldset>

                                    <fieldset class="hs-submit full-width">

<!--                                     <input type="reset">
                                    <input type="submit"  id="previous" name="previous" value="previous">-->
                                    <input type="submit"  id="next" name="next" value="Next">
                                   
                                   
                                </fieldset>

                                </form>
                            </div>    
                        </div>
                    </div>
                    <!-- middle section end -->

                   
                </div>
            </div>
        </div>
    </section>
   
</body>
</html>

 <!-- Javascript validation Start-->
  <script type="text/javascript" src="<?php echo base_url('js/jquery-ui.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.validate.js'); ?>"></script>
 <!-- Javascript validation End-->

   <script type="text/javascript">

            //validation for edit email formate form

            $(document).ready(function () { 

                $("#jobseeker_regform").validate({
                     ignore: [],
                    rules: {

                        ApplyFor: {

                            required: true
                           
                        },   
                    },

                    messages: {

                          ApplyFor: {

                            required: "ApplyFor Is Required."
                            
                        },

                    }

                });
                   });
    </script>
 


<!-- script for skill textbox automatic start-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.3.0/select2.js"></script>

<!-- script for skill textbox automatic end-->

<script>

//select2 autocomplete start for apply for start
var complex = <?php echo json_encode($selectdata); ?>;
$('#ApplyFor').select2().select2('val', complex)
//select2 autocomplete start for apply for End

</script>
    
    
<!-- start head -->
<?php echo $head; ?>

<script src="<?php echo base_url('js/fb_login.js'); ?>"></script>
<!--post save success pop up style strat -->
<style>
    body {
        font-family: Arial, sans-serif;
        background-size: cover;
        height: 100vh;
    }

    .box {
        width: 40%;
        margin: 0 auto;
        background: rgba(255,255,255,0.2);
        padding: 35px;
        border: 2px solid #fff;
        border-radius: 20px/50px;
        background-clip: padding-box;
        text-align: center;
    }



    .overlay {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background: rgba(0, 0, 0, 0.7);
        transition: opacity 500ms;
        visibility: hidden;
        opacity: 0;
        z-index: 10;
    }
    .overlay:target {
        visibility: visible;
        opacity: 1;
    }

    .popup {
        margin: 70px auto;
        padding: 20px;
        background: #fff;
        border-radius: 5px;
        width: 30%;
        height: 200px;
        position: relative;
        transition: all 5s ease-in-out;
    }

    .okk{
        text-align: center;
    }

    .popup .okbtn {
        position: absolute;
        transition: all 200ms;
        font-size: 26px;
        font-weight: bold;
        text-decoration: none;
        color: #fff;
        padding: 12px 30px;
        background-color: darkcyan;
        margin-left: -45px;
        margin-top: 15px;
    }

    .popup .pop_content {
        text-align: center;
        margin-top: 40px;

    }

    @media screen and (max-width: 700px){
        .box{
            width: 70%;
        }
        .popup{
            width: 70%;
        }
    }
</style>

<!--post save success pop up style end -->

<!-- start head -->
<?php echo $head; ?>

<!-- END HEAD -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/demo.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/select2-4.0.3.min.css'); ?>">

<!-- start header -->
<?php echo $header; ?>
<?php echo $job_header2; ?>
<!-- END HEADER -->

<body class="page-container-bg-solid page-boxed">
    <div class="user-midd-section">
        <div class="container">
            <div class="row">


                <div class="col-md-4"><div class="profile-box profile-box-left">
                        <div class="full-box-module">    


                            <div class="profile-boxProfileCard  module">
                                <div class="profile-boxProfileCard-cover">
                                    <a class="profile-boxProfileCard-bg u-bgUserColor a-block"
                                       href="<?php echo base_url('job/job_printpreview'); ?>"
                                       tabindex="-1"
                                       aria-hidden="true"
                                       rel="noopener">

                                        <!-- box image start -->
                                        <img src="<?php echo base_url(JOBBGIMAGE . $jobdata[0]['profile_background_main']); ?>" class="bgImage"  style="    height: 95px;
                                             width: 100%; " >
                                        <!-- box image end -->


                                    </a></div>
                                <div class="profile-box-menu  fr col-md-12">
                                    <div class="left- col-md-2"></div>
                                    <div  class="right-section col-md-10">
                                        <ul class="">
                                            <li <?php if ($this->uri->segment(1) == 'job' && $this->uri->segment(2) == 'job_printpreview') { ?> class="active" <?php } ?>><a href="<?php echo base_url('job/job_printpreview'); ?>"> Profile</a>
                                            </li>



                                            <li <?php if ($this->uri->segment(1) == 'job' && $this->uri->segment(2) == 'job_save_post') { ?> class="active" <?php } ?>><a href="<?php echo base_url('job/job_save_post'); ?>">Saved </a>
                                            </li>

                                            <li <?php if ($this->uri->segment(1) == 'job' && $this->uri->segment(2) == 'job_applied_post') { ?> class="active" <?php } ?>><a href="<?php echo base_url('job/job_applied_post'); ?>">Applied </a>
                                            </li>




                                        </ul>
                                    </div>
                                </div>
                                <div class="profile-boxProfileCard-content">
                                    <div class="buisness-profile-txext ">

                                        <a class="profile-boxProfileCard-avatarLink a-inlineBlock" href="<?php echo base_url('job/job_printpreview/' . $jobdata[0]['user_id']); ?>" title="zalak" tabindex="-1" aria-hidden="true" rel="noopener">
                                            <img src="<?php echo base_url(USERIMAGE . $jobdata[0]['job_user_image']); ?>" alt=""  style="" >

                                        </a>
                                    </div>
                                    <div class="profile-box-user">
                                        <span class="profile-box-name ">
                                            <a  href="<?php echo site_url('job/job_printpreview/' . $jobdata[0]['user_id']); ?>">  <?php echo $jobdata[0]['fname'] . ' ' . $jobdata[0]['lname']; ?></a>
                                        </span>
                                        <div class="profile-box-user">
                                            <span class="profile-box-name"><a href="<?php echo base_url('job/job_printpreview/' . $jobdata[0]['user_id']); ?>"><?php
                                                    if (ucwords($jobdata[0]['designation'])) {
                                                        echo ucwords($jobdata[0]['designation']);
                                                    } else {
                                                        echo "Designation";
                                                    }
                                                    ?></a></span>
                                        </div>
                                    </div>
                                    <div id="profile-box-profile-prompt"></div>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>


                <div class="col-md-7 col-sm-7 all-form-content">
                    <div class="common-form">
                        <div class="job-saved-box">
                            <h3>Search Result</h3>

                            <div class="contact-frnd-post">
                                <!-- @nk!t 7-4-2017 start -->
                                <?php
                                if ($postdetail) {
                                    ?>
                                    <!-- @nk!t 7-4-2017 end -->
                                    <?php
                                    foreach ($postdetail as $post) {



                                        $userid = $this->session->userdata('aileenuser');
                                        $contition_array = array('user_id' => $userid, 'post_id' => $post['post_id'], 'job_delete' => 0);
                                        $jobdata = $this->common->select_data_by_condition('job_apply', $contition_array, $data = '*', $sortby = '', $orderby = 'desc', $limit = '', $offset = '', $join_str = array(), $groupby = '');

                                        if ($jobdata[0]['job_save'] != 2) {
                                            ?> 

                                            <div class="job-contact-frnd ">

                                                <div class="profile-job-post-detail clearfix">


                                                    <!-- pop up box start-->
                                                    <div id="popup1" class="overlay">
                                                        <div class="popup">

                                                            <div class="pop_content">
                                                                Your Post is Successfully Saved.
                                                                <p class="okk"><a class="okbtn" href="#">Ok</a></p>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- pop up box end-->

                                                    <!-- pop up box start-->
                                                    <div id="<?php echo 'popup5' . $post['post_id']; ?>" class="overlay">
                                                        <div class="popup">


                                                            <div class="pop_content">
                                                                Are You Sure want to Apply this post?.

                                                                <p class="okk">
                                                                    <a id="<?php echo $post['post_id']; ?>" onClick="apply_post(this.id)"  class="okbtnpop" href="#">Apply</a></p>

                                                                <p class="okk"><a class="cnclbtn" href="#">Cancle</a></p>

                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- pop up box end-->




                                                    <div class="profile-job-post-title-inside clearfix">
                                                        <div class="profile-job-post-location-name">
                                                            <ul>

                                                                <li><?php echo $post['post_name']; ?></li>

                                                                <li><a href="<?php echo base_url('recruiter/rec_profile/' . $post['user_id']); ?>"><?php
                                                                        $cache_time = $this->db->get_where('recruiter', array('user_id' => $post['user_id']))->row()->re_comp_name;

                                                                        echo ucwords($cache_time);
                                                                        ?></a></li>

                                                                <li><a href="<?php echo base_url('recruiter/rec_profile/' . $post['user_id']); ?>"><?php
                                                                        $cache_time = $this->db->get_where('recruiter', array('user_id' => $post['user_id']))->row()->rec_firstname;

                                                                        echo ucwords($cache_time);
                                                                        ?></a></li>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="profile-job-post-title clearfix search">
                                                        <div class="profile-job-profile-button clearfix">
                                                            <div class="profile-job-details col-md-12">
                                                                <ul>
                                                                    <li>
                                                                        <a href="#" data-toggle="tooltip" data-placement="top" title="<?php echo $post['exp_month'] . "month  " . $post['exp_year'] . "year" . "Required"; ?>"><i class="fa fa-star-o" aria-hidden="true"></i>       <?php echo $post['max_month'] . "month  " . $post['max_year'] . "year"; ?> </a>

                                                                    </li>

                                                                    <li>

                                                                        <?php $cityname = $this->db->get_where('cities', array('city_id' => $post['city']))->row()->city_name; ?>
                                                                        <p><i class="fa fa-map-marker" aria-hidden="true"><?php echo $cityname; ?></i></p>
                                                                    </li>

                                                                    <li class="fr">

                                                                        Created Date:<?php
                                                                        echo date('d-M-Y', strtotime($post['created_date']));
                                                                        ?>

                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="profile-job-profile-menu">
                                                            <ul class="clearfix">
                                                                <li> <b> Skills:</b> <span> 
                                                                        <?php
                                                                        $comma = ",";
                                                                        $k = 0;
                                                                        $aud = $post['post_skill'];
                                                                        $aud_res = explode(',', $aud);
                                                                        foreach ($aud_res as $skill) {
                                                                            if ($k != 0) {
                                                                                echo $comma;
                                                                            }
                                                                            $cache_time = $this->db->get_where('skill', array('skill_id' => $skill))->row()->skill;


                                                                            echo $cache_time;
                                                                            $k++;
                                                                        }
                                                                        ?>       
                                                                    </span>
                                                                </li>


                                                                <?php if ($post['other_skill']) { ?>
                                                                    <li><b>Other Skill:</b><span><?php echo $post['other_skill']; ?></span>
                                                                    </li>
                                                                <?php } else { ?>
                                                                    <li><b>Other Skill:</b><span><?php echo "-"; ?></span></li><?php } ?>

                                                                <li><b>Description:</b><span><?php echo $post['post_description']; ?></span>
                                                                </li>
                                                                <li><b>Interview Process:</b><span><?php echo $post['interview_process']; ?></span>
                                                                </li>

                                                                <?php if ($post['min_sal']) { ?>
                                                                    <li><b>Minimum Salary:</b><span><?php echo $post['min_sal']; ?></span>
                                                                    </li>
                                                                <?php } else { ?>
                                                                    <li><b>Minimum Salary:</b><span><?php echo "-"; ?></span>
                                                                    </li><?php } ?>

                                                                <?php if ($post['max_sal']) { ?>
                                                                    <li><b>Maximum Salary:</b><span><?php echo $post['max_sal']; ?></span>
                                                                    </li>
                                                                <?php } else { ?>
                                                                    <li><b>Maximum Salary:</b><span><?php echo "-"; ?></span>
                                                                    </li><?php } ?>

                                                                <li><b>No of Position:</b><span><?php echo $post['post_position']; ?></span>
                                                                </li>


                                                            </ul>
                                                        </div>
                                                        <div class="profile-job-profile-button clearfix">
                                                            <div class="profile-job-details col-md-12">
                                                                <ul><li>

                                                                        Last Date:<?php
                                                                        echo date('d-M-Y', strtotime($post['post_last_date']));
                                                                        ?>
                                                                    </li>

                                                                    <input type="hidden" name="search" id="search" value="<?php echo $keyword; ?>">

                                                                    <input type="text" name="search" id="search" value="<?php echo $keyword1; ?>">

                                                                    <?php
                                                                    $this->data['userid'] = $userid = $this->session->userdata('aileenuser');

                                                                    $contition_array = array('post_id' => $post['post_id'], 'job_delete' => 0, 'user_id' => $userid);
                                                                    $jobsave = $this->data['jobsave'] = $this->common->select_data_by_condition('job_apply', $contition_array, $data = '*', $sortby = '', $orderby = 'desc', $limit = '', $offset = '', $join_str = array(), $groupby = '');

                                                                    if ($jobsave[0]['job_save'] == 1) {
                                                                        ?>

                                                                        <button  class="button" disabled>Applied</button>

                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <input type="hidden" id="<?php echo 'allpost' . $post['post_id']; ?>" value="all">
                                                                        <input type="hidden" id="<?php echo 'userid' . $post['post_id']; ?>" value="<?php echo $post['user_id']; ?>">

                                                                        <a href="<?php echo '#popup5' . $post['post_id']; ?>"  class= "<?php echo 'applypost' . $post['post_id']; ?>  button">Apply</a>   

                                                                        <?php
                                                                        $userid = $this->session->userdata('aileenuser');
                                                                        $contition_array = array('user_id' => $userid, 'job_save' => '2', 'post_id ' => $post['post_id'], 'job_delete' => '0');
                                                                        $jobsave = $this->data['jobsave'] = $this->common->select_data_by_condition('job_apply', $contition_array, $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '');

                                                                        if ($jobsave) {
                                                                            ?>
                                                                            <a class="button">Saved</a>
                                                                        <?php } else { ?>       

                                                                            <a id="<?php echo $post['post_id']; ?>" onClick="save_post(this.id)" href="#popup1" class="<?php echo 'savedpost' . $post['post_id']; ?> button">Save</a>
                                                                        <?php } ?>
                                                                        <?php
                                                                    }
                                                                    ?>

                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <!-- @nk!t 7-4-2017 start -->
                                    <?php
                                } else {
                                    ?>

                                    <div class="text-center rio">
                                        <h1 class="page-heading  product-listing" style="border:0px;margin-bottom: 11px;">Oops No Data Found.</h1>
                                        <p style="margin-left:4%;text-transform:none !important;border:0px;">We couldn't find what you were looking for.</p>
                                        <ul>
                                            <li style="text-transform:none !important; list-style: none;">Make sure you used the right keywords.</li>
                                        </ul>
                                    </div>

                                    <?php
                                }
                                ?>
                                <!-- @nk!t 7-4-2017 end -->
                            </div>


                        </div>
                    </div>
                </div>
                </section>

                </body>
                </html>

                <!-- script for skill textbox automatic start-->
                <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                <script src="<?php echo base_url('js/jquery.highlite.js'); ?>"></script>
                <script src="<?php echo base_url('js/jquery-ui.min.js'); ?>"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
                <!-- script for skill textbox automatic end -->

                <!-- apply post start -->

                <script type="text/javascript">
                                                            function apply_post(abc)
                                                            {

                                                                var alldata = document.getElementById("allpost" + abc);

                                                                var user = document.getElementById("userid" + abc);

                                                                $.ajax({
                                                                    type: 'POST',
                                                                    url: '<?php echo base_url() . "job/job_apply_post" ?>',
                                                                    data: 'post_id=' + abc + '&allpost=' + alldata.value + '&userid=' + user.value,
                                                                    success: function (data) {

                                                                        $('.' + 'applypost' + abc).html(data);


                                                                    }
                                                                });

                                                            }
                </script>

                <!-- apply post end -->

                <!-- save post start -->

                <script type="text/javascript">
                    function save_post(abc)
                    {

                        $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url() . "job/job_save" ?>',
                            data: 'post_id=' + abc,
                            success: function (data) {

                                $('.' + 'savedpost' + abc).html(data);


                            }
                        });

                    }
                </script>

                <!-- save post end -->



                <!-- for autocomplete in searchbox -->
                <script>
                    $(function () {

                        var complex = <?php echo json_encode($demo); ?>;


                        var availableTags = complex;
                        $("#tags").autocomplete({
                            source: availableTags
                        });
                    });
                </script>
                <!-- end for autocomplete -->



                <!-- code for highlight start -->

                <script type="text/javascript">
                    var text = document.getElementById("search").value;
                    //alert(text);

                    $(".search").highlite({

                        text: text

                    });



                </script>
                <!-- highlight is ended -->



                <script>
                    //select2 autocomplete start for skill
                    $('#searchskills').select2({

                        placeholder: 'Find Your Skills',

                        ajax: {

                            url: "<?php echo base_url(); ?>job/keyskill",
                            dataType: 'json',
                            delay: 250,

                            processResults: function (data) {

                                return {

                                    results: data


                                };

                            },
                            cache: true
                        }
                    });
                    //select2 autocomplete End for skill

                    //select2 autocomplete start for Location
                    $('#searchplace').select2({

                        placeholder: 'Find Your Location',
                        maximumSelectionLength: 1,
                        ajax: {

                            url: "<?php echo base_url(); ?>job/location",
                            dataType: 'json',
                            delay: 250,

                            processResults: function (data) {

                                return {

                                    results: data


                                };

                            },
                            cache: true
                        }
                    });
                    //select2 autocomplete End for Location

                </script>

                <script>
                    //tooltip
                    $(document).ready(function () {
                        $('[data-toggle="tooltip"]').tooltip();

                    });
                </script>



                <!-- popup form edit start -->

                <script>
                    // Get the modal
                    var modal = document.getElementById('myModal');

                    // Get the button that opens the modal
                    var btn = document.getElementById("myBtn");

                    // Get the <span> element that closes the modal
                    var span = document.getElementsByClassName("close")[0];

                    // When the user clicks the button, open the modal 
                    btn.onclick = function () {
                        modal.style.display = "block";
                    }

                    // When the user clicks on <span> (x), close the modal
                    span.onclick = function () {
                        modal.style.display = "none";
                    }

                    // When the user clicks anywhere outside of the modal, close it
                    window.onclick = function (event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                        }
                    }
                </script>


                <!-- popup form edit END -->

                <script type="text/javascript" src="<?php echo base_url('js/jquery.validate.min.js') ?>"></script>
                <script type="text/javascript" src="<?php echo base_url('js/jquery.validate.js'); ?>"></script>

                <script type="text/javascript">

                    //validation for edit email formate form

                    $(document).ready(function () {

                        $("#jobdesignation").validate({

                            rules: {

                                designation: {

                                    required: true,

                                },

                            },

                            messages: {

                                designation: {

                                    required: "Designation Is Required.",

                                },

                            },

                        });
                    });
                </script>

                <!-- for search validation -->
                <script type="text/javascript">
                    function checkvalue() {
                        // alert("hi");
                        var searchkeyword = document.getElementById('tags').value;
                        var searchplace = document.getElementById('searchplace').value;
                        // alert(searchkeyword);
                        // alert(searchplace);
                        if (searchkeyword == "" && searchplace == "") {
                         //   alert('Please enter Keyword');
                            return false;
                        }
                    }
                </script>
                <!-- end search validation -->
